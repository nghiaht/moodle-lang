<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'teambuilder', language 'en', branch 'MOODLE_28_STABLE'
 *
 * @package   teambuilder
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['buildteams'] = 'Build Teams';
$string['intro'] = 'Introduction';
$string['intro_help'] = 'The introduction to your Team Builder instance.';
$string['modulename'] = 'Team Builder';
$string['modulenameplural'] = 'Team Builders';
$string['name'] = 'Name';
$string['pluginadministration'] = 'Team Builder administration';
$string['pluginname'] = 'Team Builder';
$string['preview'] = 'Preview';
$string['questionnaire'] = 'Questionnaire';
$string['teambuilder'] = 'Team Builder';
$string['teambuilder:addinstance'] = 'Add a new teambuilder module';
$string['teambuilder:build'] = 'Build teams from survey response';
$string['teambuilder:create'] = 'Create survey';
$string['teambuilder:respond'] = 'Respond to Questionnaire';
