<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'report_myfeedback', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   report_myfeedback
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['affiliateddepartment'] = 'Affiliated department:';
$string['attempt'] = 'attempt';
$string['attempts'] = 'attempts';
$string['blockstring'] = 'My Feedback string';
$string['blocktitle'] = 'My Feedback';
$string['dashboard'] = 'My Feedback Dashboard';
$string['dbhost'] = 'DB Host';
$string['dbhostinfo'] = 'Remote Database host name (on which the SQL queries will be executed - must be a duplicate of this Moodle database instance - used for avoiding load issues on primary Moodle database).<br />Leave blank to use the default Moodle database.';
$string['dbname'] = 'DB Name';
$string['dbnameinfo'] = 'Remote Database name (on which the SQL queries will be executed - must be a duplicate of this Moodle database instance - used for avoiding load issues on primary Moodle database).<br />Leave blank to use the default Moodle database.';
$string['dbpass'] = 'DB Password';
$string['dbpassinfo'] = 'Remote Database password (for above username).<br />Leave blank to use the default Moodle database.';
$string['dbuser'] = 'DB Username';
$string['dbuserinfo'] = 'Remote Database username (should have SELECT privileges on above DB).<br />Leave blank to use the default Moodle database.';
$string['draft'] = 'draft';
$string['draft_submission'] = 'draft submission';
$string['draft_submission_msg'] = 'The assignment is still in draft status. It has not yet been submitted';
$string['email_tutor'] = 'Email your tutor';
$string['enrolledmodules'] = 'Modules currently enrolled on:';
$string['eventreportdownload'] = 'My Feedback report table downloaded';
$string['eventreportviewed'] = 'My Feedback report viewed';
$string['export_to_excel'] = 'Export to Excel';
$string['feedback'] = 'view feedback';
$string['fullfeedback'] = 'view full feedback';
$string['grademark'] = 'GradeMark';
$string['gradetblheader_assessment'] = 'Assessment (part name)';
$string['gradetblheader_availablegrade'] = 'Available grade';
$string['gradetblheader_course'] = 'Course';
$string['gradetblheader_duedate'] = 'Due date';
$string['gradetblheader_feedback'] = 'Full feedback';
$string['gradetblheader_generalfeedback'] = 'General feedback';
$string['gradetblheader_grade'] = 'Grade';
$string['gradetblheader_module'] = 'Module';
$string['gradetblheader_range'] = 'Range';
$string['gradetblheader_relativegrade'] = 'Relative grade';
$string['gradetblheader_submissiondate'] = 'Submission date';
$string['gradetblheader_submission_feedback'] = 'Submission / Feedback';
$string['gradetblheader_type'] = 'Type';
$string['gradetblheader_viewed'] = 'Viewed';
$string['gradetblheader_weighting'] = 'Weighting';
$string['grading_form'] = 'view grading form';
$string['groupwork'] = 'group';
$string['lastmoodlelogin'] = 'Last Moodle login:';
$string['late_submission_msg'] = 'The assignment was submitted late.';
$string['manual_gradeitem'] = 'Manual item';
$string['meet_tutor'] = 'Meet your tutor';
$string['moodle_assignment'] = 'Assignment';
$string['my_feedback'] = 'My Feedback';
$string['myfeedback:view'] = 'View my feedback';
$string['new_window_msg'] = 'Opens in a new window';
$string['noenrolments'] = 'This user has not yet been enrolled in any courses';
$string['no_submission'] = 'no submission';
$string['no_submission_msg'] = 'There is no submission and it is past the due date. Disregard if you have been granted an individual extension.';
$string['offline_assignment'] = 'offline';
$string['originality'] = 'Originality';
$string['parentdepartment'] = 'Parent department:';
$string['pluginname'] = 'My Feedback';
$string['print_report'] = 'Print';
$string['programme'] = 'Programme:';
$string['provisional_grades1'] = 'The marks shown here are provisional and may include marks for assessments that do not count towards your final grade. Please refer to the';
$string['provisional_grades2'] = 'to see a formal record of your grade.';
$string['quiz'] = 'Quiz';
$string['reset_table'] = 'Reset table';
$string['review'] = 'review';
$string['rubric'] = 'view rubric';
$string['search'] = 'Search';
$string['submission'] = 'view submission';
$string['tabs_feedback'] = 'Feedback comments';
$string['tabs_feedback_text'] = 'Here you can view the general feedback from the assessed parts of your modules. This is taken from the General feedback section on Moodle and Turnitin assignments. Select view feedback to view any of your assessments.';
$string['tabs_overview'] = 'Overview';
$string['tabs_tutor'] = 'Personal tutor';
$string['turnitin_assignment'] = 'Turnitin';
$string['tutor_messages'] = 'Messages';
$string['wordcloud_text'] = 'This word cloud shows the frequency of words and phrases that have appeared in your feedback - the larger the word, the more frequently it is used in feedback. Hover over a word to see how many times and where it has been used.';
$string['wordcloud_title'] = 'Words used commonly in feedback';
$string['workshop'] = 'Workshop';
