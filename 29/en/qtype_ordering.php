<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_ordering', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   qtype_ordering
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addmoreanswers'] = 'Add {$a} more items';
$string['answer'] = 'Item text';
$string['answerheader'] = 'Draggable item {no}';
$string['correctorder'] = 'The correct order for these items is as follows:';
$string['defaultquestionname'] = 'Drag the following items into the correct order.';
$string['noresponsedetails'] = 'Sorry, no details of the response to this question are available.';
$string['notenoughanswers'] = 'Ordering questions must have more than {$a} answers.';
$string['ordering'] = 'Ordering';
$string['ordering_help'] = 'Several items are displayed in a jumbled order. The items can be dragged into a meaningful order.';
$string['ordering_link'] = 'question/type/ordering';
$string['orderingsummary'] = 'Put jumbled items into a meaningful order.';
$string['pluginname'] = 'Ordering';
$string['pluginnameadding'] = 'Adding an Ordering question';
$string['pluginnameediting'] = 'Editing an Ordering question';
$string['pluginname_help'] = 'Several items are displayed in a jumbled order. The items can be dragged into a meaningful order.';
$string['pluginname_link'] = 'question/type/ordering';
$string['pluginnamesummary'] = 'Put jumbled items into a meaningful order.';
$string['selectall'] = 'Select all items';
$string['selectcontiguous'] = 'Select a contiguous subset of items';
$string['selectcount'] = 'Size of subset';
$string['selectcount_help'] = 'The number of items that will be displayed when the question is appears in a quiz.';
$string['selectrandom'] = 'Select a random subset of items';
$string['selecttype'] = 'Item selection type';
$string['selecttype_help'] = 'Choose whether to display all the items or a subset of the items.';
