<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'virtualclass', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   virtualclass
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['askplaymsg'] = '<span id="askplaymsg"> Should we start playing session?</span><br /><span id="remaingmsg">Remaining data could be downloaded in background.</span>';
$string['audiotest'] = 'Test Audio';
$string['closebeforeopen'] = 'Could not save the virtualclass. You have specified a close date before the open date.';
$string['closenotset'] = 'You have to specify a close time.';
$string['closesameopen'] = 'Session open and close time should not be same';
$string['closetime'] = 'Sessions closed';
$string['configactiontolocalrun'] = 'If you select \'Local\' SSL (https://) will be required to use Screen Sharing Features.';
$string['disablespeaker'] = 'Disable Audio';
$string['downloadsession'] = 'Please wait while download Sesssion.';
$string['enablespeaker'] = 'Enable Audio';
$string['indvprogress'] = 'Individual File Upload.';
$string['joinroom'] = 'Join virtual class room';
$string['liverun'] = 'Online - Use vidya.io server';
$string['localrun'] = 'Local - Use Moodle Files';
$string['modulename'] = 'Virtualclass';
$string['modulename_help'] = 'Use the virtualclass module for real-time online learning.  The virtualclass module allows you to participate in synchronous learning,which means that the teacher and students are logged into the virtual learning environment and interacting with each other at the same time.
This module provide students with asynchronous communication tools, such as whiteboard and chat capabilities';
$string['modulenameplural'] = 'Virtualclasss';
$string['notsavekey'] = 'API key is missing. Go to <a href="{$a}">GetKey page</a> to save key.';
$string['opentime'] = 'Sessions open';
$string['overallprogress'] = 'Overall Progress';
$string['play'] = 'Play';
$string['pluginadministration'] = 'Virtualclass administration';
$string['pluginname'] = 'Virtualclass';
$string['pressalways'] = 'Press always to speak';
$string['pressonce'] = 'Press once to speak';
$string['pushtotalk'] = 'Push <br >  To <br >Talk &nbsp; &nbsp;';
$string['replay_message'] = 'Your recordered session has been played';
$string['selectteacher'] = 'Select teacher';
$string['selectteacher_help'] = 'Select teacher as a moderator for virtual classroom';
$string['sessionclosed'] = 'This session is not available now';
$string['sessionsschedule'] = 'Schedule for sessions';
$string['silencedetect'] = 'Silence Detection';
$string['teachername'] = 'Teacher : {$a->firstname} {$a->lastname}';
$string['totalprogress'] = 'Total Progress';
$string['uploadedsession'] = 'You have uploaded the current session.';
$string['uploadrecordedfile'] = 'Upload recorded file';
$string['uploadsession'] = 'Please wait while upload Session.';
$string['virtualclass'] = 'Virtualclass';
$string['virtualclass:addinstance'] = 'Add a new virtualclass';
$string['virtualclass:dorecording'] = 'Save recoding';
$string['virtualclassfieldset'] = 'Custom example fieldset';
$string['virtualclassname'] = 'Virtualclass name';
$string['virtualclassname_help'] = 'This is a real-time online class room which allows participates interaction with each other and provide asynchronous communication tools such as whiteboard, chat and screen sharing .';
$string['virtualclass:recordingdelete'] = 'Delete recording';
$string['virtualclass:recordingupload'] = 'Upload recorded file';
$string['virtualclasstiming'] = 'Virtual class timing : {$a->open} to {$a->close}';
$string['virtualclass:view'] = 'View virtualclass';
$string['wheretorunvirtualclass'] = 'From where to serve Virtualclass App';
