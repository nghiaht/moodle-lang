<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_moodlecloudsignup', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   local_moodlecloudsignup
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['brand:brandname'] = 'MoodleCloud';
$string['brand:logout'] = 'logout';
$string['FOSUserBundle:change_password.flash.success'] = 'The password has been changed';
$string['FOSUserBundle:change_password.submit'] = 'Change password';
$string['FOSUserBundle:form.current_password'] = 'Current password:';
$string['FOSUserBundle:form.email'] = 'Email:';
$string['FOSUserBundle:form.group_name'] = 'Group name:';
$string['FOSUserBundle:form.new_password'] = 'New password:';
$string['FOSUserBundle:form.new_password_confirmation'] = 'Repeat new password:';
$string['FOSUserBundle:form.password'] = 'Password:';
$string['FOSUserBundle:form.password_confirmation'] = 'Repeat password:';
$string['FOSUserBundle:form.username'] = 'Username:';
$string['FOSUserBundle:group.edit.submit'] = 'Update group';
$string['FOSUserBundle:group.flash.created'] = 'The group has been created';
$string['FOSUserBundle:group.flash.deleted'] = 'The group has been deleted';
$string['FOSUserBundle:group.flash.updated'] = 'The group has been updated';
$string['FOSUserBundle:group.new.submit'] = 'Create group';
$string['FOSUserBundle:group.show.name'] = 'Group name';
$string['FOSUserBundle:layout.logged_in_as'] = 'Logged in as %username%';
$string['FOSUserBundle:layout.login'] = 'Login';
$string['FOSUserBundle:layout.logout'] = 'Logout';
$string['FOSUserBundle:layout.register'] = 'Register';
$string['FOSUserBundle:profile.edit.submit'] = 'Update';
$string['FOSUserBundle:profile.flash.updated'] = 'The profile has been updated';
$string['FOSUserBundle:profile.show.email'] = 'Email';
$string['FOSUserBundle:profile.show.username'] = 'Username';
$string['FOSUserBundle:registration.back'] = 'Back to the originating page.';
$string['FOSUserBundle:registration.check_email'] = 'An email has been sent to %email%. It contains an activation link you must click to activate your account.';
$string['FOSUserBundle:registration.confirmed'] = 'Congrats %username%, your account is now activated.';
$string['FOSUserBundle:registration.email.message'] = 'Hello %username%!

To finish activating your account - please visit %confirmationUrl%

Regards,
the Team.';
$string['FOSUserBundle:registration.email.subject'] = 'Welcome %username%!';
$string['FOSUserBundle:registration.flash.user_created'] = 'The user has been created successfully';
$string['FOSUserBundle:registration.submit'] = 'Register';
$string['FOSUserBundle:resetting.check_email'] = 'An email has been sent to %email%. It contains a link you must click to reset your password.';
$string['FOSUserBundle:resetting.email.message'] = 'Hello %username%!

To reset your password - please visit %confirmationUrl%

Regards,
the Team.';
$string['FOSUserBundle:resetting.email.subject'] = 'Reset Password';
$string['FOSUserBundle:resetting.flash.success'] = 'The password has been reset successfully';
$string['FOSUserBundle:resetting.password_already_requested'] = 'The password for this user has already been requested within the last 24 hours.';
$string['FOSUserBundle:resetting.request.invalid_username'] = 'The username or email address "%username%" does not exist.';
$string['FOSUserBundle:resetting.request.submit'] = 'Reset password';
$string['FOSUserBundle:resetting.request.username'] = 'Username or email address:';
$string['FOSUserBundle:resetting.reset.submit'] = 'Change password';
$string['FOSUserBundle:security.login.password'] = 'Password:';
$string['FOSUserBundle:security.login.remember_me'] = 'Remember me';
$string['FOSUserBundle:security.login.submit'] = 'Login';
$string['FOSUserBundle:security.login.username'] = 'Username:';
$string['homepage:body.intro'] = 'Free Moodle hosting, from the people that make Moodle.';
$string['homepage:body.title'] = 'MoodleCloud';
$string['homepage:general.servicestatus'] = 'Service Status';
$string['homepage:infoboxes.footer'] = 'Feeling all inspired and ready to start building your MoodleCloud site? We’ve created some helpful content you’ll see when you first start up your site. You can also check out our Youtube channel for how-to videos and to discover more about the potential of Moodle and read stories about how educators across the globe are creating amazing things with their Moodle sites.';
$string['homepage:infoboxes.infobox1.intro'] = 'MoodleCloud is a new hosting solution offered directly from the makers of Moodle, allowing anyone to get a free Moodle site in seconds. MoodleCloud includes the latest version of Moodle’s award-winning education technology software in addition to selected integrations.';
$string['homepage:infoboxes.infobox1.title'] = 'Latest Version of Moodle';
$string['homepage:infoboxes.infobox2.intro'] = 'Add a conferencing session to your MoodleCloud with thanks to our friends at BigBlueButton. Integrated free with all MoodleCloud sites, their open source solution includes full online conferencing, including video, audio, whiteboards and desktop sharing.';
$string['homepage:infoboxes.infobox2.title'] = 'BigBlueButton Conferencing';
$string['homepage:infoboxes.infobox3.intro'] = 'Free MoodleCloud sites vary a little to core Moodle, are a little less flexible with a small advert in the site footer and do not come with support - if you need help or tips in using Moodle, well you’ll need to contact a Moodle Partner or visit the community resources on moodle.org.';
$string['homepage:infoboxes.infobox3.title'] = 'Community Resources';
$string['homepage:infoboxes.infobox4.intro'] = 'Every MoodleCloud administrator has access to their portal where you can monitor usage, statistics and remove your site (not that we think you’d want to). This is a good way to see how your site is growing and flourishing with students, courses and uploads.';
$string['homepage:infoboxes.infobox4.title'] = 'Monitor Usage & Statistics';
$string['homepage:infoboxes.infobox5.intro'] = 'Access your content, users and take learning and training everywhere with MoodleCloud on mobile. Our official Moodle Mobile app connects with your MoodleCloud with no setup required by administrators. Available on iOS, Android & Windows.';
$string['homepage:infoboxes.infobox5.title'] = 'Mobile App';
$string['homepage:infoboxes.infobox6.intro'] = 'MoodleCloud is especially suited for lone teachers without many resources who want to use Moodle for a class, but it’s also aimed at anyone wanting to try Moodle in a real educational situation, such as small schools, small businesses, government departments.';
$string['homepage:infoboxes.infobox6.title'] = 'Teachers';
$string['homepage:infoboxes.intro'] = 'Your own free MoodleCloud site - no hassle, no trial, no credit card, no problem.';
$string['homepage:infoboxes.title'] = 'Wish the tech didn\'t interrupt the teaching?';
$string['homepage:login.intro'] = 'If you have a MoodleCloud account already, you can access your portal here:';
$string['homepage:login.title'] = 'Portal Login';
$string['homepage:page.description'] = 'Moodle hosting solutions available from Moodle available for anyone to launch the learning software in the cloud';
$string['homepage:page.title'] = 'MoodleCloud: Free Hosting Services from the makers of Moodle';
$string['homepage:signup.info.sizelimit'] = '200MB file space';
$string['homepage:signup.info.title'] = 'Free Moodle sites are currently limited to:';
$string['homepage:signup.info.userlimit'] = '50 active user accounts';
$string['homepage:signup.intro'] = 'Create your free Moodle site with MoodleCloud today.';
$string['homepage:signup.links.moreinfo.title'] = 'Wait, I Need more Info';
$string['homepage:signup.links.signup.title'] = 'Sign Up';
$string['homepage:signup.title'] = 'Sign Up';
$string['langconfig:direction'] = 'ltr';
$string['langconfig:langname'] = 'English';
$string['login:security.login.password'] = 'Password';
$string['login:security.login.password_placeholder'] = 'Password';
$string['login:security.login.submit'] = 'Log In';
$string['login:security.login.username'] = 'MoodleCloud name';
$string['login:security.login.username_placeholder'] = 'mysite.moodlecloud.com';
$string['messages:auditlog.administrator'] = 'MoodleCloud Administration Team';
$string['messages:auditlog.automatedtool'] = 'MoodleCloud Bot';
$string['messages:auditlog.site.removalComplete'] = 'The Site was removed';
$string['messages:auditlog.site.removalQueued'] = 'Site removal was queued';
$string['pluginname'] = 'MoodleCloud Signup and Portal Strings';
$string['portal:graphs.axes.dbsize'] = 'Database size';
$string['portal:graphs.axes.filesize'] = 'File size';
$string['portal:graphs.axes.users'] = 'Users';
$string['portal:graphs.filetypes.application'] = 'Application data';
$string['portal:graphs.filetypes.audio'] = 'Audio';
$string['portal:graphs.filetypes.example'] = 'Example';
$string['portal:graphs.filetypes.image'] = 'Images';
$string['portal:graphs.filetypes.message'] = 'Messages';
$string['portal:graphs.filetypes.model'] = 'Models';
$string['portal:graphs.filetypes.multipart'] = 'Multipart-items';
$string['portal:graphs.filetypes.text'] = 'Text';
$string['portal:graphs.filetypes.video'] = 'Videos';
$string['portal:historical.fetchfailure.lead'] = 'I\'m sorry, we were unable to retrieve your historical data. If you come back later, we may have something for you.';
$string['portal:historical.fetchfailure.title'] = 'Oh noes!';
$string['portal:list.body.lead'] = '';
$string['portal:list.body.title'] = 'MoodleCloud Portal';
$string['portal:list.table.titles.location'] = 'Location';
$string['portal:list.table.titles.sites'] = 'Sites';
$string['portal:list.table.titles.subdomain'] = 'Subdomain';
$string['portal:messages.csrffailure'] = 'Unable to confirm changes. Please try again.';
$string['portal:messages.removalqueued'] = 'Queued for removal';
$string['portal:messages.sitenotfound'] = 'Site could not be loaded. Please try again.';
$string['portal:page.title'] = 'MoodleCloud: Portal';
$string['portal:siteinfo.page.title'] = 'MoodleCloud: Site information for SUBDOMAIN';
$string['portal:siteinfo.panels.historical.title'] = 'Historical statistics';
$string['portal:siteinfo.panels.info.location'] = 'Location';
$string['portal:siteinfo.panels.info.status'] = 'Status';
$string['portal:siteinfo.panels.info.title'] = 'Information';
$string['portal:siteinfo.panels.info.version'] = 'Version';
$string['portal:siteinfo.panels.statistics.title'] = 'Statistics';
$string['portal:statistics.fetchfailure.lead'] = 'I\'m sorry, we were unable to retrieve your current statistics. If you come back later, we may have something for you.';
$string['portal:statistics.fetchfailure.title'] = 'Oh noes!';
$string['portal:statistics.graphs.databasesize'] = 'Database size';
$string['portal:statistics.graphs.files'] = 'Files';
$string['portal:statistics.graphs.users'] = 'Users';
$string['portal:tasks.removal.warning.html'] = '<h1>Hello from MoodleCloud!</h1>
<p>We\'ve noticed that you have not been using your MoodleCloud site <a href="%url%">%fqdn%</a> in the last 60 days.</p>
<p>As per our terms and conditions, we will be deleting your site after %day% %month%, %year% to save space on our hosting resources.</p>
<p>If you\'d like to give us some feedback on why you are not using your MoodleCloud site, please complete <a href="https://www.surveymonkey.com/r/moodlecloud-inactive">this short survey</a>.</p>

<p>Thanks for trying our service,</p>
<p>MoodleCloud team</p>';
$string['portal:tasks.removal.warning.plain'] = 'Hello!

We\'ve noticed that you have not been using your MoodleCloud site %fqdn% in the last 60 days.

As per our terms and conditions, we will be deleting your site after %removaldate% to save space on our hosting resources.

If you\'d like to give us some feedback on why you are not using your MoodleCloud site, please complete this short survey: https://www.surveymonkey.com/r/moodlecloud-inactive

Thanks for trying our service,
MoodleCloud team';
$string['portal:tasks.removal.warning.title'] = 'MoodleCloud site "%subdomain%" is inactive and will be deleted soon';
$string['portal:view.danger.modal.buttons.cancel'] = 'It was all a big misunderstanding!';
$string['portal:view.danger.modal.buttons.continue'] = 'Really, really remove my MoodleCloud site';
$string['portal:view.danger.modal.lead'] = 'By confirming below, you confirm that you have retrieved all content, and accept that your site will be permanently removed from our systems.';
$string['portal:view.danger.modal.title'] = 'Confirm removal';
$string['portal:view.danger.panel.body.buttons.remove'] = 'Remove my MoodleCloud site';
$string['portal:view.danger.panel.body.lead'] = '<p>Once you remove this MoodleCloud site, it really is gone.</p>
<p>Please be sure that you\'ve finished with it and have retrieved any content that you need.</p>';
$string['portal:view.danger.panel.body.title'] = 'Remove my MoodleCloud site';
$string['portal:view.danger.panel.title'] = 'Scary stuff';
$string['regions:ap-southeast-2'] = 'Australia';
$string['regions:eu-west-1'] = 'Ireland';
$string['regions:us-west-2'] = 'United States';
$string['signup:auditlog.account.created'] = 'New user account created';
$string['signup:auditlog.account.linked'] = 'Linked to existing account';
$string['signup:auditlog.account.validated'] = 'User account validation complete';
$string['signup:auditlog.account.validationstarted'] = 'User account validation message sent';
$string['signup:auditlog.site.automaticallyconfirmed'] = 'Site confirmed without user validation';
$string['signup:auditlog.site.provisionmailedout'] = 'Provisioning completion e-mail sent';
$string['signup:auditlog.site.provisionQueued'] = 'Site queued for provisioning';
$string['signup:auditlog.site.reserved'] = 'Site was reserved';
$string['signup:authcode.body.lead'] = 'We’ve just sent a verification code to your phone via SMS, you’ll need to enter it below. If you do not receive the SMS, please check our <a href="https://moodle.com/cloud/faq/#sms" target="_blank">FAQs</a> for possible reasons.';
$string['signup:authcode.body.links.startover'] = 'Want to start over? <a href="%link%">Let\'s go back to the beginning</a>.';
$string['signup:authcode.body.title'] = 'Confirm your phone number.';
$string['signup:authcode.form.fields.authcode'] = 'Authentication Code';
$string['signup:authcode.form.fields.continue'] = 'Continue';
$string['signup:authcode.form.placeholders.authcode'] = '123456';
$string['signup:errors.blacklistinline'] = 'Sorry, that site is not available.';
$string['signup:errors.invalidinline'] = 'Sorry, site names must be between 3 and 53 characters long and composed of the letters a-z, numbers and hyphens only.';
$string['signup:errors.maxsites'] = 'A user account already exists with a matching phone number. Users may only create a single MoodleCloud instance.';
$string['signup:errors.reserved'] = 'Oh no! Someone already took this one.';
$string['signup:messages.failedvalidationcode'] = 'Something\'s not quite right with that one. Did you enter it correctly?';
$string['signup:messages.passwordupdated'] = 'Great! We\'ve updated your password. You can use that to login to your MoodleCloud site.';
$string['signup:messages.validationcode'] = '%code% is your verification code for %subdomain%.moodlecloud.com';
$string['signup:page.breadcrumb'] = 'Signup';
$string['signup:page.description'] = 'Free Moodle learning software hosted in cloud servers for teachers and trainers to deploy Moodle sites in an instant';
$string['signup:page.title'] = 'MoodleCloud: Sign Up for Free Moodle Hosting';
$string['signup:password.body.lead'] = '';
$string['signup:password.body.restoredpassword'] = 'Great news! We found a password for your current MoodleCloud account. If you want to keep using this password, you can skip this section.';
$string['signup:password.body.title'] = 'Finally, set your own password!';
$string['signup:password.form.fields.password'] = 'New password';
$string['signup:password.form.fields.password_confirmation'] = 'And again, just to make sure';
$string['signup:password.form.fields.save'] = 'Save password';
$string['signup:password.form.placeholders.password'] = '';
$string['signup:password.form.placeholders.password_confirmation'] = '';
$string['signup:personal.body.lead'] = 'We just need to know who you are and make sure you agree to our Terms of Service.';
$string['signup:personal.body.title'] = 'Your site is available!';
$string['signup:personal.form.fields.continue'] = 'Continue';
$string['signup:personal.form.fields.email'] = 'Email address';
$string['signup:personal.form.fields.firstname'] = 'First name';
$string['signup:personal.form.fields.lastname'] = 'Family name';
$string['signup:personal.form.fields.phonenumber'] = 'Mobile/Cell number';
$string['signup:personal.form.fields.tandc'] = 'To continue, you need to first agree to our  Terms of Service.';
$string['signup:personal.form.fields.timezone'] = 'Your timezone';
$string['signup:personal.form.placeholders.email'] = '';
$string['signup:personal.form.placeholders.firstname'] = '';
$string['signup:personal.form.placeholders.lastname'] = '';
$string['signup:personal.form.placeholders.phonenumber'] = '';
$string['signup:personal.form.placeholders.timezone'] = 'Select timezone';
$string['signup:provision_mail.page.html'] = '<h1>Welcome to MoodleCloud!</h1>
<p>Your free Moodle site <a href="%url%">%url%</a> has now been created.</p>
<p>You can log into your Moodle site using the username "admin" and the password you set during signup.</p>
<p>Your free site is limited to 50 active user accounts and a 200Mb disk quota.</p>
<p>Tips:</p>
<ul>
  <li>You can delete unwanted users in the Site Administration to make room for more.</li>
  <li>To avoid having content count against your disk quota, use public resources that are available on the internet, such as web pages, Youtube videos and so on.</li>
</ul>
<p>To monitor your usage any time visit your MoodleCloud portal at moodlecloud.com. Log in using your site name "%subdomain%" and the same password as the one for your admin account on your site.</p>
<p>Regards,</p>
<p>MoodleCloud team</p>';
$string['signup:provision_mail.page.plain'] = 'Welcome to MoodleCloud!
Your free Moodle site %url% has now been created.
You can log into your Moodle site using the username "admin" and the password you set during signup.
Your free site is limited to 50 active user accounts and a 200Mb disk quota.
Tips:
  - You can delete unwanted users in the Site Administration to make room for more.
  - To avoid having content count against your disk quota, use public resources that are available on the internet, such as web pages, Youtube videos and so on.

To monitor your usage any time visit your MoodleCloud portal at moodlecloud.com. Log in using your site name "%subdomain%" and the same password as the one for your admin account on your site.

Regards,
MoodleCloud team';
$string['signup:provision_mail.page.title'] = 'Your MoodleCloud site %fqdn% is ready!';
$string['signup:provision_mail.subject'] = 'Your MoodleCloud site %site.fqdn% is ready!';
$string['signup:reserve.available'] = 'Huzzah! Your site name is available.';
$string['signup:reserve.body.lead'] = 'Choose your unique site name and select your location (this is where your site will be hosted, so we recommend a location closest to you and your students).';
$string['signup:reserve.body.title'] = 'Your MoodleCloud site is only a few clicks away!';
$string['signup:reserve.form.fields.continue'] = 'Get my site';
$string['signup:reserve.form.fields.region'] = 'Hosting location';
$string['signup:reserve.form.fields.subdomain'] = 'Site Name';
$string['signup:reserve.form.placeholders.subdomain'] = 'yoursite.moodlecloud.com';
$string['signup:reserve.form.tooltips.region'] = 'This is where your site will be hosted. We recommend choosing the location closest to you and your students.';
$string['signup:status.body.ariasetupcomplete'] = 'The setup of your new site has now completed. You can now log in.';
$string['signup:status.body.lead'] = '<p>Hey presto!</p>
<p>You wished it, now you have your very own Moodle.</p>
<p>Jump in and continue creating your perfect online learning space.</p>';
$string['signup:status.body.login'] = 'Log In';
$string['signup:status.body.title'] = 'All done!  Your very own Moodle site, free!';
$string['signup:steps.x'] = 'Step %x%';
$string['signup:steps.xofy'] = 'Step %x% of %y%';
$string['signup:terms.title'] = 'MoodleCloud Site Terms of Service';
$string['signup:texts.statusintro'] = 'TODO';
$string['signup:texts.statustitle'] = 'You wished for it. Your very own Moodle site, free!';
$string['status:label.expired'] = 'Removed due to inactivity';
$string['status:label.pendingIdentityConfirmation'] = 'Pending verification';
$string['status:label.pendingProvision'] = 'Queued for creation';
$string['status:label.pendingRemoval'] = 'Queued for removal';
$string['status:label.provisioned'] = 'Live';
$string['status:label.removed'] = 'Removed';
$string['status:label.reserved'] = 'Reserved';
$string['status:tooltip.expired'] = 'This site has been removed from our system due to inactivity';
$string['status:tooltip.pendingIdentityConfirmation'] = 'This site has been reserved, and a confirmation SMS has been sent.';
$string['status:tooltip.pendingProvision'] = 'This site has been sent off to our provisioning system and will be with you really really soon!';
$string['status:tooltip.pendingRemoval'] = 'This site has been queued for removal and will be removed from our systems soon.';
$string['status:tooltip.provisioned'] = 'This site is currently live and available.';
$string['status:tooltip.removed'] = 'This site has been removed.';
$string['status:tooltip.reserved'] = 'This site has been reserved but the signup process has not yet been completed.';
$string['telephony:lookup.apifailure'] = 'Erk nO! Something went wrong with our messaging system. Please try again in just a moment.';
$string['telephony:lookup.invalidnumber'] = 'Invalid phone number';
$string['telephony:lookup.notacellphone'] = 'You must provide a valid cell phone number';
$string['telephony:validator.apifailure'] = 'Erk nO! Something went wrong with our messaging system. Please try again in just a moment.';
$string['validators:fos_user.current_password.invalid'] = 'The entered password is invalid';
$string['validators:fos_user.email.already_used'] = 'The email is already used';
$string['validators:fos_user.email.blank'] = 'Please enter an email';
$string['validators:fos_user.email.invalid'] = 'The email is not valid';
$string['validators:fos_user.email.long'] = '[-Inf,Inf]The email is too long';
$string['validators:fos_user.email.short'] = '[-Inf,Inf]The email is too short';
$string['validators:fos_user.group.blank'] = 'Please enter a name';
$string['validators:fos_user.group.long'] = '[-Inf,Inf]The name is too long';
$string['validators:fos_user.group.short'] = '[-Inf,Inf]The name is too short';
$string['validators:fos_user.new_password.blank'] = 'Please enter a new password';
$string['validators:fos_user.new_password.short'] = '[-Inf,Inf]The new password is too short';
$string['validators:fos_user.password.blank'] = 'Please enter a password';
$string['validators:fos_user.password.mismatch'] = 'The entered passwords don\'t match';
$string['validators:fos_user.password.short'] = '[-Inf,Inf]The password is too short';
$string['validators:fos_user.username.already_used'] = 'The username is already used';
$string['validators:fos_user.username.blank'] = 'Please enter a username';
$string['validators:fos_user.username.long'] = '[-Inf,Inf]The username is too long';
$string['validators:fos_user.username.short'] = '[-Inf,Inf]The username is too short';
$string['validators:signup.email.format'] = 'Please enter a valid e-mail address';
$string['validators:signup.email.mustexist'] = 'Please enter your e-mail address';
$string['validators:signup.phone.mustexist'] = 'Please enter your mobile phone number';
$string['validators:signup.status.password_mismatch'] = 'The passwords you entered don\'t match';
$string['validators:signup.tandc.mustagree'] = 'In order to use the MoodleCloud, you must agree to the Terms of Service.';
$string['validators:signup.timezone.mustselect'] = 'Please select your timezone';
$string['validators:site.email.mustexist'] = 'You must specify your email';
$string['validators:site.email.validformat'] = 'Please specify your email address in a valid format';
$string['validators:site.firstname.mustexist'] = 'You must specify your first name';
$string['validators:site.lastname.mustexist'] = 'You must specify your last name';
$string['validators:site.subdomain.format'] = 'Subdomains should start with a letter. They must contain a combination of letters, numbers, and hyphens only.';
$string['validators:site.subdomain.maxlength'] = 'Subdomains must be no more than 53 characters long';
$string['validators:site.subdomain.minlength'] = 'Subdomains must be at least three characters in length';
$string['validators:site.subdomain.mustexist'] = 'You must specify a subdomain';
