<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'quizgame', language 'en', branch 'MOODLE_28_STABLE'
 *
 * @package   quizgame
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['emptyquiz'] = 'There are no multiple choice questions in the selected category.';
$string['endofgame'] = 'Your score was: {$a}. Press space or click to restart.';
$string['modulename'] = 'Quizventure';
$string['modulename_help'] = 'Use the Quizventure module to give the students a fun way of learning.';
$string['modulenameplural'] = 'Quizventure games';
$string['pluginadministration'] = 'Quizventure administration';
$string['pluginname'] = 'Quizventure';
$string['quizgame'] = 'Quizventure';
$string['quizgame:addinstance'] = 'Add a Quizventure instance';
$string['quizgamefieldset'] = 'Custom example fieldset';
$string['quizgamename'] = 'Quizventure name';
$string['quizgamename_help'] = 'What is the name of this Quizventure?';
$string['quizgame:view'] = 'View Quizventure';
$string['score'] = 'Score: {$a->score} Lives: {$a->lives}';
$string['sound'] = 'Sound';
$string['spacetostart'] = 'Press space or click to start';
