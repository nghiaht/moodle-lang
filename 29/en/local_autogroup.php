<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_autogroup', language 'en', branch 'MOODLE_28_STABLE'
 *
 * @package   local_autogroup
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addtonewcourses'] = 'Add to new courses';
$string['addtorestoredcourses'] = 'Add to restored courses';
$string['auth'] = 'Authentication Method';
$string['autogroupdescription'] = '"Autogroups" will automatically assign your users to groups within a course based upon information within their user profile.';
$string['autogroup:managecourse'] = 'Manage autogroup settings on course';
$string['cleanupold'] = 'Clean up old groups?';
$string['coursesettings'] = 'Auto Groups';
$string['coursesettingstitle'] = 'Auto Groups: {$a}';
$string['defaultroles'] = 'Default Eligible Roles';
$string['defaults'] = 'Default Settings';
$string['department'] = 'Department';
$string['dontgroup'] = 'Don\'t group users';
$string['enabled'] = 'Enabled';
$string['general'] = 'General Configuration';
$string['groupby'] = 'Group by';
$string['institution'] = 'Institution';
$string['lang'] = 'Preferred Language';
$string['newsettingsintro'] = 'To start grouping your users, simply select a profile field from the "Group by" option below and click "Save Changes".';
$string['pluginname'] = 'Auto Group';
$string['roles'] = 'Eligible Roles';
$string['strict'] = 'Strict Enforcement';
$string['strict_info'] = 'Monitor additional events such as "group member removed" to ensure that users are always in their correct groups. Enabling this option has a performance impact.';
$string['updatesettingsintro'] = 'This course is already grouping users by "{$a}". You can either change this to a new field or select "Don\'t group users". Remember, doing this will remove any older auto groups unless you select "no" for "Clean up old groups?"';
