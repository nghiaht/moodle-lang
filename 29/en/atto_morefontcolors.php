<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_morefontcolors', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   atto_morefontcolors
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['availablecolors'] = 'Available colors';
$string['availablecolors_desc'] = 'You may define available colors by listing them here, one hexcode per line. Note that changing these values only affects the appearance of the Font Color menu but does not affect existing texts.';
$string['custom'] = 'Custom font color (in hex)';
$string['customcolor'] = 'Custom font color';
$string['hexadecimal'] = 'Hexadecimal:';
$string['hsl'] = 'HSL:';
$string['luminance'] = 'Luminance:';
$string['pluginname'] = 'More font colors';
$string['rgb'] = 'RGB:';
$string['saturation'] = 'Saturation:';
$string['setting_custom'] = 'Allow custom color';
$string['setting_custom_desc'] = 'Allow the users to enter a custom font color in hex';
$string['submit'] = 'Submit';
