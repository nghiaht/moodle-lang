<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_table', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   atto_table
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['addcolumnafter'] = 'Insert column after';
$string['addrowafter'] = 'Insert row after';
$string['both'] = 'Both';
$string['caption'] = 'Caption';
$string['columns'] = 'Columns';
$string['createtable'] = 'Create table';
$string['deletecolumn'] = 'Delete column';
$string['deleterow'] = 'Delete row';
$string['edittable'] = 'Edit table';
$string['headers'] = 'Define headers on';
$string['movecolumnleft'] = 'Move column left';
$string['movecolumnright'] = 'Move column right';
$string['moverowdown'] = 'Move row down';
$string['moverowup'] = 'Move row up';
$string['numberofcolumns'] = 'Number of columns';
$string['numberofrows'] = 'Number of rows';
$string['pluginname'] = 'Table';
$string['rows'] = 'Rows';
$string['updatetable'] = 'Update table';
