<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'qtype_javaunittest', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   qtype_javaunittest
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accesscontrolexception'] = 'AccessControlException occurred.';
$string['arrayindexoutofboundexception'] = 'ArrayIndexOutOfBoundException occurred.';
$string['bufferoverflowexception'] = 'BufferOverflowException occurred.';
$string['bufferunderflowexception'] = 'BufferUnderflowError occurred.';
$string['CA'] = 'CORRECT ANSWER: All answers are correct.';
$string['CE'] = 'COMPILER ERROR: Your source code does not compile. Please check it for errors.';
$string['classcastexception'] = 'ClassCastException occurred.';
$string['compiling'] = 'compiling... [{$a}s]';
$string['crontask'] = 'Unittest feedback cleanup';
$string['debug_heading'] = 'Local debug information';
$string['debug_heading_desc'] = 'Enable only for testing! (only local java execution)';
$string['debug_logfile'] = 'Save logfiles to temporary directory';
$string['debug_nocleanup'] = 'Do not clean up temporary files and directories';
$string['feedback_all_except_stacktrace'] = 'all except stacktraces';
$string['feedbacklevel'] = 'Feedback';
$string['feedback_nothing'] = 'nothing';
$string['feedback_only_times'] = 'times';
$string['feedback_times_count_of_tests'] = 'times and count of tests/failures';
$string['filenotfoundexception'] = 'FileNotFoundException occurred.';
$string['givencode'] = 'Given code';
$string['givencode_help'] = 'Code which is provided by the instructor';
$string['graderinfo'] = 'Information for graders';
$string['ioexception'] = 'IOException occurred.';
$string['JE'] = 'JUNIT TEST FILE ERROR: Test cannot be executed. The given answer seems to be incompatible to the test class or the test class could not be compiled.';
$string['limit_heading'] = 'Resource limits';
$string['loadedtestclassheader'] = 'Load test file';
$string['local_execution_desc'] = 'If you want compiling and test running on this server you need to set the following.';
$string['local_execution_heading'] = 'Execution on this server';
$string['memory_limit_output'] = 'Memory limit (output) in KB';
$string['memory_limit_output_desc'] = 'Limits the size of outputs during test executions.';
$string['memory_xmx'] = 'Memory limit (heap) in MB';
$string['memory_xmx_desc'] = 'This sets the option -Xmx for the Java VM. Specifies the maximum size of the memory allocation pool.';
$string['negativearraysizeexception'] = 'NegativeArraySizeException occurred.';
$string['nlines'] = '{$a} lines';
$string['nullpointerexception'] = 'NullPointerException occurred.';
$string['outofmemoryerror'] = 'OutOfMemoryError occurred.';
$string['pathhamcrest'] = 'Path of hamcrest';
$string['pathjava'] = 'Path of java binary';
$string['pathjavac'] = 'Path of javac binary';
$string['pathjunit'] = 'Path of junit';
$string['pathpolicy'] = 'Path of the policy file for the Java Security Manager';
$string['PCA'] = 'PARTIALLY CORRECT ANSWER: Some answers are correct, some not.';
$string['pluginname'] = 'javaunittest';
$string['pluginnameadding'] = 'Adding an javaunittest question';
$string['pluginnameediting'] = 'Editing an javaunittest question';
$string['pluginname_help'] = 'JUnit question type';
$string['pluginname_link'] = 'question/type/javaunittest';
$string['pluginnamesummary'] = 'Allows a Java-code response which is evaluated by a defined JUnit test';
$string['precommand'] = 'Command before test execution';
$string['precommand_desc'] = 'This will be executed on shell before the tests. You may use ulimit to limit resources (e.g. cpu time) for the tests.';
$string['remote_execution_heading'] = 'Execution on other server';
$string['remoteserver'] = 'Remote URL';
$string['remoteserver_desc'] = 'Give a URL to a remote machine if you want remote compiling and test running. Otherwise leave this empty if you wish execution on this server. The remote server may override the settings for memory and time limit. (e.g. http://remoteserver/moodle_qtype_javaunittest/server.php)';
$string['remoteserver_password'] = 'Password';
$string['remoteserver_user'] = 'Username';
$string['responsefieldlines'] = 'Input box size';
$string['responseformat'] = 'Response format';
$string['running'] = 'running... [{$a}s]';
$string['stackoverflowerror'] = 'StackOverflowError occurred.';
$string['stringindexoutofboundexception'] = 'StringIndexOutOfBoundException occurred.';
$string['testclassname'] = 'JUnit test class name';
$string['testclassname_help'] = 'The JUnit class name of the following JUnit test code. The JUnit class name must be the same as the class name in the following "JUnit test code" section.';
$string['timeout_real'] = 'Timeout';
$string['timeout_real_desc'] = 'Timeout in seconds (real time) for test executions.';
$string['TO'] = 'TIMEOUT: The execution time limit is reached.';
$string['uploadtestclass'] = 'JUnit test class';
$string['uploadtestclass_help'] = 'Please put your JUnit testing code here.';
$string['WA'] = 'WRONG ANSWER: All answers are wrong.';
