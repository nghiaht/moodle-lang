<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_checkmarkreport', language 'en', branch 'MOODLE_28_STABLE'
 *
 * @package   local_checkmarkreport
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['additional_columns'] = 'Additional columns';
$string['additional_information'] = 'Additional information';
$string['additional_settings'] = 'Additional settings';
$string['by'] = 'by';
$string['checkmarkreport:view'] = 'View checkmark report';
$string['checkmarkreport:view_courseoverview'] = 'View checkmark coursereport';
$string['checkmarkreport:view_own_overview'] = 'View my checkmark report';
$string['checkmarkreport:view_students_overview'] = 'View checkmark userreport';
$string['error_retriefing_members'] = 'Error getting groupmembers';
$string['eventexported'] = 'Checkmarkreport exported';
$string['eventoverviewexported'] = 'Checkmarkreport overview exported';
$string['eventoverviewviewed'] = 'Checkmarkreport overview viewed';
$string['eventuseroverviewexported'] = 'Checkmarkreport useroverview exported';
$string['eventuseroverviewviewed'] = 'Checkmarkreport useroverview viewed';
$string['eventuserviewexported'] = 'Checkmarkreport userview exported';
$string['eventuserviewviewed'] = 'Checkmarkreport userview viewed';
$string['eventviewed'] = 'Checkmarkreport viewed';
$string['example'] = 'Example';
$string['examples'] = 'Examples';
$string['exportas'] = 'Export as';
$string['filter'] = 'Filter';
$string['grade'] = 'Grade';
$string['grade_help'] = 'Shows grade calculated by checked examples for the whole course as well as for each checkmark.';
$string['groupings'] = 'Groupings';
$string['groups'] = 'Groups';
$string['groups_help'] = 'Use this to select the groups to be displayed. Empty groups are disabled and only members of selected groups are displayed in the users selection (after update).';
$string['loading'] = 'Loading...';
$string['noaccess'] = 'You have no access to this module. You\'re missing the required capabilities to view this content.';
$string['overview'] = 'Overview';
$string['overview_alt'] = 'View checkmark coursereport';
$string['overwritten'] = 'Overwritten';
$string['pluginname'] = 'Checkmark report';
$string['pluginname_help'] = 'Checkmark reports extend the functionality of mod_checkmark by making convenient course-overview reports for all checkmarks available.';
$string['pluginnameplural'] = 'Checkmark reports';
$string['showgrade'] = 'Show grade';
$string['showpoints'] = 'Show points';
$string['showpoints_help'] = 'Show coresponding points for each example instead of checked (☒) or unchecked (☐) symbols.';
$string['status'] = 'Status';
$string['sumabs'] = 'Show x/y examples';
$string['sumabs_help'] = 'Show how many examples have been checked in the whole course and for each checkmark.';
$string['sumrel'] = 'Show % of examples/grades';
$string['sumrel_help'] = 'Show relative values ( XX % ) of checked examples as well as grade calculated by checked examples for course in total and each checkmark.';
$string['update'] = 'Update';
$string['useroverview'] = 'Student Overview';
$string['useroverview_alt'] = 'View checkmark userreport';
$string['userview'] = 'Userview';
$string['userview_alt'] = 'View my checkmark report';
$string['xlsover256'] = 'XLS file format only supports 256 columns at maximum. The file you\'re about to generate exceeds this limitation. Please deselect some instances or avoid using XLS.';
