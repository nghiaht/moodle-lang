<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'atto_pastespecial', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   atto_pastespecial
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['gdocCSS'] = 'Google Document CSS properties';
$string['gdocCSS_default'] = 'background-color,color,font-family,font-size,font-weight,font-style,text-decoration,list-style-type,text-align';
$string['gdocCSS_desc'] = 'Allowed CSS properties for pasting from Google Documents. See default for format.';
$string['libreCSS'] = 'LibreWriter CSS properties';
$string['libreCSS_default'] = 'background,color,font-size';
$string['libreCSS_desc'] = 'Allowed CSS properties for pasting from LibreWriter. See default for format.';
$string['otherCSS'] = 'Other CSS properties';
$string['otherCSS_desc'] = 'Allowed CSS properties for pasting from Other. See default of Word CSS for format.';
$string['paste'] = 'Paste';
$string['pastefromgdoc'] = 'Paste from Google Document';
$string['pastefromlibre'] = 'Paste from LibreWriter';
$string['pastefromother'] = 'Paste from Other';
$string['pastefromword'] = 'Paste from Microsoft Word';
$string['pastehere'] = 'Paste content here';
$string['pasteunformatted'] = 'Paste as unformatted text';
$string['pluginname'] = 'Paste special';
$string['settings'] = 'Paste special settings';
$string['wordCSS'] = 'Word CSS properties';
$string['wordCSS_default'] = 'font-family,font-size,background,color,background-color';
$string['wordCSS_desc'] = 'Allowed CSS properties for pasting from Microsoft Word. See default for format.';
