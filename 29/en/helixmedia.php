<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'helixmedia', language 'en', branch 'MOODLE_28_STABLE'
 *
 * @package   helixmedia
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['choosemedia_title'] = 'Choose Media';
$string['consumer_key'] = 'HML Consumer Key';
$string['consumer_key2'] = 'The consumer key used for access to the Helix Media Library LTI server.';
$string['helixmedia:addinstance'] = 'Add a new Helix Media Library Resource';
$string['helixmedia:manage'] = 'Manage Helix Media Library Resources';
$string['helixmediasummary'] = 'Summary';
$string['helixmediatext'] = 'Activity name';
$string['helixmedia:view'] = 'View Helix Media Library Resources';
$string['hml_in_new_window'] = 'Open Helix Media Resource';
$string['hml_in_new_window_message'] = 'If a new window doesn\'t open automatically containing the resource you wish to view, please use the link below to open it.';
$string['invalid_launch'] = 'Invalid parameters supplied for Helix Media Library LTI launch request. Aborting.';
$string['launch_url'] = 'HML LTI Launch URL';
$string['launch_url2'] = 'Put the LTI launch URL of the Helix Media Library here. This should be a URL in the format: https://upload.myhmlserver.org/Lti/Launch';
$string['migrate_do_button'] = 'Migrate Selected Content';
$string['migrate_finished'] = 'The migration process has finished. You can now exit this page.';
$string['migrate_found'] = 'All the URL modules linked to active Helix Media Repository instances are listed below:';
$string['migrate_from_course'] = 'from course';
$string['migrate_no_repo_mod'] = 'WARNING: The Helix Media Library Repository module is not installed, this is necessary in order to perform the migration process. Please install and configure the Repository module first.';
$string['migrate_not_found'] = 'The following URL modules are linked to inactive Helix Media Repository instances:';
$string['migrate_not_found_2'] = 'Note: This will have been caused by the removal/re-installation of the repository module.';
$string['migrate_not_found_3'] = 'The above content can still be migrated to the new activity module, but this process will only work if the currently installed repository module is still connecting to the same Helix Media Library installation which was used by the earlier inactive installations.';
$string['migrate_nothing_found'] = 'No URL module instances linking to the Helix Media Library have been found, all modules appear to have been migated.';
$string['migrate_ref_invalid'] = 'The linked video has an invalid reference ID. URL instance not migrated.';
$string['migrate_url_instance'] = 'Migrating URL instance:';
$string['migrate_vid_not_found'] = 'The linked video cannot be found. URL instance not migrated.';
$string['modal_delay'] = 'Video add dialog box close delay in seconds';
$string['modal_delay2'] = 'By default the modal dialogue box used to add videos will automatically close once the video has been chosen. You can optionally delay the closing of this dialogue by the number of seconds specified here, or disable the auto-close by setting this value to -1. Please note, this setting will have no effect on the modal dialogs used by the plugins for the TinyMCE and ATTO editors which will continue to remain open until closed by the user.';
$string['modulename'] = 'Media Library';
$string['modulename_help'] = 'The Helix Media Library provides a customised LTI based Moodle for the integration of HML media into Moodle';
$string['modulename_link'] = 'mod/helixmedia/view';
$string['modulenameplural'] = 'helix media';
$string['modulenamepluralformatted'] = 'Helix Media Library Instances';
$string['nohelixmedias'] = 'No Helix Media Library Library Instances found.';
$string['not_authorised'] = 'You are not authorised to perform this Helix Media Library operation.';
$string['org_id'] = 'Organisation ID';
$string['org_id2'] = 'The organisation ID or name which will be sent to the Helix Media Library. The URL of your HML installation will be sent if this is left blank.';
$string['pluginadministration'] = 'Media Library';
$string['pluginname'] = 'Media Library';
$string['repo_migrate_link'] = 'Open the Repository Module Migration Tool';
$string['repo_migrate_message'] = 'The repository module migration tool will convert content that was created using the Helix Media Library repository module so that it uses this activity module instead. Use the link below to start the migration process.';
$string['repo_migrate_title'] = 'Repository Module Migration';
$string['shared_secret'] = 'HML Shared Secret';
$string['shared_secret2'] = 'The shared secret used for comunications between Moodle and the Helix Media Library via LTI.';
