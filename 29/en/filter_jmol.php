<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'filter_jmol', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   filter_jmol
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['actinium'] = 'Actinium';
$string['actinoids'] = 'actinoids';
$string['allfeatures'] = 'All features';
$string['aluminium'] = 'Aluminium';
$string['americium'] = 'Americium';
$string['antialias'] = 'Antialias';
$string['antimony'] = 'Antimony';
$string['argon'] = 'Argon';
$string['arsenic'] = 'Arsenic';
$string['astatine'] = 'Astatine';
$string['atoms'] = 'Atoms';
$string['atoms_desc'] = 'Atom labels';
$string['axes'] = 'Axes';
$string['axes_desc'] = 'Crystallographic axes';
$string['backbone'] = 'Backbone';
$string['backbone_desc'] = 'Backbone only';
$string['backgroundcolour'] = 'Background colour';
$string['ballandstick'] = 'Ball and stick';
$string['barium'] = 'Barium';
$string['berkelium'] = 'Berkelium';
$string['beryllium'] = 'Beryllium';
$string['bismuth'] = 'Bismuth';
$string['blackbackground'] = 'Black background';
$string['bohrium'] = 'Bohrium';
$string['boron'] = 'Boron';
$string['bromine'] = 'Bromine';
$string['cadmium'] = 'Cadmium';
$string['caesium'] = 'Caesium';
$string['calcium'] = 'Calcium';
$string['californium'] = 'Californium';
$string['carbon'] = 'Carbon';
$string['cartoon'] = 'Cartoon';
$string['cartoon_desc'] = 'Secondary structure cartoon';
$string['cartoonsastrace'] = 'Cartoons as trace';
$string['cerium'] = 'Cerium';
$string['chlorine'] = 'Chlorine';
$string['chromium'] = 'Chromium';
$string['cobalt'] = 'Cobalt';
$string['colourscheme'] = 'Colour scheme';
$string['copernicium'] = 'Copernicium';
$string['copper'] = 'Copper';
$string['crystallography'] = 'Crystallography';
$string['curium'] = 'Curium';
$string['darmstadtium'] = 'Darmstadtium';
$string['dblock'] = 'd-block';
$string['display'] = 'Display';
$string['displayconsole'] = 'Display console';
$string['displaymenu'] = 'Display menu';
$string['displaytechnology'] = 'Display technology';
$string['dots'] = 'Dots';
$string['dots_desc'] = 'Dotted van der Waals surface';
$string['download'] = 'Download';
$string['downloadpngj'] = 'Download PNGJ structure + image file';
$string['downloadstructurefile'] = 'Download the chemical structure data file';
$string['dubnium'] = 'Dubnium';
$string['dysprosium'] = 'Dysprosium';
$string['einsteinium'] = 'Einsteinium';
$string['ellipsoidsasdots'] = 'Ellipsoids as dots';
$string['erbium'] = 'Erbium';
$string['europium'] = 'Europium';
$string['fblock'] = 'f-block';
$string['fermium'] = 'Fermium';
$string['filtername'] = 'Jmol';
$string['fluorine'] = 'Fluorine';
$string['francium'] = 'Francium';
$string['fullscreen'] = 'Fullscreen';
$string['gadolinium'] = 'Gadolinium';
$string['gallium'] = 'Gallium';
$string['geosurfaceasdots'] = 'Geosurface as dots';
$string['germanium'] = 'Germanium';
$string['gold'] = 'Gold';
$string['group'] = 'Group';
$string['group1'] = 'Group1, the alkali metals';
$string['group10'] = 'Group10, the nickel group';
$string['group11'] = 'Group11, the copper group, coinage metals';
$string['group12'] = 'Group12, the zinc group';
$string['group13'] = 'Group13, the boron group';
$string['group14'] = 'Group14, the carbon group';
$string['group15'] = 'Group15, the pnictogens, nitrogen group';
$string['group16'] = 'Group16, the chalcogens, oxygen group';
$string['group17'] = 'Group17, the halogens';
$string['group18'] = 'Group18, the noble gases';
$string['group2'] = 'Group2, the alkaline earth metals';
$string['group3'] = 'Group3, the scandium group including the lanthanoids and actinoids';
$string['group4'] = 'Group4, the titanium group';
$string['group5'] = 'Group5, the vanadium group';
$string['group6'] = 'Group6, the chromium group';
$string['group7'] = 'Group7, the manganese group';
$string['group8'] = 'Group8, the iron group';
$string['group9'] = 'Group9, the cobalt group';
$string['hafnium'] = 'Hafnium';
$string['hassium'] = 'Hassium';
$string['hbonds'] = 'H&nbsp;bonds';
$string['hbonds_desc'] = 'Hydrogen bonds';
$string['helium'] = 'Helium';
$string['help'] = 'Help';
$string['holmium'] = 'Holmium';
$string['hydrogen'] = 'Hydrogen';
$string['hydrogens'] = 'Hydrogens';
$string['indium'] = 'Indium';
$string['iodine'] = 'Iodine';
$string['iridium'] = 'Iridium';
$string['iron'] = 'Iron';
$string['jmolfiletypes'] = 'Jmol file types';
$string['jmolfiletypes_desc'] = 'Description of Jmol file types';
$string['jmolhelp'] = 'Jmol help';
$string['jsdisabled'] = 'The Jmol/JSmol 3D chemical structure viewer uses JavaScript. You may need to enable JavaScript in your Web browser. If you are unable to make use of this technology, you can still use the download link (below) to retrieve the chemical structure file.';
$string['jsmol'] = 'JSmol';
$string['jsmol_desc'] = 'JSmol';
$string['jsmol_help'] = 'Click on cover image to activate';
$string['jsmol_link'] = 'http://google.co.uk';
$string['krypton'] = 'Krypton';
$string['labels'] = 'Labels';
$string['lanthanoids'] = 'lanthanoids';
$string['lanthanum'] = 'Lanthanum';
$string['lawrencium'] = 'Lawrencium';
$string['lead'] = 'Lead';
$string['lightgreybackground'] = 'Light grey background';
$string['lithium'] = 'Lithium';
$string['loading'] = 'Loading...';
$string['lutetium'] = 'Lutetium';
$string['magnesium'] = 'Magnesium';
$string['managanese'] = 'Managanese';
$string['meitnerium'] = 'Meitnerium';
$string['mendelevium'] = 'Mendelevium';
$string['mercury'] = 'Mercury';
$string['molecular'] = 'Molecular';
$string['molybdenum'] = 'Molybdenum';
$string['neodymium'] = 'Neodymium';
$string['neon'] = 'Neon';
$string['neptunium'] = 'Neptunium';
$string['nickel'] = 'Nickel';
$string['niobium'] = 'Niobium';
$string['nitrogen'] = 'Nitrogen';
$string['noantialiasing'] = 'No antialiasing';
$string['nobelium'] = 'Nobelium';
$string['notranslucency'] = 'No translucency';
$string['off'] = 'Off';
$string['off_desc'] = 'Labels off';
$string['osmium'] = 'Osmium';
$string['oxygen'] = 'Oxygen';
$string['palladium'] = 'Palladium';
$string['pblock'] = 'p-block';
$string['performance'] = 'Performance';
$string['phosphorus'] = 'Phosphorus';
$string['platinum'] = 'Platinum';
$string['pluginname'] = 'Jmol';
$string['plutonium'] = 'Plutonium';
$string['polonium'] = 'Polonium';
$string['polyhedra'] = 'Polyhedra';
$string['potassium'] = 'Potassium';
$string['praseodymium'] = 'Praseodymium';
$string['primary'] = 'Primary';
$string['primary_desc'] = 'Colour primary structure (residue sequence)';
$string['promethium'] = 'Promethium';
$string['protoactinium'] = 'Protoactinium';
$string['quaternary'] = 'Quaternary';
$string['quaternary_desc'] = 'Colour quaternary structure';
$string['radium'] = 'Radium';
$string['radon'] = 'Radon';
$string['residues'] = 'Residues';
$string['residues_desc'] = 'Residue labels';
$string['rhenium'] = 'Rhenium';
$string['rhodium'] = 'Rhodium';
$string['ribbon'] = 'Ribbon';
$string['roentgenium'] = 'Roentgenium';
$string['rubidium'] = 'Rubidium';
$string['ruthenium'] = 'Ruthenium';
$string['rutherfordium'] = 'Rutherfordium';
$string['samarium'] = 'Samarium';
$string['sblock'] = 's-block';
$string['scandium'] = 'Scandium';
$string['seaborgium'] = 'Seaborgium';
$string['secondary'] = 'Secondary';
$string['secondary_desc'] = 'Colour secondary structure';
$string['selenium'] = 'Selenium';
$string['silicon'] = 'Silicon';
$string['silver'] = 'Silver';
$string['sodium'] = 'Sodium';
$string['spacefill'] = 'Spacefill';
$string['spacefill_desc'] = 'van der Waals radius';
$string['spin'] = 'Spin';
$string['ssbonds'] = 'SS&nbsp;bonds';
$string['ssbonds_desc'] = 'Disuphide bonds';
$string['stick'] = 'Stick';
$string['sticks'] = 'Sticks';
$string['strontium'] = 'Strontium';
$string['style'] = 'Style';
$string['sulphur'] = 'Sulphur';
$string['surface'] = 'Surface';
$string['surface_desc'] = 'van der Waals surface';
$string['surfacesdotted'] = 'Surfaces dotted';
$string['tantalum'] = 'Tantalum';
$string['taskcleanjmoltemp'] = 'Clean Jmol filter temp files';
$string['technetium'] = 'Technetium';
$string['tellurium'] = 'Tellurium';
$string['terbium'] = 'Terbium';
$string['termini'] = 'Termini';
$string['termini_desc'] = 'Termini labels';
$string['tertiary'] = 'Tertiary';
$string['tertiary_desc'] = 'Colour tertiary structure';
$string['thallium'] = 'Thallium';
$string['thorium'] = 'Thorium';
$string['thulium'] = 'Thulium';
$string['tin'] = 'Tin';
$string['titanium'] = 'Titanium';
$string['togglefullscreen'] = 'Toggle fullscreen';
$string['tungsten'] = 'Tungsten';
$string['unitcell'] = 'Unit&nbsp;cell';
$string['unitcell_desc'] = 'Crystallographic unit cell';
$string['uranium'] = 'Uranium';
$string['use'] = 'Display technology';
$string['use_desc'] = 'Description of display technologyl';
$string['vanadium'] = 'Vanadium';
$string['whitebackground'] = 'White background';
$string['wireframe'] = 'Wireframe';
$string['wireframeonly'] = 'Wireframe only';
$string['xenon'] = 'Xenon';
$string['ytterbium'] = 'Ytterbium';
$string['yttrium'] = 'Yttrium';
$string['zinc'] = 'Zinc';
$string['zirconium'] = 'Zirconium';
