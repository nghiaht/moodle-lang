<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'accredible', language 'en', branch 'MOODLE_27_STABLE'
 *
 * @package   accredible
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['accredible:addinstance'] = 'Add a certificate instance';
$string['accredible:manage'] = 'Manage a certificate instance';
$string['accredible:student'] = 'Retrieve a certificate';
$string['accredible:view'] = 'View a certificate';
$string['achievementid'] = 'Achievement id (must be unique)';
$string['activityname'] = 'Activity name';
$string['additionalactivitiesone'] = 'Warning: You are adding more than one activity to a course.<br/>Both activities are viewable by students, so be sure to give them different names.';
$string['additionalactivitiesthree'] = 'This is the name that will appear on the ceriticate.';
$string['additionalactivitiestwo'] = 'Certificates will only be listed on the activity page if they were issued with this achievement id.';
$string['apikeyhelp'] = 'Enter your API key from accredible.com';
$string['apikeylabel'] = 'API key';
$string['autoissueheader'] = 'Automatic issuing criteria';
$string['certificatename'] = 'Certificate name';
$string['certificateurl'] = 'Certificate url';
$string['chooseexam'] = 'Choose final quiz';
$string['completionissueheader'] = 'Auto-issue criteria: by activity completion';
$string['dashboardlink'] = 'Accredible dashboard link';
$string['dashboardlinktext'] = 'To delete or style credentials, log in to the <a href="https://accredible.com/issuer/login" target="_blank">dashboard</a>';
$string['datecreated'] = 'Date created';
$string['description'] = 'Description';
$string['eventcertificatecreated'] = 'A certificate was posted to Accredible';
$string['gradeissueheader'] = 'Auto-issue criteria: by final quiz grade';
$string['id'] = 'ID';
$string['indexheader'] = 'All certificates for {$a}';
$string['issued'] = 'Issued';
$string['manualheader'] = 'Manually issue certificates';
$string['modulename'] = 'Accredible certificate';
$string['modulename_help'] = 'The Accredible certificates activity module allows you to issue course certificates to students on accredible.com.

Add the activity wherever you want your students see their certificate.';
$string['modulename_link'] = 'mod/accredible/view';
$string['modulenameplural'] = 'Accredible certificates';
$string['nocertificates'] = 'There are no certificates';
$string['passinggrade'] = 'Percentage grade needed to pass course (%)';
$string['pluginadministration'] = 'Accredible certificates administration';
$string['pluginname'] = 'Accredible certificates';
$string['recipient'] = 'Recipient';
$string['viewheader'] = 'Certificates for {$a}';
$string['viewimgcomplete'] = 'Click to view your certificate';
$string['viewimgincomplete'] = 'Course still in progress';
$string['viewsubheader'] = 'Achievement id: {$a}';
