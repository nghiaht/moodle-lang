<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_exacomp', language 'en', branch 'MOODLE_28_STABLE'
 *
 * @package   block_exacomp
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['allcourses'] = 'all courses';
$string['all_modules'] = 'all activities';
$string['all_niveaus'] = 'all levels';
$string['allstudents'] = 'All students';
$string['alltopics'] = 'All topics';
$string['apply_filter'] = 'apply filter';
$string['April'] = 'April';
$string['assigndone'] = 'task done:';
$string['assigned_example'] = 'Assigned Example';
$string['assignfrom'] = 'from';
$string['assignlearninggroup'] = 'peer group';
$string['assignlearningpartner'] = 'peer-to-peer';
$string['assignmyself'] = 'by myself';
$string['assignteacher'] = 'trainer';
$string['assignuntil'] = 'until';
$string['attachement_example'] = 'Attachement';
$string['August'] = 'August';
$string['choosestudent'] = 'Choose student:';
$string['choosesubject'] = 'Choose subject:';
$string['choosetopic'] = 'Choose topic';
$string['city'] = 'City';
$string['columnselect'] = 'Column selection';
$string['column_setting'] = 'hide/display columns';
$string['comp'] = 'Topic';
$string['competencegrid_nodata'] = 'In case the competency grid is empty the outcomes for the chosen subject were not assigned to a level in the datafile. This can be fixed by associating outcomes with levels at www.edustandards.org and re-importing the xml-file.';
$string['comp_field_idea'] = 'Skill';
$string['completed_config'] = 'The configuration of Exabis Competencies is completed.';
$string['conf_badges'] = 'configure badges';
$string['conf_comps'] = 'configure competences';
$string['contactcomps'] = 'contract all';
$string['course'] = 'Course';
$string['dataerr'] = 'At least a link or a file are required!';
$string['December'] = 'December';
$string['delete_confirmation'] = 'Do you really want to delete this example?';
$string['description_edit_badge_comps'] = 'Here you can associate the selected badge with outcomes.';
$string['descriptors'] = 'outcomes/standards';
$string['descriptors_help'] = 'You can select multible outcomes/standards';
$string['detail_description'] = 'Use Moodle activities to evaluate competencies.';
$string['do_demo_import'] = 'import demo data to see how Exabis Competencies works.';
$string['doimport'] = 'import outcomes/standards';
$string['doimport_again'] = 're-import outcomes/standards';
$string['doimport_own'] = 'import individual outcomes/standards';
$string['eportitem_notshared'] = '(not shared)';
$string['eportitems'] = 'This participant has submitted the following ePortfolio artifacts:';
$string['eportitem_shared'] = '(shared)';
$string['exacomp:addinstance'] = 'Add a exabis competencies block';
$string['exacomp:admin'] = 'overview of administrator actions in a course';
$string['exacomp:deleteexamples'] = 'delete examples';
$string['exacomp:myaddinstance'] = 'Add a exabis competencies block to my moodle';
$string['exacomp:student'] = 'overview of student actions in a course';
$string['exacomp:teacher'] = 'overview of trainer actions in a course';
$string['exacomp:use'] = 'use Exabis Competencies';
$string['example_upload_header'] = 'Upload my own task/example';
$string['expandcomps'] = 'expand all';
$string['explainconfig'] = 'Your outcomes have already been imported. In this configuration you have to make the selection of the main standards you would like to use in this Moodle installation.';
$string['explaineditactivities_subjects'] = '';
$string['extern_task'] = 'External Task';
$string['February'] = 'February';
$string['filerequired'] = 'A file must be selected.';
$string['filteredtaxonomies'] = 'Examples are filtered accordingly to the following taxonomies:';
$string['first_configuration_step'] = 'The first step of the configuration is to import some data to Exabis Competencies.';
$string['forwhat'] = 'Wof&uuml;r du das brauchst:';
$string['gained'] = 'gained';
$string['grading'] = 'Grading:';
$string['grading_scheme'] = 'grading scheme';
$string['help_content'] = '<h1>Introduction Video</h1>
<iframe width="640" height="360" src="//www.youtube.com/embed/EL4Vb3_17EM?feature=player_embedded" frameborder="0" allowfullscreen></iframe>';
$string['hideevaluation'] = 'To hide self-assessment click <a href="{$a}">here</a>';
$string['hideevaluation_student'] = 'To hide trainer-assessment click <a href="{$a}">here</a>';
$string['howtocheck'] = 'Wie du dein K&ouml;nnen pr&uuml;fen kannst:';
$string['importdone'] = 'data has already been imported from xml';
$string['importfail'] = 'an error has occured during import';
$string['importinfo'] = 'Please create your outcomes/standards at <a href="http://www.edustandards.org">www.edustandards.org</a> or visit <a href="http://www.github.com/gtn/edustandards">github.com/gtn/edustandards</a> to download an available xml file.';
$string['importpending'] = 'no data has been imported yet!';
$string['importsuccess'] = 'data was successfully imported!';
$string['importsuccess_own'] = 'individual data was imported successfully!';
$string['importwebservice'] = 'It is possible to keep the data up to date via a <a href="{$a}">webservice</a>.';
$string['infolink'] = 'Additional information:';
$string['instruction'] = 'Instruction';
$string['instruction_content'] = 'This is an overview for learning resources that are associated with
				standards and ticking off competencies for students. Students can
				assess their competencies. Moodle activities that were turned in by
				students are displayed with a red icon. ePortfolio-artifacts of students
				are displayed in blue icons.';
$string['item_file'] = 'File';
$string['item_link'] = 'Link';
$string['item_no_comps'] = 'There are no outcomes assigned to the following items:';
$string['item_note'] = 'Note';
$string['item_title'] = 'Title';
$string['item_type'] = 'Type';
$string['item_url'] = 'URL';
$string['January'] = 'January';
$string['July'] = 'July';
$string['June'] = 'June';
$string['LA_assessment'] = 'assessment';
$string['LA_backtoview'] = 'back to original view';
$string['LA_enddate'] = 'end date';
$string['LA_FRI'] = 'FRI';
$string['LA_from_m'] = 'from';
$string['LA_from_n'] = 'from';
$string['LA_learning'] = 'What can I learn?';
$string['LA_MON'] = 'MON';
$string['LA_no_example'] = 'no example available';
$string['LA_no_learningagenda'] = 'There is no learning agenda available for this week.';
$string['LA_no_student_selected'] = '-- no student selected --';
$string['LA_plan'] = 'working plan';
$string['LA_select_student'] = 'Please select a student to view his learning agenda.';
$string['LA_startdate'] = 'start date';
$string['LA_student'] = 'S';
$string['LA_teacher'] = 'T';
$string['LA_THU'] = 'THU';
$string['LA_to'] = 'to';
$string['LA_todo'] = 'What do I do?';
$string['LA_TUE'] = 'TUE';
$string['LA_WED'] = 'WED';
$string['legend_activities'] = 'Moodle activities';
$string['legend_eportfolio'] = 'ePortfolio';
$string['legend_notask'] = 'no Moodle activities/quiz has been submitted for this outcome';
$string['legend_upload'] = 'Upload your own task/example';
$string['link'] = 'Link';
$string['linkerr'] = 'The given link is not valid!';
$string['lisfilename'] = 'Use LIS filename template';
$string['March'] = 'March';
$string['May'] = 'May';
$string['module_filter'] = 'filter activities';
$string['mybadges'] = 'My badges';
$string['my_badges'] = 'My Badges';
$string['my_comps'] = 'My Competencies';
$string['my_items'] = 'My artifacts';
$string['my_periods'] = 'My assessments';
$string['name'] = 'Name';
$string['name_example'] = 'Name';
$string['next_step'] = 'This configuration step has been completed. Click here to continue configuration.';
$string['next_step_teacher'] = 'The configuration that has to be done by the administrator is now completed. To continue with the course specific configuration click here.';
$string['niveau_filter'] = 'filter levels';
$string['no_activities_selected'] = 'please associate Moodle activities with competences';
$string['no_activities_selected_student'] = 'There is no data available yet';
$string['no_badges_yet'] = 'no badges available';
$string['no_course_activities'] = 'No Moodle activities found in this course - click here to create some.';
$string['nodata'] = 'There is no data do display';
$string['no_topics_selected'] = 'configuration of exabis competencies is not completed yet. please chose a topic that you would like to associate Moodle activities with';
$string['November'] = 'November';
$string['noxmlfile'] = 'There is no data available to import. Please visit <a href="https://github.com/gtn/edustandards">https://github.com/gtn/edustandards</a> to download the required outcomes to the blocks xml directory.';
$string['October'] = 'October';
$string['oldxmlfile'] = 'You are using an outdated xml-file. Please create new outcomes/standards at <a href="http://www.edustandards.org">www.edustandards.org</a> or visit <a href="http://www.github.com/gtn/edustandards">github.com/gtn/edustandards</a> to download an available xml file to the blocks xml directory.';
$string['optional_step'] = 'There are no participants in your course yet. If you want to enrol some please use this link.';
$string['overview'] = 'This is an overview of all students and the course competencies.';
$string['pendingbadges'] = 'Pending badges';
$string['pendingcomp'] = 'pending competencies';
$string['period_feedback'] = 'Feedback';
$string['period_reviewer'] = 'Reviewer';
$string['pluginname'] = 'Exabis Competencies';
$string['profile_settings_badges_lineup'] = 'Badges settings';
$string['profile_settings_choose_courses'] = 'Using Exabis Competencies trainers assess your competencies in various subjects. You can select which course to include in the competence profile.';
$string['profile_settings_choose_items'] = 'Exabis ePortfolio is used to document your competencies on your individual learning path. You can select which artifacts to include in the competence profile.';
$string['profile_settings_choose_periods'] = 'Exabis Student Review stores reviews in various categories over several periods. You can select which periods to include in the competence profile.';
$string['profile_settings_no_item'] = 'No Exabis ePortfolio item available, so there is nothing to display.';
$string['profile_settings_no_period'] = 'No review in a period in Exabis Student Review available.';
$string['profile_settings_onlygainedbadges'] = 'I don\'t want to see pending badges.';
$string['profile_settings_showallcomps'] = 'all my competencies';
$string['profile_settings_showonlyreached'] = 'I only want to see already gained outcomes in my competence profile';
$string['profile_settings_usebadges'] = 'I want to see badges in my competence profile.';
$string['profile_settings_useexaport'] = 'I want to see competencies used in Exabis ePortfolio within my profile.';
$string['profile_settings_useexastud'] = 'I want to see evaluations from Exabis Student Review.';
$string['profoundness_0'] = 'not reached';
$string['profoundness_1'] = 'partially gained';
$string['profoundness_2'] = 'fully gained';
$string['profoundness_basic'] = 'Basic competence';
$string['profoundness_description'] = 'Description';
$string['profoundness_entirely'] = 'Entirely achieved';
$string['profoundness_extended'] = 'Extended competence';
$string['profoundness_mainly'] = 'Mainly achieved';
$string['progress'] = 'Progress';
$string['radargrapherror'] = 'Radargraph can only be displayed with 3-7 axis';
$string['reached_topic'] = 'Ich habe diese Kompetenz erreicht:';
$string['ready_to_activate'] = 'This badge is ready to be activated:';
$string['requirements'] = 'Was du schon k&ouml;nnen solltest:';
$string['save_selection'] = 'Save selection';
$string['save_success'] = 'changes were successful';
$string['second_configuration_step'] = 'In this configuration step you have to pre-select standards.';
$string['selectall'] = 'Select all';
$string['select_student'] = 'Please select a student first';
$string['selfevaluation'] = 'Self assessment';
$string['September'] = 'September';
$string['settings_alternativedatamodel'] = 'Baden W&uuml;rttemberg Version';
$string['settings_alternativedatamodel_description'] = 'Tick to use Baden W&uuml;rttemberg Version';
$string['settings_autotest'] = 'Automatical gain of competence through quizzes';
$string['settings_autotest_description'] = 'Competences that are associated with quizzes are gained automatically if needed percentage of quiz is reached';
$string['settings_configxmlserverurl'] = 'Url to a xml file, which is used for keeping the database entries up to date';
$string['settings_enableteacherimport'] = 'Use school specific standards';
$string['settings_enableteacherimport_description'] = 'Check to enable school specific standard import for trainers';
$string['settings_skillmanagement'] = 'Use skills-management';
$string['settings_skillmanagement_description'] = 'Check to use skills-management-tool instead of import';
$string['settings_testlimit'] = 'Quiz-percentage needed to gain competence';
$string['settings_testlimit_description'] = 'This percentage has to be reached to gain the competence';
$string['settings_usebadges'] = 'Use badges';
$string['settings_usebadges_description'] = 'Check to associate badges with competences';
$string['settings_xmlserverurl'] = 'Server-URL';
$string['show_all_course_examples'] = 'Show examples from all courses';
$string['show_all_descriptors'] = 'Show all outcomes in overview';
$string['show_all_examples'] = 'Show external examples for students';
$string['show_all_taxonomies'] = 'All taxonomies';
$string['showevaluation'] = 'To show self-assessment click <a href="{$a}">here</a>';
$string['showevaluation_student'] = 'To show trainer-assessment click <a href="{$a}">here</a>';
$string['solution'] = 'Solution';
$string['solution_example'] = 'Solution';
$string['sorting'] = 'select sorting:';
$string['specificcontent'] = 'site-specific topics';
$string['specificsubject'] = 'site-specific subjects';
$string['studentcomp'] = 'self evaluated competencies';
$string['studentshortcut'] = 'S';
$string['subject'] = 'subjects';
$string['subject_singular'] = 'Field of competence';
$string['tab_admin_configuration'] = 'Standards pre-selection';
$string['tab_admin_import'] = 'Import';
$string['tab_badges'] = 'My badges';
$string['tab_competence_details'] = 'Detailed competence-view';
$string['tab_competence_grid'] = 'Competence grid';
$string['tab_competence_overview'] = 'Overview of competencies';
$string['tab_competence_profile'] = 'Competence profile';
$string['tab_competence_profile_profile'] = 'Profile';
$string['tab_competence_profile_settings'] = 'Settings';
$string['tab_examples'] = 'Examples and tasks';
$string['tab_help'] = 'Help';
$string['tab_learning_agenda'] = 'Learning agenda';
$string['tab_profoundness'] = 'Basic & Extended Competencies';
$string['tab_skillmanagement'] = 'Manage competencies';
$string['tab_student_all'] = 'All gained competencies';
$string['tab_teacher_demo_settings'] = 'work with demo data';
$string['tab_teacher_settings'] = 'Settings';
$string['tab_teacher_settings_assignactivities'] = 'Assign Moodle activities';
$string['tab_teacher_settings_badges'] = 'Edit badges';
$string['tab_teacher_settings_configuration'] = 'Configuration';
$string['tab_teacher_settings_selection'] = 'Subject selection';
$string['tab_teacher_settings_selection_st'] = 'Schooltype selection';
$string['task_example'] = 'Tasks';
$string['taxonomies'] = 'taxonomies';
$string['taxonomy'] = 'taxonomy';
$string['teachercomp'] = 'gained competencies';
$string['teacherevaluation'] = 'Trainer assessment';
$string['teacher_first_configuration_step'] = 'The first step of course configuration is to adjust general settings for your course.';
$string['teacher_second_configuration_step'] = 'In the second configuration step topics to work with in this course have to be selected.';
$string['teachershortcut'] = 'T';
$string['teacher_third_configuration_step'] = 'The next step is to associate Moodle activities with competencies';
$string['teacher_third_configuration_step_link'] = '(optional: if you don\'t want to work with activities untick the setting "I want to work with Moodle activities" in the tab "Configuration")';
$string['teacher_tipp'] = 'tip';
$string['teacher_tipp_1'] = 'This competence has been associated with';
$string['teacher_tipp_2'] = 'Moodle activities and has been reached with';
$string['teacher_tipp_3'] = 'outcomes.';
$string['textalign'] = 'Switch text align';
$string['tick_some'] = 'Please make a selection!';
$string['titlenotemtpy'] = 'A name is required.';
$string['to_award'] = 'To award this badge in exacomp you have to configure competencies';
$string['to_award_role'] = 'To award this badge in exacomp you have to add the "Manual issue by role" criteria';
$string['total'] = 'total';
$string['total_example'] = 'Complete Example';
$string['usedetailpage'] = 'Use detailed overview of competencies';
$string['useprofoundness'] = 'Use basic and extended competencies';
$string['usernosubmission'] = 'has not yet submitted any Moodle activities or quizzes associated with this outcome';
$string['usernosubmission_topic'] = 'has not yet submitted any Moodle activities or quizzes associated with this topic';
$string['usersubmitted'] = 'has submitted the following Moodle activities:';
$string['usersubmittedquiz'] = 'has done the following quizzes:';
$string['uses_activities'] = 'I work with Moodle activites';
$string['warning_use_activities'] = 'Warning: you are now working with Moodle-activites that are associated with competences. Please verify that the same outcomes are used as before.';
