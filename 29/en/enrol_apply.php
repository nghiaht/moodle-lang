<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_apply', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   enrol_apply
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['apply:config'] = 'Configure apply enrol instances';
$string['applydate'] = 'Enrol date';
$string['applymanage'] = 'Manage enrolment applications';
$string['apply:manage'] = 'Manage apply enrolment';
$string['apply:unenrol'] = 'Cancel users from the course';
$string['apply:unenrolapply'] = 'Cancel self from the course';
$string['apply:unenrolself'] = 'Cancel self from the course';
$string['applyuser'] = 'First name / Surname';
$string['applyusermail'] = 'Email';
$string['btncancel'] = 'Cancel';
$string['btnconfirm'] = 'Confirm';
$string['cancelmailcontent'] = 'Cancelation email content';
$string['cancelmailcontent_desc'] = 'Please use the following special marks to replace email content with data from Moodle.<br/>{firstname}:The first name of the user; {content}:The course name;{lastname}:The last name of the user;{username}:The users registration username';
$string['cancelmailsubject'] = 'Cancelation email subject';
$string['comment'] = 'Comment';
$string['confirmenrol'] = 'Manage application';
$string['confirmmailcontent'] = 'Confirmation email content';
$string['confirmmailcontent_desc'] = 'Please use the following special marks to replace email content with data from Moodle.<br/>{firstname}:The first name of the user; {content}:The course name;{lastname}:The last name of the user;{username}:The users registration username';
$string['confirmmailsubject'] = 'Confirmation email subject';
$string['confirmusers'] = 'Enrol Confirm';
$string['coursename'] = 'Course';
$string['editdescription'] = 'Textarea description';
$string['enrolname'] = 'Course enrol confirmation';
$string['enrolusers'] = 'Enrol users';
$string['mailtoteacher_suject'] = 'New Enrolment request!';
$string['notification'] = '<b>Enrolment application successfully sent</b>. <br/><br/>You will be informed by email when your enrolment has been confirmed.';
$string['pluginname'] = 'Course enrol confirmation';
$string['pluginname_desc'] = 'With this plug-in users can apply to be enrolled in a course. A teacher or site manager will then have to approve the enrolment before the user gets enroled.';
$string['sendmailtomanager'] = 'Send email notification to managers';
$string['sendmailtoteacher'] = 'Send email notification to teachers';
$string['show_extra_user_profile'] = 'Show extra user profile fields on enrolment screen';
$string['show_standard_user_profile'] = 'Show standard user profile fields on enrolment screen';
$string['status'] = 'Allow Course enrol confirmation';
$string['status_desc'] = 'Allow course access of internally enrolled users.';
$string['user_profile'] = 'User Profile';
