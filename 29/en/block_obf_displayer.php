<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_obf_displayer', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   block_obf_displayer
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['blocktitle'] = 'My Open Badges';
$string['blocktitlemoz'] = 'Backpack badges';
$string['blocktitleobf'] = 'Badges';
$string['blocktitleobp'] = 'Open Badge Passport badges';
$string['cachedef_obf_assertions'] = 'OBF badge assertion cache';
$string['cachedef_obf_assertions_moz'] = 'Mozilla Backpack badge cache';
$string['cachedef_obf_assertions_obp'] = 'Open Badge Passport badge cache';
$string['largebadges'] = 'Use large badges';
$string['obf_displayer'] = 'Open Badge Factory';
$string['obf_displayer:addinstance'] = 'Add a new Open Badge Factory badges block';
$string['obf_displayer:myaddinstance'] = 'Add a new Open Badge Factory badges block to the My Moodle page';
$string['pluginname'] = 'Open Badge Factory Badges block';
$string['providerselect'] = 'Open Badges that will be shown';
$string['showmoz'] = 'Mozilla Backpack Badges';
$string['showobf'] = 'Badges awarded for courses on this site';
$string['showobp'] = 'Open Badge Passport Badges';
