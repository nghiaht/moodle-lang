<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'local_intelliboard', language 'en', branch 'MOODLE_29_STABLE'
 *
 * @package   local_intelliboard
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['adv_settings'] = 'Advanced Setting';
$string['ajax'] = 'Frequency';
$string['ajax_desc'] = 'Session storing frequency via AJAX. 0 - AJAX disabled (in seconds)';
$string['courses'] = 'Courses';
$string['dashboard'] = 'Dashboard';
$string['enabled'] = 'Enabled Tracking';
$string['enabled_desc'] = 'Enable Tracking';
$string['inactivity'] = 'Inactivity';
$string['inactivity_desc'] = 'User inactivity time (in seconds)';
$string['intelliboardroot'] = 'IntelliBoard';
$string['learners'] = 'Learners';
$string['load'] = 'Load';
$string['pluginname'] = 'IntelliBoard.net Plugin';
$string['report'] = 'Report';
$string['reports'] = 'Reports';
$string['settings'] = 'Settings';
$string['trackadmin'] = 'Tracking Admins';
$string['trackadmin_desc'] = 'Enable tracking for admin users (not recommended)';
$string['tracking'] = 'Session Tracking';
$string['tracking_title'] = 'Time Tracking';
