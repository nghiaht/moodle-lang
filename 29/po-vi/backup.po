#. extracted from en\backup.php, vi\backup.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'backup', language 'en', branch 'MOODLE_29_STABLE'
#.  *
#.  * @package   backup
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['autoactivedescription']
msgid ""
"Choose whether or not to do automated backups. If manual is selected "
"automated backups will be possible only by through the automated backups CLI "
"script. This can be done either manually on the command line or through cron."
msgstr ""
"Chọn xem liệu có thục hiện sao lưu tự động. Nếu thủ công được chọn các sao "
"lưu tự động sẽ chỉ có thể thực hiện được thông qua mã nguồn CLI sao lưu tự "
"động. Việc này có thể được thực hiện thủ công trên trình dòng lệnh hoặc "
"thông qua cron."

#: $string['autoactivedisabled']
msgid "Disabled"
msgstr "Tắt"

#: $string['autoactiveenabled']
msgid "Enabled"
msgstr "Mở"

#: $string['autoactivemanual']
msgid "Manual"
msgstr "Bằng tay"

#: $string['automatedbackupschedule']
msgid "Schedule"
msgstr "Lịch làm việc"

#: $string['automatedbackupschedulehelp']
msgid "Choose which days of the week to perform automated backups."
msgstr "Chọn ngày muốn thực hiện sao lưu tự động."

#: $string['automatedbackupsinactive']
msgid "Automated backups haven't been enabled by the site admin"
msgstr "Quản trị viên chưa mở chức năng sao lưu tự động"

#: $string['automatedbackupstatus']
msgid "Automated backup status"
msgstr "Trạng thái các bản sao lưu tự động"

#: $string['automatedsettings']
msgid "Automated backup settings"
msgstr "Các thiết lập sao lưu tự động"

#: $string['automatedsetup']
msgid "Automated backup setup"
msgstr "Thiết lập sao lưu tự động"

#: $string['automatedstorage']
msgid "Automated backup storage"
msgstr "Lưu trữ sao lưu tự động"

#: $string['automatedstoragehelp']
msgid ""
"Choose the location where you want backups to be stored when they are "
"automatically created."
msgstr "Chọn nơi bạn muốn các bản sao được lưu trữ khi chúng được tạo tự động."

#: $string['backupactivity']
msgid "Backup activity: {$a}"
msgstr "Sao lưu hoạt động (activities): {$a}"

#: $string['backupcourse']
msgid "Backup course: {$a}"
msgstr "Sao lưu khoá học: {$a}"

#: $string['backupcoursedetails']
msgid "Course details"
msgstr "Chi tiết khoá học"

#: $string['backupcoursesection']
msgid "Section: {$a}"
msgstr "Phiên: {$a}"

#: $string['backupcoursesections']
msgid "Course sections"
msgstr "Các phiên của khoá học"

#: $string['backupdate']
msgid "Date taken"
msgstr "Dữ liệu được lấy"

#: $string['backupdetails']
msgid "Backup details"
msgstr "Chi tiết sao lưu"

#: $string['backupdetailsnonstandardinfo']
msgid ""
"The selected file is not a standard Moodle backup file. The restore process "
"will try to convert the backup file into the standard format and then "
"restore it."
msgstr ""
"Tập tin được chọn không phải tập tin sao lưu chuẩn Moodle. Quá trình phục "
"hồi sẽ cố chuyển tập tin sao lưu sang định dạng chuẩn và rồi phục hồi nó."

#: $string['backupformat']
msgid "Format"
msgstr "Định dạng"

#: $string['backupformatimscc1']
msgid "IMS Common Cartridge 1.0"
msgstr "IMS Common Cartridge 1.0"

#: $string['backupformatimscc11']
msgid "IMS Common Cartridge 1.1"
msgstr "IMS Common Cartridge 1.1"

#: $string['backupformatmoodle1']
msgid "Moodle 1"
msgstr "Moodle 1"

#: $string['backupformatmoodle2']
msgid "Moodle 2"
msgstr "Moodle 2"

#: $string['backupformatunknown']
msgid "Unknown format"
msgstr "Định dạng không xác định"

#: $string['backuplog']
msgid "Technical information and warnings"
msgstr "Thông tin kĩ thuật và cảnh báo"

#: $string['backupmode']
msgid "Mode"
msgstr "Chế độ"

#: $string['backupmode10']
msgid "General"
msgstr "Chung"

#: $string['backupmode20']
msgid "Import"
msgstr "Nhập"

#: $string['backupmode30']
msgid "Hub"
msgstr "Hub"

#: $string['backupmode40']
msgid "Same site"
msgstr "Cùng hệ thống"

#: $string['backupmode50']
msgid "Automated"
msgstr "Tự động"

#: $string['backupmode60']
msgid "Converted"
msgstr "Hoàn đổi"

#: $string['backupsection']
msgid "Backup course section: {$a}"
msgstr "Sau lưu phiên {$a} của khoá học"

#: $string['backupsettings']
msgctxt "$string['backupsettings']"
msgid "Backup settings"
msgstr "Các thiết lập sao lưu"

#: $string['backupsitedetails']
msgid "Site details"
msgstr ""

#: $string['backupstage16action']
msgctxt "$string['backupstage16action']"
msgid "Continue"
msgstr "Tiếp tục"

#: $string['backupstage1action']
msgctxt "$string['backupstage1action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['backupstage2action']
msgctxt "$string['backupstage2action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['backupstage4action']
msgctxt "$string['backupstage4action']"
msgid "Perform backup"
msgstr "Thực thi việc sao lưu"

#: $string['backupstage8action']
msgctxt "$string['backupstage8action']"
msgid "Continue"
msgstr "Tiếp tục"

#: $string['backuptype']
msgid "Type"
msgstr "Kiểu"

#: $string['backuptypeactivity']
msgid "Activity"
msgstr "Hoạt động"

#: $string['backuptypecourse']
msgid "Course"
msgstr "Khoá học"

#: $string['backuptypesection']
msgid "Section"
msgstr "Phân mục"

#: $string['backupversion']
msgid "Backup version"
msgstr "Phiên bản sao lưu"

#: $string['cannotfindassignablerole']
msgid ""
"The {$a} role in the backup file cannot be mapped to any of the roles that "
"you are allowed to assign."
msgstr ""
"Vai {$a} trong tập tin sao lưu không thể mô tả cho bất cứ vai nào mà bạn "
"muốn gán."

#: $string['choosefilefromactivitybackup']
msgid "Activity backup area"
msgstr "Khu vực sao lưu hoạt động"

#: $string['choosefilefromactivitybackup_help']
msgid ""
"When backup activities using default settings, backup files will be stored "
"here"
msgstr ""
"Khi sao lưu các hoạt động bằng sử dụng các thiết lập mặc định, các tập tin "
"sao lưu sẽ được lưu trữ ở đây"

#: $string['choosefilefromautomatedbackup']
msgid "Automated backups"
msgstr "Các sao lưu tự động"

#: $string['choosefilefromautomatedbackup_help']
msgid "Contains automatically generated backups."
msgstr "Chứa các sao lưu được tạo tự động."

#: $string['choosefilefromcoursebackup']
msgid "Course backup area"
msgstr "Khu vực sao lưu khoá học"

#: $string['choosefilefromcoursebackup_help']
msgid ""
"When backup courses using default settings, backup files will be stored here"
msgstr ""
"Khi sao lưu các khoá học bằng sử dụng các thiết lập mặc định, các tập tin "
"sao lưu sẽ được lưu trữ ở đây"

#: $string['choosefilefromuserbackup']
msgid "User private backup area"
msgstr "Khu vực sao lưu riêng của người dùng"

#: $string['choosefilefromuserbackup_help']
msgid ""
"When backup courses with \"Anonymize user information\" option ticked, "
"backup files will be stored here"
msgstr ""
"Khi sao lưu các khóa học với lựa chọn \"Ẩn thông tin người dùng\", các tập "
"tin sao lưu sẽ được lưu trữ ở đây"

#: $string['configgeneralactivities']
msgid "Sets the default for including activities in a backup."
msgstr "Cài đặt tính năng mặc định để bao gồm được các hoạt động khi sao lưu."

#: $string['configgeneralanonymize']
msgid ""
"If enabled all information pertaining to users will be anonymised by default."
msgstr ""
"Nếu được kích hoạt, tất cả các thông tin về người dùng, theo mặc định, sẽ bị "
"khuyết danh."

#: $string['configgeneralbadges']
msgid "Sets the default for including badges in a backup."
msgstr "Đặt mặc định đối với các huy hiệu đi kèm trong sao lưu."

#: $string['configgeneralblocks']
msgid "Sets the default for including blocks in a backup."
msgstr "Đặt mặc định đối với các khối đi kèm trong sao lưu."

#: $string['configgeneralcomments']
msgid "Sets the default for including comments in a backup."
msgstr "Đặt mặc định đối với các bình luận đi kèm trong sao lưu."

#: $string['configgeneralfilters']
msgid "Sets the default for including filters in a backup."
msgstr "Đặt mặc định đối với bộ lọc đi kèm trong sao lưu."

#: $string['configgeneralgroups']
msgid "Sets the default for including groups and groupings in a backup."
msgstr ""

#: $string['configgeneralhistories']
msgid "Sets the default for including user history within a backup."
msgstr "Đặt mặc định đối với lịch sử người dùng đi kèm trong sao lưu."

#: $string['configgenerallogs']
msgid "If enabled logs will be included in backups by default."
msgstr "Nếu nhật kí được kích hoạt chúng sẽ đi kèm trong sao lưu theo mặc định."

#: $string['configgeneralquestionbank']
msgid ""
"If enabled the question bank will be included in backups by default. PLEASE "
"NOTE: Disabling this setting will disable the backup of activities which use "
"the question bank, such as the quiz."
msgstr ""
"Nếu được kích hoạt ngân hàng câu hỏi sẽ được sao lưu mặc định. HÃY CHÚ Ý: Vô "
"hiệu hóa thiết lập này sẽ vô hiệu hóa việc sao lưu các hoạt động sử dụng "
"ngân hàng câu hỏi, như trắc nghiệm."

#: $string['configgeneralroleassignments']
msgid "If enabled by default roles assignments will also be backed up."
msgstr "Nếu được kích hoạt theo mắc định các phân quyền cũng sẽ được sao lưu."

#: $string['configgeneralusers']
msgid "Sets the default for whether to include users in backups."
msgstr "Đặt mặc định đối với việc kèm theo người dùng trong sao lưu."

#: $string['configgeneraluserscompletion']
msgid ""
"If enabled user completion information will be included in backups by "
"default."
msgstr ""
"Nếu được kích hoạt thông tin hoàn tất người dùng sẽ đi kèm trong sao lưu "
"theo mặc định."

#: $string['configloglifetime']
msgid ""
"This specifies the length of time you want to keep backup logs information. "
"Logs that are older than this age are automatically deleted. It is "
"recommended to keep this value small, because backup logged information can "
"be huge."
msgstr ""
"Chỉ định khoảng thời gian bạn muốn giữ thông tin nhật kí sao lưu. Nhật kí cũ "
"hơn thời gian này sẽ tự động xóa. Khuyến khích giữ giá trị này nhỏ, bởi vì "
"thông tin nhật kí sao lưu có thể khổng lồ."

#: $string['confirmcancel']
msgid "Cancel backup"
msgstr "Hủy sao lưu"

#: $string['confirmcancelno']
msgid "Stay"
msgstr "Giữ"

#: $string['confirmcancelquestion']
msgid ""
"Are you sure you wish to cancel?\n"
"Any information you have entered will be lost."
msgstr ""
"Bạn có muốn hủy?\n"
"Bất kì thông tin nào bạn vừa nhập sẽ bị mất."

#: $string['confirmcancelyes']
msgid "Cancel"
msgstr "Hủy"

#: $string['confirmnewcoursecontinue']
msgid "New course warning"
msgstr "Cảnh báo khóa học mới"

#: $string['confirmnewcoursecontinuequestion']
msgid ""
"A temporary (hidden) course will be created by the course restoration "
"process. To abort restoration click cancel. Do not close the browser while "
"restoring."
msgstr ""
"Một khóa học tạm thời (ẩn) sẽ được tạo bởi quá trình phục hồi khóa học. Để "
"bỏ phục hồi nhấn hủy. Không đóng trình duyệt trong khi phục hồi."

#: $string['coursecategory']
msgid "Category the course will be restored into"
msgstr "Chuyên mục mà khóa học sẽ được phục hồi thành"

#: $string['courseid']
msgid "Original ID"
msgstr "ID ban đầu"

#: $string['coursesettings']
msgctxt "$string['coursesettings']"
msgid "Course settings"
msgstr "Cài đặt khóa học"

#: $string['coursetitle']
msgctxt "$string['coursetitle']"
msgid "Title"
msgstr "Tiêu đề"

#: $string['currentstage1']
msgctxt "$string['currentstage1']"
msgid "Initial settings"
msgstr "Cài đặt ban đầu"

#: $string['currentstage16']
msgctxt "$string['currentstage16']"
msgid "Complete"
msgstr "Hoàn thành"

#: $string['currentstage2']
msgctxt "$string['currentstage2']"
msgid "Schema settings"
msgstr "Thiết lập giản đồ"

#: $string['currentstage4']
msgctxt "$string['currentstage4']"
msgid "Confirmation and review"
msgstr "Xác nhận và kiểm duyệt"

#: $string['currentstage8']
msgctxt "$string['currentstage8']"
msgid "Perform backup"
msgstr "Thực hiện sao lưu"

#: $string['enterasearch']
msgid "Enter a search"
msgstr "Nhập tìm kiếm"

#: $string['error_block_for_module_not_found']
msgid ""
"Orphan block instance (id: {$a->bid}) for course module (id: {$a->mid}) "
"found. This block will not be backed up"
msgstr ""
"Tìm thấy khối thực thể mồ côi (id: {$a->bid}) dành cho mô-đun khóa học "
"({$a->mid}). Khhối này sẽ không được sao lưu"

#: $string['error_course_module_not_found']
msgid ""
"Orphan course module (id: {$a}) found. This module will not be backed up."
msgstr ""
"Tìm thấy mô-đun khóa học mồ côi  (id: {$a}). Mô-đun này sẽ không được sao lưu"

#: $string['errorfilenamemustbezip']
msgid "The filename you enter must be a ZIP file and have the .mbz extension"
msgstr "Tên tệp bạn vừa nhập phải là tệp ZIP và có đuôi .mbz"

#: $string['errorfilenamerequired']
msgid "You must enter a valid filename for this backup"
msgstr "Bạn phải nhập tên tệp hợp lệ để sao lưu"

#: $string['errorinvalidformat']
msgid "Unknown backup format"
msgstr "Định dạng sao lưu không rõ"

#: $string['errorinvalidformatinfo']
msgid ""
"The selected file is not a valid Moodle backup file and can't be restored."
msgstr ""
"Tập tin được họn không phải tập tin sao lưu Moodle hợp lệ và không thể phục "
"hồi được."

#: $string['errorminbackup20version']
msgid ""
"This backup file has been created with one development version of Moodle "
"backup ({$a->backup}). Minimum required is {$a->min}. Cannot be restored."
msgstr ""
"Tập tin phục hồi này đã được tạo với một phiên bản phát triển của bản sao "
"lưu Moodle ({$a->backup}). Yêu cầu tối thiểu là {$a->min}. Không thể phục "
"hồi được."

#: $string['errorrestorefrontpagebackup']
msgid "You can only restore front page backups on the front page"
msgstr ""

#: $string['executionsuccess']
msgid "The backup file was successfully created."
msgstr "Tập tin sao lưu được tạo lập thành công."

#: $string['filealiasesrestorefailures']
msgid "Aliases restore failures"
msgstr "Các thất bại phục hồi định danh"

#: $string['filealiasesrestorefailures_help']
msgid ""
"Aliases are symbolic links to other files, including those stored in "
"external repositories. In some cases, Moodle cannot restore them - for "
"example when restoring the backup at another site or when the referenced "
"file does not exist.\n"
"\n"
"More details and the actual reason of the failure can be found in the "
"restore log file."
msgstr ""
"Định danh là các liên kết tượng trưng đến các tập tin khác, bao gồm những "
"cái được lưu trữ trong các kho bên ngoài. Trong một số trường hợp, Moodle "
"không thể phục hồi chúng - ví dụ khi phục hồi sao lưu ở một trang khác khi "
"mà tập tin được tham chiếu không tồn tại.\n"
"\n"
"Thông tin chi tiết hơn và nguyên do thất bại thực sư có thể tìm thấy ở tập "
"tin nhật kí phục hồi."

#: $string['filealiasesrestorefailuresinfo']
msgid ""
"Some aliases included in the backup file could not be restored. The "
"following list contains their expected location and the source file they "
"were referring to at the original site."
msgstr ""
"Một số định danh kèm trong tập tin sao lưu không thể phục hồi được. Danh "
"sách sau chứa vị trí mong đợi của chúng và tập tin nguồn mà chúng chỉ đến "
"tại trang gốc."

#: $string['filealiasesrestorefailures_link']
msgid "restore/filealiases"
msgstr ""

#: $string['filename']
msgid "Filename"
msgstr "Tên tệp"

#: $string['filereferencesincluded']
msgid ""
"File references to external contents included in backup package, they won't "
"work on other sites."
msgstr "Các tham chiếu tập tin"

#: $string['filereferencesnotsamesite']
msgid "Backup is from other site, file references cannot be restored"
msgstr "Sao lưu từ trang khác, các tham chiếu tập tin không thể phục hồi được"

#: $string['filereferencessamesite']
msgid "Backup is from the same site, file references can be restored"
msgstr "Sao lưu cùng trang, các tham chiếu tập tin có thể phục hồi"

#: $string['generalactivities']
msgctxt "$string['generalactivities']"
msgid "Include activities and resources"
msgstr "Bao gồm các hoạt động"

#: $string['generalanonymize']
msgid "Anonymise information"
msgstr "Ẩn thông tin"

#: $string['generalbackdefaults']
msgid "General backup defaults"
msgstr "Mặc định sao lưu thông thường"

#: $string['generalbadges']
msgctxt "$string['generalbadges']"
msgid "Include badges"
msgstr "Bao gồm các huy hiệu"

#: $string['generalblocks']
msgctxt "$string['generalblocks']"
msgid "Include blocks"
msgstr "Bao gồm các khối"

#: $string['generalcomments']
msgctxt "$string['generalcomments']"
msgid "Include comments"
msgstr "Bao gồm các bình luận"

#: $string['generalfilters']
msgctxt "$string['generalfilters']"
msgid "Include filters"
msgstr "Bao gồm các bộ lọc"

#: $string['generalgradehistories']
msgctxt "$string['generalgradehistories']"
msgid "Include histories"
msgstr ""

#: $string['generalgroups']
msgctxt "$string['generalgroups']"
msgid "Include groups and groupings"
msgstr ""

#: $string['generalhistories']
msgctxt "$string['generalhistories']"
msgid "Include histories"
msgstr "Bao gồm lịch sử"

#: $string['generallogs']
msgid "Include logs"
msgstr "Bao gồm nhật kí"

#: $string['generalquestionbank']
msgctxt "$string['generalquestionbank']"
msgid "Include question bank"
msgstr "Bao gồm ngân hàng câu hỏi"

#: $string['generalroleassignments']
msgid "Include role assignments"
msgstr "Bao gồm các phân quyền"

#: $string['generalsettings']
msgid "General backup settings"
msgstr "Các thiết lập sao lưu thông thường"

#: $string['generalusers']
msgid "Include users"
msgstr "Bao gồm các người dùng"

#: $string['generaluserscompletion']
msgid "Include user completion information"
msgstr "Bao gồm thông tin hoàn tất người dùng"

#: $string['hidetypes']
msgid "Hide type options"
msgstr "Ẩn tùy chọn loại"

#: $string['importbackupstage16action']
msgctxt "$string['importbackupstage16action']"
msgid "Continue"
msgstr "Tiếp tục"

#: $string['importbackupstage1action']
msgctxt "$string['importbackupstage1action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['importbackupstage2action']
msgctxt "$string['importbackupstage2action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['importbackupstage4action']
msgctxt "$string['importbackupstage4action']"
msgid "Perform import"
msgstr "Thực hiện nhập"

#: $string['importbackupstage8action']
msgctxt "$string['importbackupstage8action']"
msgid "Continue"
msgstr "Tiếp tục"

#: $string['importcurrentstage0']
msgid "Course selection"
msgstr "Chọn khóa học"

#: $string['importcurrentstage1']
msgctxt "$string['importcurrentstage1']"
msgid "Initial settings"
msgstr "Các thiết lập ban đầu"

#: $string['importcurrentstage16']
msgctxt "$string['importcurrentstage16']"
msgid "Complete"
msgstr "Hoàn thành"

#: $string['importcurrentstage2']
msgctxt "$string['importcurrentstage2']"
msgid "Schema settings"
msgstr "Các thiết lập giản đồ"

#: $string['importcurrentstage4']
msgctxt "$string['importcurrentstage4']"
msgid "Confirmation and review"
msgstr "Xác nhận và kiểm duyệt"

#: $string['importcurrentstage8']
msgctxt "$string['importcurrentstage8']"
msgid "Perform import"
msgstr "Thực hiện nhập"

#: $string['importfile']
msgid "Import a backup file"
msgstr "Nhập một tập tin sao lưu"

#: $string['importgeneralmaxresults']
msgid "Maximum number of courses listed for import"
msgstr "Số khóa học tối đa được liệt kê để nhập"

#: $string['importgeneralmaxresults_desc']
msgid ""
"This controls the number of courses that are listed during the first step of "
"the import process"
msgstr "Kiểm soát số khóa học được liệt liệt kê suốt bước đầu quá trình nhập"

#: $string['importgeneralsettings']
msgid "General import defaults"
msgstr "Mặc định nhập thông thường"

#: $string['importsuccess']
msgid "Import complete. Click continue to return to the course."
msgstr "Nhập hoàn tất. Nhấn tiếp tục để trở về khóa học."

#: $string['includeactivities']
msgid "Include:"
msgstr "Bao gồm:"

#: $string['includeditems']
msgid "Included items:"
msgstr "Các mục bao gồm:"

#: $string['includefilereferences']
msgid "File references to external contents"
msgstr "Các tham chiếu tập tin đến nội dung bên ngoài"

#: $string['includesection']
msgid "Section {$a}"
msgstr "Phân mục {$a}"

#: $string['includeuserinfo']
msgid "User data"
msgstr "Dữ liệu người dùng"

#: $string['jumptofinalstep']
msgid "Jump to final step"
msgstr ""

#: $string['locked']
msgid "Locked"
msgstr "Đã bị khóa"

#: $string['lockedbyconfig']
msgid "This setting has been locked by the default backup settings"
msgstr "Thiết lập này đã bị khóa bởi các thiết lập sao lưu mặc định"

#: $string['lockedbyhierarchy']
msgid "Locked by dependencies"
msgstr "Bị khóa bởi các bộ phụ thuộc"

#: $string['lockedbypermission']
msgid "You don't have sufficient permissions to change this setting"
msgstr "Bạn không đủ quyền hạn thay đổi thiết lập này"

#: $string['loglifetime']
msgid "Keep logs for"
msgstr "Giữ nhật kí trong"

#: $string['managefiles']
msgid "Manage backup files"
msgstr "Quản lí các tập tin sao lưu"

#: $string['missingfilesinpool']
msgid ""
"Some files could not be saved during the backup, it won't be possible to "
"restore them."
msgstr ""
"Một số tập tin không thể lưu trong quá trình sao lưu, sẽ không thể phục hồi "
"chúng."

#: $string['module']
msgid "Module"
msgstr "Mô-đun"

#: $string['moodleversion']
msgid "Moodle version"
msgstr "Phiên bản Moodle"

#: $string['morecoursesearchresults']
msgid "More than {$a} courses found, showing first {$a} results"
msgstr "Tìm thấy hơn {$a} khóa học, hiện {$a} kết quả đầu tiên"

#: $string['moreresults']
msgid "There are too many results, enter a more specific search."
msgstr "Có quá nhiều kết quả, nhập tìm kiếm cụ thể hơn"

#: $string['nomatchingcourses']
msgid "There are no courses to display"
msgstr "Không có khóa học nào để hiển thị"

#: $string['norestoreoptions']
msgid "There are no categories or existing courses you can restore to."
msgstr "Không có chuyên mục hay khóa học tồn tại nào mà bạn có thể phục hồi"

#: $string['originalwwwroot']
msgid "URL of backup"
msgstr "URL sao lưu"

#: $string['preparingdata']
msgid "Preparing data"
msgstr "Đang chuẩn bị dữ liệu"

#: $string['preparingui']
msgid "Preparing to display page"
msgstr "Đang chuẩn bị hiển thị trang"

#: $string['previousstage']
msgid "Previous"
msgstr "Trước"

#: $string['qcategory2coursefallback']
msgctxt "$string['qcategory2coursefallback']"
msgid ""
"The questions category \"{$a->name}\", originally at system/course category "
"context in backup file, will be created at course context by restore"
msgstr ""
"Chuyên mục câu hỏi  \"{$a->name}\", ban đầu ở bối cảnh chuyên mục hệ thống/"
"khóa học trong tập tin sao lưu, sẽ được tạo ở bối cảnh khóa học khi phục hồi"

#: $string['qcategorycannotberestored']
msgid "The questions category \"{$a->name}\" cannot be created by restore"
msgstr "Chuyên mục câu hỏi \"{$a->name}\" không thể phục hồi được"

#: $string['question2coursefallback']
msgctxt "$string['question2coursefallback']"
msgid ""
"The questions category \"{$a->name}\", originally at system/course category "
"context in backup file, will be created at course context by restore"
msgstr ""
"Chuyên mục câu hỏi \"{$a->name}\" , ban đầu ở bối cảnh chuyên mục hệ thống/"
"khóa học trong tập tin sao lưu, sẽ được tạo ở bối cảnh khóa học khi phục hồi"

#: $string['questionegorycannotberestored']
msgid "The questions \"{$a->name}\" cannot be created by restore"
msgstr ""

#: $string['restoreactivity']
msgid "Restore activity"
msgstr "Hoạt động phục hồi"

#: $string['restorecourse']
msgid "Restore course"
msgstr "Phục hồi khóa học"

#: $string['restorecoursesettings']
msgctxt "$string['restorecoursesettings']"
msgid "Course settings"
msgstr ""

#: $string['restoreexecutionsuccess']
msgid ""
"The course was restored successfully, clicking the continue button below "
"will take you to view the course you restored."
msgstr ""
"Khóa học được phục hồi thành công, nhấn nút tiếp tục bên dưới sẽ cho bạn xem "
"khóa học vừa phục hồi."

#: $string['restorefileweremissing']
msgid ""
"Some files could not be restored because they were missing in the backup."
msgstr ""
"Một số tập tin không thể phục hồi được vì chúng bị thiếu trong bản sao lưu."

#: $string['restorenewcoursefullname']
msgid "New course name"
msgstr ""

#: $string['restorenewcourseshortname']
msgid "New course short name"
msgstr ""

#: $string['restorenewcoursestartdate']
msgid "New start date"
msgstr ""

#: $string['restorerolemappings']
msgid "Restore role mappings"
msgstr "Phục hồi các sắp đặt quyền"

#: $string['restorerootsettings']
msgid "Restore settings"
msgstr "Các thiết lập phục hồi"

#: $string['restoresection']
msgid "Restore section"
msgstr "Phân mục phục hồi"

#: $string['restorestage1']
msgid "Confirm"
msgstr "Xác nhận"

#: $string['restorestage16']
msgid "Review"
msgstr "Kiểm duyệt"

#: $string['restorestage16action']
msgid "Perform restore"
msgstr "Thực hiện phục hồi"

#: $string['restorestage1action']
msgctxt "$string['restorestage1action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['restorestage2']
msgid "Destination"
msgstr "Đích"

#: $string['restorestage2action']
msgctxt "$string['restorestage2action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['restorestage32']
msgid "Process"
msgstr "Quá trình"

#: $string['restorestage32action']
msgctxt "$string['restorestage32action']"
msgid "Continue"
msgstr "Tiếp tục"

#: $string['restorestage4']
msgid "Settings"
msgstr "Cài đặt"

#: $string['restorestage4action']
msgctxt "$string['restorestage4action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['restorestage64']
msgctxt "$string['restorestage64']"
msgid "Complete"
msgstr "Hoàn thành"

#: $string['restorestage64action']
msgctxt "$string['restorestage64action']"
msgid "Continue"
msgstr "Tiếp tục"

#: $string['restorestage8']
msgid "Schema"
msgstr "Giản đồ"

#: $string['restorestage8action']
msgctxt "$string['restorestage8action']"
msgid "Next"
msgstr "Kế tiếp"

#: $string['restoretarget']
msgid "Restore target"
msgstr "Phục hồi phục tiêu"

#: $string['restoretocourse']
msgid "Restore to course:"
msgstr ""

#: $string['restoretocurrentcourse']
msgid "Restore into this course"
msgstr "Phục hồi thành khóa học này"

#: $string['restoretocurrentcourseadding']
msgid "Merge the backup course into this course"
msgstr "Trộn khóa học sao lưu thành khóa học này"

#: $string['restoretocurrentcoursedeleting']
msgid "Delete the contents of this course and then restore"
msgstr "Xóa nội dung của khóa học này và phục hồi"

#: $string['restoretoexistingcourse']
msgid "Restore into an existing course"
msgstr "Phục hồi thành một khóa học vốn có"

#: $string['restoretoexistingcourseadding']
msgid "Merge the backup course into the existing course"
msgstr "Trộn khóa học sao lưu thành khóa học vốn có"

#: $string['restoretoexistingcoursedeleting']
msgid "Delete the contents of the existing course and then restore"
msgstr "Xóa nội dung của khóa học vốn có và phục hồi"

#: $string['restoretonewcourse']
msgid "Restore as a new course"
msgstr "Phục hồi thành khóa học mới"

#: $string['restoringcourse']
msgid "Course restoration in progress"
msgstr "Đang trong quá trình phục hồi khóa học"

#: $string['restoringcourseshortname']
msgid "restoring"
msgstr "đang phục hồi"

#: $string['rootenrolmanual']
msgid "Restore as manual enrolments"
msgstr "Phục hồi thành ghi danh thủ công"

#: $string['rootsettingactivities']
msgctxt "$string['rootsettingactivities']"
msgid "Include activities and resources"
msgstr "Bao gồm các hoạt động"

#: $string['rootsettinganonymize']
msgid "Anonymize user information"
msgstr "Ẩn thông tin người dùng"

#: $string['rootsettingbadges']
msgctxt "$string['rootsettingbadges']"
msgid "Include badges"
msgstr "Bao gồm các huy hiệu"

#: $string['rootsettingblocks']
msgctxt "$string['rootsettingblocks']"
msgid "Include blocks"
msgstr "Bao gồm các khối"

#: $string['rootsettingcalendarevents']
msgid "Include calendar events"
msgstr "Bao gồm các sự kiện lịch"

#: $string['rootsettingcomments']
msgctxt "$string['rootsettingcomments']"
msgid "Include comments"
msgstr "Bao gồm các bình luận"

#: $string['rootsettingfilters']
msgctxt "$string['rootsettingfilters']"
msgid "Include filters"
msgstr "Bao gồm các bộ lọc"

#: $string['rootsettinggradehistories']
msgid "Include grade history"
msgstr "Bao gồm lịch sử điểm"

#: $string['rootsettinggroups']
msgctxt "$string['rootsettinggroups']"
msgid "Include groups and groupings"
msgstr ""

#: $string['rootsettingimscc1']
msgid "Convert to IMS Common Cartridge 1.0"
msgstr "Chuyển thành IMS Common Cartridge 1.0"

#: $string['rootsettingimscc11']
msgid "Convert to IMS Common Cartridge 1.1"
msgstr "Chuyển thành IMS Common Cartridge 1.1"

#: $string['rootsettinglogs']
msgid "Include course logs"
msgstr "Bao gồm nhật kí khóa học"

#: $string['rootsettingquestionbank']
msgctxt "$string['rootsettingquestionbank']"
msgid "Include question bank"
msgstr "Bao gồm ngân hàng câu hỏi"

#: $string['rootsettingroleassignments']
msgid "Include user role assignments"
msgstr "Bao gồm các phân quyền người dùng"

#: $string['rootsettings']
msgctxt "$string['rootsettings']"
msgid "Backup settings"
msgstr "Các thiết lập sao lưu"

#: $string['rootsettingusers']
msgid "Include enrolled users"
msgstr "Bao gồm các người dùng đã ghi danh"

#: $string['rootsettinguserscompletion']
msgid "Include user completion details"
msgstr "Bao gồm các chi tiết hoàn tất người dùng"

#: $string['sectionactivities']
msgid "Activities"
msgstr "Hoạt động"

#: $string['sectioninc']
msgid "Included in backup (no user information)"
msgstr "Được kèm trong bản sao lưu (không có thông tin người dùng)"

#: $string['sectionincanduser']
msgid "Included in backup along with user information"
msgstr "Được kèm trong bản sao lưu kèm theo thông tin người dùng"

#: $string['selectacategory']
msgid "Select a category"
msgstr "Chọn chuyên mục"

#: $string['selectacourse']
msgid "Select a course"
msgstr "Chọn khóa học"

#: $string['setting_course_fullname']
msgid "Course name"
msgstr "Tên khóa học"

#: $string['setting_course_shortname']
msgid "Course short name"
msgstr "Tên ngắn khóa học"

#: $string['setting_course_startdate']
msgid "Course start date"
msgstr "Ngày bắt đầu khóa học"

#: $string['setting_keep_groups_and_groupings']
msgid "Keep current groups and groupings"
msgstr "Giữ các nhóm hiện tại"

#: $string['setting_keep_roles_and_enrolments']
msgid "Keep current roles and enrolments"
msgstr "Giữ các quyền và ghi danh hiện tại"

#: $string['setting_overwriteconf']
msgid "Overwrite course configuration"
msgstr "Đè thiết lập khóa học"

#: $string['showtypes']
msgid "Show type options"
msgstr "Hiện các lựa chọn loại"

#: $string['sitecourseformatwarning']
msgid ""
"This is a front page backup, note that they can only be restored on the "
"front page"
msgstr ""

#: $string['skiphidden']
msgid "Skip hidden courses"
msgstr "Bỏ qua các khóa học ẩn"

#: $string['skiphiddenhelp']
msgid "Choose whether or not to skip hidden courses"
msgstr "Chọn xem có hay không việc bỏ qua các khóa học ẩn"

#: $string['skipmodifdays']
msgid "Skip courses not modified since"
msgstr "Bỏ qua các khóa học không được chỉnh sửa kể từ"

#: $string['skipmodifdayshelp']
msgid ""
"Choose to skip courses that have not been modified since a number of days"
msgstr "Chọn bỏ qua các khóa học không được chỉnh sửa kể từ một số ngày"

#: $string['skipmodifprev']
msgid "Skip courses not modified since previous backup"
msgstr "Bỏ qua các khóa học không được chỉnh sửa kể từ lần sao lưu trước"

#: $string['skipmodifprevhelp']
msgid ""
"Choose whether to skip courses that have not been modified since the last "
"automatic backup. This requires logging to be enabled."
msgstr ""
"Chọn xem có bỏ qua các khóa học không được chỉnh sửa kể từ lần sao lưu tự "
"động trước. Yêu cầu nhật kí được kích hoạt."

#: $string['storagecourseandexternal']
msgid "Course backup filearea and the specified directory"
msgstr "Vùng tập tin sao lưu khóa học và thư mục được chỉ định"

#: $string['storagecourseonly']
msgid "Course backup filearea"
msgstr "Vùng tập tin sao lưu khóa học"

#: $string['storageexternalonly']
msgid "Specified directory for automated backups"
msgstr "Thư mục được chỉ định sao lưu tự động"

#: $string['timetaken']
msgid "Time taken"
msgstr "Thời gian thực hiện"

#: $string['title']
msgctxt "$string['title']"
msgid "Title"
msgstr "Tiêu đề"

#: $string['totalcategorysearchresults']
msgid "Total categories: {$a}"
msgstr "Tổng số chuyên mục: {$a}"

#: $string['totalcoursesearchresults']
msgid "Total courses: {$a}"
msgstr "Tổng số khóa học: {$a}"

#: $string['unnamedsection']
msgid "Unnamed section"
msgstr "Phân vùng không tên"

#: $string['userinfo']
msgid "Userinfo"
msgstr "Thông tin người dùng"
