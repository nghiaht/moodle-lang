Create POT files (use to merge new versions with old versions, see pot2po)
http://translate-toolkit.readthedocs.org/en/latest/commands/pot2po.html
php2po.py -P en pot/

Create po translations files
php2po.py -t en vi po-vi/

http://translate-toolkit.readthedocs.org/en/latest/commands/php2po.html

Translate and Convert to PHP back
po2php -t en po-vi vi

