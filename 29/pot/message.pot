#. extracted from en\message.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'message', language 'en', branch 'MOODLE_29_STABLE'
#.  *
#.  * @package   message
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['addcontact']
msgid "Add contact"
msgstr ""

#: $string['addsomecontacts']
msgid ""
"To send a message to someone, or to add a shortcut for them on this page, "
"use the <a href=\"{$a}\">search tab</a> above."
msgstr ""

#: $string['addsomecontactsincoming']
msgid ""
"These messages are from people who are not in your contact list. To add them "
"to your contacts, click the \"Add contact\" icon next to their name."
msgstr ""

#: $string['ago']
msgid "{$a} ago"
msgstr ""

#: $string['ajax_gui']
msgid "Ajax chat room"
msgstr ""

#: $string['allmine']
msgid "All messages to me or from me"
msgstr ""

#: $string['allstudents']
msgid "All messages between students in course"
msgstr ""

#: $string['allusers']
msgid "All messages from all users"
msgstr ""

#: $string['backupmessageshelp']
msgid ""
"If enabled, then instant messages will be included in SITE automated backups"
msgstr ""

#: $string['beepnewmessage']
msgid "Beep when popup notification is displayed"
msgstr ""

#: $string['blockcontact']
msgid "Block contact"
msgstr ""

#: $string['blockedmessages']
msgid "{$a} message(s) to/from blocked users"
msgstr ""

#: $string['blockedusers']
msgid "Blocked users ({$a})"
msgstr ""

#: $string['blocknoncontacts']
msgid "Prevent non-contacts from messaging me"
msgstr ""

#: $string['contactlistempty']
msgid "Contact list empty"
msgstr ""

#: $string['contacts']
msgid "Contacts"
msgstr ""

#: $string['context']
msgid "context"
msgstr ""

#: $string['defaultmessageoutputs']
msgid "Default message outputs"
msgstr ""

#: $string['defaults']
msgid "Defaults"
msgstr ""

#: $string['deletemessagesdays']
msgid "Number of days before old messages are automatically deleted"
msgstr ""

#: $string['disableall']
msgid "Temporarily disable notifications"
msgstr ""

#: $string['disableall_help']
msgid ""
"Temporarily disable all notifications except those marked as \"forced\" by "
"the site administrator"
msgstr ""

#: $string['disabled']
msgid "Messaging is disabled on this site"
msgstr ""

#: $string['disallowed']
msgid "Disallowed"
msgstr ""

#: $string['discussion']
msgid "Discussion"
msgstr ""

#: $string['emailmessages']
msgid "Email messages when I am offline"
msgstr ""

#: $string['emailtagline']
msgid ""
"This is a copy of a message sent to you at \"{$a->sitename}\". Go to "
"{$a->url} to reply."
msgstr ""

#: $string['emptysearchstring']
msgid "You must search for something"
msgstr ""

#: $string['enabled']
msgid "Enabled"
msgstr ""

#: $string['errorcallingprocessor']
msgid "Error calling defined output"
msgstr ""

#: $string['errortranslatingdefault']
msgid ""
"Error translating default setting provided by plugin, using system defaults "
"instead."
msgstr ""

#: $string['errorwhilesendingmessage']
msgid "An error occurred while sending the message; please try again later."
msgstr ""

#: $string['eventmessagecontactadded']
msgid "Message contact added"
msgstr ""

#: $string['eventmessagecontactblocked']
msgid "Message contact blocked"
msgstr ""

#: $string['eventmessagecontactremoved']
msgid "Message contact removed"
msgstr ""

#: $string['eventmessagecontactunblocked']
msgid "Message contact unblocked"
msgstr ""

#: $string['eventmessagesent']
msgctxt "$string['eventmessagesent']"
msgid "Message sent"
msgstr ""

#: $string['eventmessageviewed']
msgid "Message viewed"
msgstr ""

#: $string['forced']
msgid "Forced"
msgstr ""

#: $string['formorethan']
msgid "For more than"
msgstr ""

#: $string['gotomessages']
msgid "Go to messages"
msgstr ""

#: $string['guestnoeditmessage']
msgid "Guest user can not edit messaging options"
msgstr ""

#: $string['guestnoeditmessageother']
msgid "Guest user can not edit other user messaging options"
msgstr ""

#: $string['includeblockedusers']
msgid "Include blocked users"
msgstr ""

#: $string['incomingcontacts']
msgid "Incoming contacts ({$a})"
msgstr ""

#: $string['keywords']
msgid "Keywords"
msgstr ""

#: $string['keywordssearchresults']
msgid "Messages found: {$a}"
msgstr ""

#: $string['keywordssearchresultstoomany']
msgid "More than {$a} messages found. Refine your search."
msgstr ""

#: $string['loggedin']
msgctxt "$string['loggedin']"
msgid "Online"
msgstr ""

#: $string['loggedindescription']
msgid "When I'm logged in"
msgstr ""

#: $string['loggedoff']
msgid "Not online"
msgstr ""

#: $string['loggedoffdescription']
msgid "When I'm offline"
msgstr ""

#: $string['mailsent']
msgid "Your message was sent via email."
msgstr ""

#: $string['managecontacts']
msgid "Manage my contacts"
msgstr ""

#: $string['managemessageoutputs']
msgid "Manage message outputs"
msgstr ""

#: $string['maxmessages']
msgid "Maximum number of messages to show in the discussion history"
msgstr ""

#: $string['message']
msgid "Message"
msgstr ""

#: $string['messagehistory']
msgid "Message history"
msgstr ""

#: $string['messagehistoryfull']
msgid "All messages"
msgstr ""

#: $string['messagenavigation']
msgid "Message navigation:"
msgstr ""

#: $string['messageoutputs']
msgid "Message outputs"
msgstr ""

#: $string['messages']
msgid "Messages"
msgstr ""

#: $string['messagesent']
msgctxt "$string['messagesent']"
msgid "Message sent"
msgstr ""

#: $string['messagetosend']
msgid "Message to send"
msgstr ""

#: $string['messaging']
msgid "Messaging"
msgstr ""

#: $string['messagingblockednoncontact']
msgid "{$a} will not be able to reply as you have blocked non-contacts"
msgstr ""

#: $string['messagingdisabled']
msgid "Messaging is disabled on this site, emails will be sent instead"
msgstr ""

#: $string['mostrecent']
msgid "Recent messages"
msgstr ""

#: $string['mostrecentconversations']
msgid "Recent conversations"
msgstr ""

#: $string['mostrecentnotifications']
msgid "Recent notifications"
msgstr ""

#: $string['newonlymsg']
msgid "Show only new"
msgstr ""

#: $string['newsearch']
msgid "New search"
msgstr ""

#: $string['noframesjs']
msgid "Use more accessible interface"
msgstr ""

#: $string['nomessages']
msgid "No messages waiting"
msgstr ""

#: $string['nomessagesfound']
msgid "No messages were found"
msgstr ""

#: $string['noreply']
msgid "Do not reply to this message"
msgstr ""

#: $string['nosearchresults']
msgid "There were no results from your search"
msgstr ""

#: $string['offline']
msgid "Offline"
msgstr ""

#: $string['offlinecontacts']
msgid "Offline contacts ({$a})"
msgstr ""

#: $string['online']
msgctxt "$string['online']"
msgid "Online"
msgstr ""

#: $string['onlinecontacts']
msgid "Online contacts ({$a})"
msgstr ""

#: $string['onlyfromme']
msgid "Only messages from me"
msgstr ""

#: $string['onlymycourses']
msgid "Only in my courses"
msgstr ""

#: $string['onlytome']
msgid "Only messages to me"
msgstr ""

#: $string['outputdisabled']
msgid "Output disabled"
msgstr ""

#: $string['outputdoesnotexist']
msgid "Message output does not exists"
msgstr ""

#: $string['outputenabled']
msgid "Output enabled"
msgstr ""

#: $string['outputnotavailable']
msgid "Not available"
msgstr ""

#: $string['outputnotconfigured']
msgid "Not configured"
msgstr ""

#: $string['page-message-x']
msgid "Any message pages"
msgstr ""

#: $string['pagerefreshes']
msgid "This page refreshes automatically every {$a} seconds"
msgstr ""

#: $string['permitted']
msgid "Permitted"
msgstr ""

#: $string['private_config']
msgid "Popup message window"
msgstr ""

#: $string['processortag']
msgid "Destination"
msgstr ""

#: $string['providers_config']
msgid "Configure notification methods for incoming messages"
msgstr ""

#: $string['providerstag']
msgid "Source"
msgstr ""

#: $string['readmessages']
msgid "{$a} read messages"
msgstr ""

#: $string['recent']
msgid "Recent"
msgstr ""

#: $string['removecontact']
msgid "Remove contact"
msgstr ""

#: $string['savemysettings']
msgid "Save my settings"
msgstr ""

#: $string['search']
msgid "Search"
msgstr ""

#: $string['searchcombined']
msgid "Search people and messages"
msgstr ""

#: $string['searchforperson']
msgid "Search for a person"
msgstr ""

#: $string['searchmessages']
msgid "Search messages"
msgstr ""

#: $string['sendingmessage']
msgid "Sending message"
msgstr ""

#: $string['sendingvia']
msgid "Sending \"{$a->provider}\" via \"{$a->processor}\""
msgstr ""

#: $string['sendingviawhen']
msgid "Sending \"{$a->provider}\" via \"{$a->processor}\" when {$a->state}"
msgstr ""

#: $string['sendmessage']
msgid "Send message"
msgstr ""

#: $string['sendmessageto']
msgid "Send message to {$a}"
msgstr ""

#: $string['sendmessagetopopup']
msgid "Send message to {$a} - new window"
msgstr ""

#: $string['settings']
msgid "Settings"
msgstr ""

#: $string['settingssaved']
msgid "Your settings have been saved"
msgstr ""

#: $string['showmessagewindow']
msgid "Popup window on new message"
msgstr ""

#: $string['strftimedaydatetime']
msgid "%A, %d %B %Y, %I:%M %p"
msgstr ""

#: $string['thisconversation']
msgid "this conversation"
msgstr ""

#: $string['timenosee']
msgid "Minutes since I was last seen online"
msgstr ""

#: $string['timesent']
msgid "Time sent"
msgstr ""

#: $string['touserdoesntexist']
msgid "You can not send a message to a user id ({$a}) that doesn't exist"
msgstr ""

#: $string['unblockcontact']
msgid "Unblock contact"
msgstr ""

#: $string['unreadmessages']
msgid "Unread messages ({$a})"
msgstr ""

#: $string['unreadnewmessage']
msgid "New message from {$a}"
msgstr ""

#: $string['unreadnewmessages']
msgid "New messages ({$a})"
msgstr ""

#: $string['userisblockingyou']
msgid "This user has blocked you from sending messages to them"
msgstr ""

#: $string['userisblockingyounoncontact']
msgid "{$a} only accepts messages from their contacts."
msgstr ""

#: $string['userssearchresults']
msgid "Users found: {$a}"
msgstr ""

#: $string['viewconversation']
msgid "View conversation"
msgstr ""
