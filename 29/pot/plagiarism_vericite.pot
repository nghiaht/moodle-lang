#. extracted from en\plagiarism_vericite.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'plagiarism_vericite', language 'en', branch 'MOODLE_29_STABLE'
#.  *
#.  * @package   plagiarism_vericite
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['advanced_settings']
msgid "Advanced Settings"
msgstr ""

#: $string['defaultAssignment']
msgid ""
"This setting will be the default setting when creating a new assignment."
msgstr ""

#: $string['disable_dynamic_inline']
msgid "Disable dynamic inline submissions"
msgstr ""

#: $string['disable_dynamic_inline_help']
msgid ""
"Inline submissions will only be submitted once and modifications will not be "
"re-submitted."
msgstr ""

#: $string['enable_debugging']
msgid "Enable debugging"
msgstr ""

#: $string['enable_debugging_help']
msgid ""
"Turn debugging on for the VeriCite module. Errors will be printed to the php "
"error log."
msgstr ""

#: $string['pluginname']
msgid "VeriCite"
msgstr ""

#: $string['savedconfigsuccess']
msgid "Plagiarism Settings Saved"
msgstr ""

#: $string['similarity']
msgid "Similarity"
msgstr ""

#: $string['studentdisclosure']
msgid "Student Disclosure"
msgstr ""

#: $string['studentdisclosuredefault']
msgid ""
"All text and files uploaded will be submitted to VeriCite, a plagiarism "
"detection service"
msgstr ""

#: $string['studentdisclosure_help']
msgctxt "$string['studentdisclosure_help']"
msgid ""
msgstr ""

#: $string['studentreportvericite']
msgid "Allow students to see reports"
msgstr ""

#: $string['studentreportvericite_help']
msgid "Select checkbox if you want to allow students to see their reports."
msgstr ""

#: $string['studentscorevericite']
msgid "Allow students to see scores"
msgstr ""

#: $string['studentscorevericite_help']
msgid "Select checkbox if you want to allow students to see their scores."
msgstr ""

#: $string['usevericite']
msgid "Use VeriCite Plagiarism Service"
msgstr ""

#: $string['usevericite_help']
msgid ""
"This setting will be the default setting when creating a new assignment.  "
"Select checkbox if you want new assignments to use VeriCite by default."
msgstr ""

#: $string['vericite']
msgid "VeriCite plagiarism plugin"
msgstr ""

#: $string['vericiteaccountid']
msgid "Account Id"
msgstr ""

#: $string['vericiteaccountid_help']
msgid "This is the id given to you from Longsight"
msgstr ""

#: $string['vericiteapi']
msgid "API URL"
msgstr ""

#: $string['vericiteapi_help']
msgid "This is the API url given to you from Longsight"
msgstr ""

#: $string['vericitedefaultsettings']
msgid "Default settings for new assignments:"
msgstr ""

#: $string['vericitedefaultsettingsforums']
msgid "Default settings for new forums:"
msgstr ""

#: $string['vericiteexplain']
msgid "For more information on this plugin see:"
msgstr ""

#: $string['vericitesecretkey']
msgid "Secret Key"
msgstr ""

#: $string['vericitesecretkey_help']
msgid "This is the secret given to you from Longsight"
msgstr ""
