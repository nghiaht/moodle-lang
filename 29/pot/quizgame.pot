#. extracted from en\quizgame.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'quizgame', language 'en', branch 'MOODLE_28_STABLE'
#.  *
#.  * @package   quizgame
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['emptyquiz']
msgid "There are no multiple choice questions in the selected category."
msgstr ""

#: $string['endofgame']
msgid "Your score was: {$a}. Press space or click to restart."
msgstr ""

#: $string['modulename']
msgctxt "$string['modulename']"
msgid "Quizventure"
msgstr ""

#: $string['modulename_help']
msgid "Use the Quizventure module to give the students a fun way of learning."
msgstr ""

#: $string['modulenameplural']
msgid "Quizventure games"
msgstr ""

#: $string['pluginadministration']
msgid "Quizventure administration"
msgstr ""

#: $string['pluginname']
msgctxt "$string['pluginname']"
msgid "Quizventure"
msgstr ""

#: $string['quizgame']
msgctxt "$string['quizgame']"
msgid "Quizventure"
msgstr ""

#: $string['quizgame:addinstance']
msgid "Add a Quizventure instance"
msgstr ""

#: $string['quizgamefieldset']
msgid "Custom example fieldset"
msgstr ""

#: $string['quizgamename']
msgid "Quizventure name"
msgstr ""

#: $string['quizgamename_help']
msgid "What is the name of this Quizventure?"
msgstr ""

#: $string['quizgame:view']
msgid "View Quizventure"
msgstr ""

#: $string['score']
msgid "Score: {$a->score} Lives: {$a->lives}"
msgstr ""

#: $string['sound']
msgid "Sound"
msgstr ""

#: $string['spacetostart']
msgid "Press space or click to start"
msgstr ""
