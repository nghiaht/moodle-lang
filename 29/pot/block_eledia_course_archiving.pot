#. extracted from en\block_eledia_course_archiving.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'block_eledia_course_archiving', language 'en', branch 'MOODLE_28_STABLE'
#.  *
#.  * @package   block_eledia_course_archiving
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['archive']
msgid "Start Archiving"
msgstr ""

#: $string['configure_description']
msgid ""
"Here you can configure the archiving process. All courses which are located "
"directly in the source categories\n"
"    will be checked against their course start date. If the date is within "
"the timespan of now and the choosen days in the past, the course will be "
"archived.\n"
"    This means the course will be set invisible, moved to the confiruged "
"archive category and all student users will be unenroled.\n"
"    In a second step all courses in archive categorie are checked against "
"their course start date.\n"
"    If it is more than the chosen number of days in the past the course will "
"be deleted.<br /><br />\n"
"    The process can be initiated through a form which is linked in the "
"block. The block can be added to the main page only."
msgstr ""

#: $string['confirm_archiving']
msgid ""
"The follwoing courses will be archived:<br />\n"
"<br />\n"
"{$a->archived}<br />\n"
"<br />\n"
"The follwoing courses will be deleted:<br />\n"
"<br />\n"
"{$a->deleted}"
msgstr ""

#: $string['confirm_header']
msgid "Confirm Archiving"
msgstr ""

#: $string['days']
msgid "Number of days to archive"
msgstr ""

#: $string['eledia_course_archiving:addinstance']
msgid "add course archiving block"
msgstr ""

#: $string['eledia_course_archiving:use']
msgid "use course archiving block"
msgstr ""

#: $string['notice']
msgid ""
"The follwoing courses were archived:<br />\n"
"<br />\n"
"{$a->archived}<br />\n"
"<br />\n"
"The follwoing courses where deleted:<br />\n"
"<br />\n"
"{$a->deleted}"
msgstr ""

#: $string['pluginname']
msgctxt "$string['pluginname']"
msgid "Course Archiving"
msgstr ""

#: $string['remove_error']
msgid "- Errors while removing"
msgstr ""

#: $string['remove_success']
msgid "- Successful removed"
msgstr ""

#: $string['sourcecat']
msgid "Categories to archivate"
msgstr ""

#: $string['targetcat']
msgid "Archive categorie"
msgstr ""

#: $string['title']
msgctxt "$string['title']"
msgid "Course Archiving"
msgstr ""
