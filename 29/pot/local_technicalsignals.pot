#. extracted from en\local_technicalsignals.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'local_technicalsignals', language 'en', branch 'MOODLE_23_STABLE'
#.  *
#.  * @package   local_technicalsignals
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['adminmessage']
msgid "Administrator message"
msgstr ""

#: $string['adminmessagecolor']
msgid "Technical notice background color"
msgstr ""

#: $string['adminmessagedesc']
msgid "Technical notice to users at screen top. Leave blanck to disable"
msgstr ""

#: $string['blue']
msgid "Blue"
msgstr ""

#: $string['globaladminmessage']
msgid "Global LNET tech notice"
msgstr ""

#: $string['globaladminmessagecolor']
msgid "Global tech notice background color"
msgstr ""

#: $string['globaladminmessagedesc']
msgid ""
"Technical notice displayed on all nodes of the MNET. Leave blank to disable"
msgstr ""

#: $string['globalmessageprefix']
msgid "<b>Global network notice</b>"
msgstr ""

#: $string['green']
msgid "Green"
msgstr ""

#: $string['orange']
msgid "Orange"
msgstr ""

#: $string['pluginname']
msgid "Exploitation technical advices"
msgstr ""

#: $string['red']
msgid "Red"
msgstr ""

#: $string['technicalsignals:manage']
msgid "Manage technical signals"
msgstr ""

#: $string['yellow']
msgid "Yellow"
msgstr ""
