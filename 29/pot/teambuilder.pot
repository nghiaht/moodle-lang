#. extracted from en\teambuilder.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'teambuilder', language 'en', branch 'MOODLE_28_STABLE'
#.  *
#.  * @package   teambuilder
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['buildteams']
msgid "Build Teams"
msgstr ""

#: $string['intro']
msgid "Introduction"
msgstr ""

#: $string['intro_help']
msgid "The introduction to your Team Builder instance."
msgstr ""

#: $string['modulename']
msgctxt "$string['modulename']"
msgid "Team Builder"
msgstr ""

#: $string['modulenameplural']
msgid "Team Builders"
msgstr ""

#: $string['name']
msgid "Name"
msgstr ""

#: $string['pluginadministration']
msgid "Team Builder administration"
msgstr ""

#: $string['pluginname']
msgctxt "$string['pluginname']"
msgid "Team Builder"
msgstr ""

#: $string['preview']
msgid "Preview"
msgstr ""

#: $string['questionnaire']
msgid "Questionnaire"
msgstr ""

#: $string['teambuilder']
msgctxt "$string['teambuilder']"
msgid "Team Builder"
msgstr ""

#: $string['teambuilder:addinstance']
msgid "Add a new teambuilder module"
msgstr ""

#: $string['teambuilder:build']
msgid "Build teams from survey response"
msgstr ""

#: $string['teambuilder:create']
msgid "Create survey"
msgstr ""

#: $string['teambuilder:respond']
msgid "Respond to Questionnaire"
msgstr ""
