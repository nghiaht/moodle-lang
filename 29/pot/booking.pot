#. extracted from en\booking.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'booking', language 'en', branch 'MOODLE_27_STABLE'
#.  *
#.  * @package   booking
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['activitycompletionsuccess']
msgid "All selected users have been marked for activity completion"
msgstr ""

#: $string['addcategory']
msgid "Edit categories"
msgstr ""

#: $string['addeditbooking']
msgid "Edit booking"
msgstr ""

#: $string['additionalfields']
msgid "Additional fields"
msgstr ""

#: $string['addmorebookings']
msgid "Add more bookings"
msgstr ""

#: $string['addnewbookingoption']
msgid "Add a new booking option"
msgstr ""

#: $string['addnewcategory']
msgid "Add new category"
msgstr ""

#: $string['addnewinstitution']
msgid "Add new institution"
msgstr ""

#: $string['addnewtagtemplate']
msgid "Add new tag template"
msgstr ""

#: $string['address']
msgid "Address"
msgstr ""

#: $string['addteachers']
msgid "Add teachers"
msgstr ""

#: $string['addtocalendar']
msgid "Add to calendar"
msgstr ""

#: $string['addtogroup']
msgid "Automatically enrol users in group"
msgstr ""

#: $string['addtogroup_help']
msgid ""
"Automatically enrol users in group - group will be created automatically "
"with name \"Bookin name - Option name\""
msgstr ""

#: $string['advancedoptions']
msgid "Advanced options"
msgstr ""

#: $string['agreetobookingpolicy']
msgid "I have read and agree to the following booking policies"
msgstr ""

#: $string['allbookingoptions']
msgid "Download users for all booking options"
msgstr ""

#: $string['allchangessave']
msgid "All changes have been saved."
msgstr ""

#: $string['allmailssend']
msgid "All emails to users have been sent sucesfully!"
msgstr ""

#: $string['allowdelete']
msgid "Allow users to cancel their booking themselves"
msgstr ""

#: $string['allowupdate']
msgid "Allow booking to be updated"
msgstr ""

#: $string['answered']
msgid "Answered"
msgstr ""

#: $string['associatedcourse']
msgid "Associated course"
msgstr ""

#: $string['attachedfiles']
msgid "Attached files"
msgstr ""

#: $string['attachical']
msgid "Attach ical events"
msgstr ""

#: $string['attachicaldesc']
msgid ""
"Email notifications will include an attached ical event, if this is enabled"
msgstr ""

#: $string['autoenrol']
msgid "Automatically enrol users"
msgstr ""

#: $string['autoenrol_help']
msgid ""
"If selected, users will be enroled onto the relevant course as soon as they "
"make the booking and unenroled from that course as soon as the booking is "
"cancelled."
msgstr ""

#: $string['availability']
msgid "Still available"
msgstr ""

#: $string['available']
msgctxt "$string['available']"
msgid "Places available"
msgstr ""

#: $string['backtoresponses']
msgid "<< Back to responses"
msgstr ""

#: $string['booked']
msgid "Booked"
msgstr ""

#: $string['bookedpast']
msgid "Booked (course finished)"
msgstr ""

#: $string['bookedtext']
msgid "Booking confirmation"
msgstr ""

#: $string['bookedtext_help']
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['bookedusers']
msgid "Booked users"
msgstr ""

#: $string['booking']
msgctxt "$string['booking']"
msgid "Booking"
msgstr ""

#: $string['booking:addinstance']
msgid "Add new booking"
msgstr ""

#: $string['bookingattachment']
msgid "Attachment"
msgstr ""

#: $string['bookingcategory']
msgctxt "$string['bookingcategory']"
msgid "Category"
msgstr ""

#: $string['booking:choose']
msgid "Book"
msgstr ""

#: $string['bookingclose']
msgid "Until"
msgstr ""

#: $string['booking:communicate']
msgid "Can communicate"
msgstr ""

#: $string['bookingdeleted']
msgid "Your booking was cancelled"
msgstr ""

#: $string['booking:deleteresponses']
msgid "Delete responses"
msgstr ""

#: $string['booking:downloadresponses']
msgid "Download responses"
msgstr ""

#: $string['bookingduration']
msgid "Duration"
msgstr ""

#: $string['bookingfull']
msgid "There are no available places"
msgstr ""

#: $string['bookingmanagererror']
msgid ""
"The username entered is not valid. Either it does not exist or there are "
"more then one users with this username (example: if you have mnet and local "
"authentication enabled)"
msgstr ""

#: $string['bookingmeanwhilefull']
msgid "Meanwhile someone took already the last place"
msgstr ""

#: $string['bookingname']
msgid "Booking name"
msgstr ""

#: $string['bookingopen']
msgid "Open"
msgstr ""

#: $string['bookingorganizatorname']
msgid "Organizer name"
msgstr ""

#: $string['bookingpoints']
msgid "Course points"
msgstr ""

#: $string['bookingpolicy']
msgid "Booking policy"
msgstr ""

#: $string['bookingpollurl']
msgctxt "$string['bookingpollurl']"
msgid "Poll url"
msgstr ""

#: $string['bookingpollurlteachers']
msgctxt "$string['bookingpollurlteachers']"
msgid "Teachers poll url"
msgstr ""

#: $string['booking:readresponses']
msgid "Read responses"
msgstr ""

#: $string['bookingsaved']
msgid ""
"Your booking was successfully saved. You can now proceed to book other "
"courses."
msgstr ""

#: $string['booking:sendpollurl']
msgctxt "$string['booking:sendpollurl']"
msgid "Send poll url"
msgstr ""

#: $string['booking:sendpollurltoteachers']
msgid "Send poll url to teachers"
msgstr ""

#: $string['booking:subscribeusers']
msgid "Make bookings for other users"
msgstr ""

#: $string['bookingtags']
msgid "Tags"
msgstr ""

#: $string['bookingtext']
msgid "Booking text"
msgstr ""

#: $string['booking:updatebooking']
msgid "Manage booking options"
msgstr ""

#: $string['booknow']
msgid "Book now"
msgstr ""

#: $string['bookotherusers']
msgid "Book other users"
msgstr ""

#: $string['booktootherbooking']
msgid "Book users to other booking"
msgstr ""

#: $string['btnbooknowname']
msgid "Name of button \"Book now\""
msgstr ""

#: $string['btncacname']
msgid "Name of button \"Confirm activity completion\""
msgstr ""

#: $string['btncancelname']
msgid "Name of button \"Cancel booking\""
msgstr ""

#: $string['cancancelbook']
msgid "Allow user to cancel or book their booking when it is started?"
msgstr ""

#: $string['cancel']
msgid "Cancel"
msgstr ""

#: $string['cancelbooking']
msgid "Cancel booking"
msgstr ""

#: $string['categories']
msgid "Categories"
msgstr ""

#: $string['category']
msgctxt "$string['category']"
msgid "Category"
msgstr ""

#: $string['categoryname']
msgid "Category name"
msgstr ""

#: $string['choosecourse']
msgid "Choose a course"
msgstr ""

#: $string['closed']
msgid "Booking closed"
msgstr ""

#: $string['completed']
msgid "Completed"
msgstr ""

#: $string['conectedbooking']
msgid "Connected booking"
msgstr ""

#: $string['conectedbooking_help']
msgctxt "$string['conectedbooking_help']"
msgid ""
msgstr ""

#: $string['conectedoption']
msgid "Connected option"
msgstr ""

#: $string['conectedoption_help']
msgctxt "$string['conectedoption_help']"
msgid ""
msgstr ""

#: $string['confirmactivitycompletion']
msgid "Confirm activity completion"
msgstr ""

#: $string['confirmationmessage']
msgid ""
"Your booking has been registered\n"
"\n"
"\n"
"Booking status: {$a->status}\n"
"Participant:   {$a->participant}\n"
"Course:   {$a->title}\n"
"Date: {$a->startdate} {$a->starttime} - {$a->enddate} {$a->endtime}\n"
"To view all your booked courses click on the following link: "
"{$a->bookinglink}\n"
"The associated course can be found here: {$a->courselink}"
msgstr ""

#: $string['confirmationmessagesettings']
msgid "Confirmation email settings"
msgstr ""

#: $string['confirmationmessagewaitinglist']
msgid ""
"Hello {$a->participant},\n"
"\n"
"Your booking request has been registered\n"
"\n"
"Booking status: {$a->status}\n"
"Participant:   {$a->participant}\n"
"Course:   {$a->title}\n"
"Date: {$a->startdate} {$a->starttime} - {$a->enddate} {$a->endtime}\n"
"To view all your booked courses click on the following link: "
"{$a->bookinglink}"
msgstr ""

#: $string['confirmationsubject']
msgid "Booking confirmation for {$a->title}"
msgstr ""

#: $string['confirmationsubjectbookingmanager']
msgid "New booking for {$a->title} by {$a->participant}"
msgstr ""

#: $string['confirmationsubjectwaitinglist']
msgctxt "$string['confirmationsubjectwaitinglist']"
msgid "Booking status for {$a->title}"
msgstr ""

#: $string['confirmationsubjectwaitinglistmanager']
msgctxt "$string['confirmationsubjectwaitinglistmanager']"
msgid "Booking status for {$a->title}"
msgstr ""

#: $string['confirmbookingoffollowing']
msgid "Please confirm the booking of following course"
msgstr ""

#: $string['confirmdeletebookingoption']
msgid "Do you really want to delete this booking option?"
msgstr ""

#: $string['coursedate']
msgctxt "$string['coursedate']"
msgid "Date"
msgstr ""

#: $string['courseendtime']
msgid "End time of the course"
msgstr ""

#: $string['coursestarttime']
msgid "Start time of the course"
msgstr ""

#: $string['createdby']
msgid "Booking module created by edulabs.org"
msgstr ""

#: $string['csvfile']
msgid "CSV file"
msgstr ""

#: $string['csvfile_help']
msgid "CSV file must contain only one column named Institution."
msgstr ""

#: $string['dateerror']
msgid "Wrong date in line {$a}:"
msgstr ""

#: $string['dateparseformat']
msgid "Date parse format"
msgstr ""

#: $string['dateparseformat_help']
msgid ""
"Please, use date format like specified in CSV file. Help with <a href=\""
"http://php.net/manual/en/function.date.php\">this</a> resource for options."
msgstr ""

#: $string['days']
msgid "{$a} days"
msgstr ""

#: $string['daystonotify']
msgid "How many days before start of event to notify participaints?"
msgstr ""

#: $string['defaultbookingoption']
msgid "Default booking options"
msgstr ""

#: $string['defaultdateformat']
msgid "j.n.Y H:i:s"
msgstr ""

#: $string['deletebooking']
msgid ""
"Do you really want to unsubscribe from following course? <br /><br /> <b>{$a}"
" </b>"
msgstr ""

#: $string['deletebookingoption']
msgid "Delete this booking option"
msgstr ""

#: $string['deletecategory']
msgid "Delete"
msgstr ""

#: $string['deletedbookingmessage']
msgid ""
"Booking for following course deleted: {$a->title}\n"
"\n"
"User: {$a->participant}\n"
"Title: {$a->title}\n"
"Date: {$a->startdate} {$a->starttime} - {$a->enddate} {$a->endtime}\n"
"Course: {$a->courselink}\n"
"Booking link: {$a->bookinglink}"
msgstr ""

#: $string['deletedbookingsubject']
msgid "Deleted booking: {$a->title} by {$a->participant}"
msgstr ""

#: $string['deletedbookingusermessage']
msgid ""
"Hello {$a->participant},\n"
"\n"
"Your booking for {$a->title} ({$a->startdate} {$a->starttime}) has been "
"cancelled."
msgstr ""

#: $string['deletedbookingusersubject']
msgid "Booking for {$a->title} cancelled"
msgstr ""

#: $string['deletedtext']
msgid "Cancelled booking message"
msgstr ""

#: $string['deletedtext_help']
msgctxt "$string['deletedtext_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['deletesubcategory']
msgid "Please, first delete all subcategories of this category!"
msgstr ""

#: $string['deleteuserfrombooking']
msgid "Do you really want to delete the users from the booking?"
msgstr ""

#: $string['donotselectcourse']
msgid "No course selected"
msgstr ""

#: $string['download']
msgid "Download"
msgstr ""

#: $string['downloadallresponses']
msgid "Download all responses for all booking options"
msgstr ""

#: $string['downloadusersforthisoptionods']
msgid "Download users as .ods"
msgstr ""

#: $string['downloadusersforthisoptionxls']
msgid "Download users as .xls"
msgstr ""

#: $string['editcategory']
msgctxt "$string['editcategory']"
msgid "Edit"
msgstr ""

#: $string['editinstitutions']
msgid "Edit institutions"
msgstr ""

#: $string['edittag']
msgctxt "$string['edittag']"
msgid "Edit"
msgstr ""

#: $string['enablecompletion']
msgid "Enable activity completion."
msgstr ""

#: $string['enablecompletiongroup']
msgid "Activity completion"
msgstr ""

#: $string['endtimenotset']
msgid "End date not set"
msgstr ""

#: $string['entervalidurl']
msgid "Please, enter a valid URL!"
msgstr ""

#: $string['error:failedtosendconfirmation']
msgid ""
"The following user did not receive a confirmation mail\n"
"\n"
"Booking status: {$a->status}\n"
"Participant:   {$a->participant}\n"
"Course:   {$a->title}\n"
"Date: {$a->startdate} {$a->starttime} - {$a->enddate} {$a->endtime}\n"
"Link: {$a->bookinglink}\n"
"Associated course: {$a->courselink}"
msgstr ""

#: $string['eventbooking_cancelled']
msgid "Booking canceled"
msgstr ""

#: $string['eventbookingoption_booked']
msgid "Booking option booked"
msgstr ""

#: $string['eventduration']
msgid "Event duration"
msgstr ""

#: $string['eventpoints']
msgid "Points"
msgstr ""

#: $string['eventreport_viewed']
msgid "Report viewed"
msgstr ""

#: $string['eventtype']
msgid "Event type"
msgstr ""

#: $string['excelfile']
msgid "CSV file with activity completion"
msgstr ""

#: $string['existingsubscribers']
msgid "Existing subscribers"
msgstr ""

#: $string['expired']
msgid "Sorry, this activity closed on {$a} and is no longer available"
msgstr ""

#: $string['fillinatleastoneoption']
msgid "You need to provide at least two possible answers."
msgstr ""

#: $string['forcourse']
msgid "for course"
msgstr ""

#: $string['full']
msgid "Full"
msgstr ""

#: $string['fullname']
msgid "Full name"
msgstr ""

#: $string['goenrol']
msgid "Go to registration"
msgstr ""

#: $string['gotobooking']
msgid "<< Bookings"
msgstr ""

#: $string['gotop']
msgid "Go to top"
msgstr ""

#: $string['groupname']
msgid "Group name"
msgstr ""

#: $string['havetologin']
msgid "You have to log in before you can submit your booking"
msgstr ""

#: $string['hours']
msgid "{$a} hours"
msgstr ""

#: $string['howmanyusers']
msgid "How many users you can book?"
msgstr ""

#: $string['howmanyusers_help']
msgctxt "$string['howmanyusers_help']"
msgid ""
msgstr ""

#: $string['importcsvbookingoption']
msgid "Import CSV with booking options"
msgstr ""

#: $string['importcsvtitle']
msgid "Import CSV"
msgstr ""

#: $string['importexcelbutton']
msgctxt "$string['importexcelbutton']"
msgid "Import activity completion"
msgstr ""

#: $string['importexceltitle']
msgctxt "$string['importexceltitle']"
msgid "Import activity completion"
msgstr ""

#: $string['importfinished']
msgid "Importing finished!"
msgstr ""

#: $string['institution']
msgid "Institution"
msgstr ""

#: $string['institutionname']
msgid "Institution name"
msgstr ""

#: $string['institutions']
msgid "Institutions"
msgstr ""

#: $string['lblsputtname']
msgid "Name of label \"Send poll url to teachers\""
msgstr ""

#: $string['lblteachname']
msgid "Name of label \"Teachers\""
msgstr ""

#: $string['limit']
msgid "Limit"
msgstr ""

#: $string['limitanswers']
msgid "Limit the number of participants"
msgstr ""

#: $string['limitanswers_help']
msgid ""
"If you change this option and you have booked people, you can remove them "
"without notification!"
msgstr ""

#: $string['location']
msgid "Location"
msgstr ""

#: $string['mailconfirmationsent']
msgid "You will shortly receive a confirmation e-mail"
msgstr ""

#: $string['managebooking']
msgid "Manage"
msgstr ""

#: $string['maxoverbooking']
msgid "Max. number of places on waiting list"
msgstr ""

#: $string['maxparticipantsnumber']
msgid "Max. number of participants"
msgstr ""

#: $string['maxperuser']
msgid "Max current bookings per user"
msgstr ""

#: $string['maxperuser_help']
msgid ""
"The maximum number of bookings an individual user can make in this activity "
"at once. After an event end time has passed, it is no longer counted against "
"this limit."
msgstr ""

#: $string['maxperuserwarning']
msgid "You currently have {$a->count}/{$a->limit} maximum bookings"
msgstr ""

#: $string['messagesend']
msgid "You message was sucesfully send."
msgstr ""

#: $string['messagesubject']
msgid "Subject"
msgstr ""

#: $string['messagetext']
msgid "Message"
msgstr ""

#: $string['minutes']
msgid "{$a} minutes"
msgstr ""

#: $string['modulename']
msgctxt "$string['modulename']"
msgid "Booking"
msgstr ""

#: $string['modulenameplural']
msgid "Bookings"
msgstr ""

#: $string['mustchooseone']
msgid "You must choose an option before saving.  Nothing was saved."
msgstr ""

#: $string['mustfilloutuserinfobeforebooking']
msgid ""
"Befor proceeding to the booking form, please fill in some personal booking "
"information"
msgstr ""

#: $string['no']
msgid "No"
msgstr ""

#: $string['nobookingselected']
msgid "No booking option selected"
msgstr ""

#: $string['nocourse']
msgid "No course selected for this booking option"
msgstr ""

#: $string['noguestchoose']
msgid "Sorry, guests are not allowed to enter data"
msgstr ""

#: $string['noresultsviewable']
msgid "The results are not currently viewable."
msgstr ""

#: $string['norighttobook']
msgid ""
"Booking is not possible for your user role. Please contact the site "
"administrator to give you the appropriate rights or sign in."
msgstr ""

#: $string['nosubscribers']
msgid "There are no teachers assigned!"
msgstr ""

#: $string['notbooked']
msgid "Not yet booked"
msgstr ""

#: $string['notconectedbooking']
msgid "Not connected"
msgstr ""

#: $string['noteacherfound']
msgid "No teacher found in line {$a}:"
msgstr ""

#: $string['notificationsubject']
msgid "Upcoming course..."
msgstr ""

#: $string['notificationtext']
msgid "Notification text shown on activity completion."
msgstr ""

#: $string['notificationtext_help']
msgctxt "$string['notificationtext_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['notificationtextmessage']
msgid ""
"Course will start:\n"
"\n"
"Course:   {$a->title}\n"
"Date: {$a->startdate} {$a->starttime} - {$a->enddate} {$a->endtime}"
msgstr ""

#: $string['notificationtextsubject']
msgid "Info about course start!"
msgstr ""

#: $string['notopenyet']
msgid "Sorry, this activity is not available until {$a}"
msgstr ""

#: $string['nouserfound']
msgid "No user found:"
msgstr ""

#: $string['nousers']
msgid "No users!"
msgstr ""

#: $string['onlythisbookingurl']
msgid "Only this booking URL"
msgstr ""

#: $string['onwaitinglist']
msgid "You are on the waiting list"
msgstr ""

#: $string['optionid']
msgid "Option ID"
msgstr ""

#: $string['organizatorname']
msgid "Organizator name"
msgstr ""

#: $string['placesavailable']
msgctxt "$string['placesavailable']"
msgid "Places available"
msgstr ""

#: $string['pluginadministration']
msgid "Booking administration"
msgstr ""

#: $string['pluginname']
msgctxt "$string['pluginname']"
msgid "Booking"
msgstr ""

#: $string['pollstrftimedate']
msgid "%Y-%m-%d"
msgstr ""

#: $string['pollurl']
msgctxt "$string['pollurl']"
msgid "Poll url"
msgstr ""

#: $string['pollurl_help']
msgctxt "$string['pollurl_help']"
msgid ""
"You can use any of the following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['pollurlteachers']
msgctxt "$string['pollurlteachers']"
msgid "Teachers poll url"
msgstr ""

#: $string['pollurlteachers_help']
msgctxt "$string['pollurlteachers_help']"
msgid ""
"You can use any of the following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['pollurlteacherstext']
msgid "Teachers send poll url"
msgstr ""

#: $string['pollurlteacherstext_help']
msgctxt "$string['pollurlteacherstext_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['pollurlteacherstextmessage']
msgid ""
"Please, take the survey\n"
"\n"
"Survey url {pollurlteachers}"
msgstr ""

#: $string['pollurlteacherstextsubject']
msgctxt "$string['pollurlteacherstextsubject']"
msgid "Please, take the survey"
msgstr ""

#: $string['pollurltext']
msgctxt "$string['pollurltext']"
msgid "Send poll url"
msgstr ""

#: $string['pollurltext_help']
msgctxt "$string['pollurltext_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['pollurltextmessage']
msgid ""
"Please, take the survey\n"
"\n"
"Survey url {pollurl}"
msgstr ""

#: $string['pollurltextsubject']
msgctxt "$string['pollurltextsubject']"
msgid "Please, take the survey"
msgstr ""

#: $string['potentialsubscribers']
msgid "Potential subscribers"
msgstr ""

#: $string['removeafterminutes']
msgid "Remove activity completion after N minutes"
msgstr ""

#: $string['removeresponses']
msgid "Remove all responses"
msgstr ""

#: $string['reset']
msgid "Reset"
msgstr ""

#: $string['responses']
msgid "Responses"
msgstr ""

#: $string['responsesto']
msgid "Responses to {$a}"
msgstr ""

#: $string['rootcategory']
msgid "Root"
msgstr ""

#: $string['savenewtagtemplate']
msgid "Save"
msgstr ""

#: $string['searchDate']
msgctxt "$string['searchDate']"
msgid "Date"
msgstr ""

#: $string['searchFinished']
msgid "Course completed"
msgstr ""

#: $string['searchName']
msgid "Name"
msgstr ""

#: $string['searchSurname']
msgid "Surname"
msgstr ""

#: $string['searchtag']
msgid "Search tags"
msgstr ""

#: $string['select']
msgid "Selection"
msgstr ""

#: $string['selectatleastoneuser']
msgid "Please, select at least 1 user!"
msgstr ""

#: $string['selectcategory']
msgid "Select category"
msgstr ""

#: $string['sendconfirmmail']
msgid "Send confirmation email"
msgstr ""

#: $string['sendconfirmmailtobookingmanger']
msgid "Send confirmation email to booking manager"
msgstr ""

#: $string['sendcustommessage']
msgid "Send custom message"
msgstr ""

#: $string['sendmailtobooker']
msgid ""
"Book other users page: Send mail to user who books instead to users who are "
"booked"
msgstr ""

#: $string['sendmailtobooker_help']
msgid ""
"Activate this option in order to send booking confirmation mails to\n"
"  the user who books other users instead to users, who have been added to a "
"booking option.\n"
"  This is only relevant for bookings made on the page \"book other users\"."
msgstr ""

#: $string['showactive']
msgid "Show only active bookings"
msgstr ""

#: $string['showallbookings']
msgid "Show booking overview for all bookings"
msgstr ""

#: $string['showinapi']
msgid "Show in API?"
msgstr ""

#: $string['showmybookings']
msgid "Show only my bookings"
msgstr ""

#: $string['spaceleft']
msgid "space available"
msgstr ""

#: $string['spacesleft']
msgid "spaces available"
msgstr ""

#: $string['startendtimeknown']
msgid "Start and end time of course are known"
msgstr ""

#: $string['starttimenotset']
msgid "Start date not set"
msgstr ""

#: $string['statuschangebookedmessage']
msgid ""
"Hello {$a->participant},\n"
"\n"
"Your booking status has changed. You are now registered in {$a->title}.\n"
"\n"
"Booking status: {$a->status}\n"
"Participant:   {$a->participant}\n"
"Course:   {$a->title}\n"
"Date: {$a->startdate} {$a->starttime} - {$a->enddate} {$a->endtime}\n"
"To view all your booked courses click on the following link: "
"{$a->bookinglink}\n"
"The associated course can be found here: {$a->courselink}"
msgstr ""

#: $string['statuschangebookedsubject']
msgid "Booking status changed for {$a->title}"
msgstr ""

#: $string['statuschangetext']
msgid "Status change message"
msgstr ""

#: $string['statuschangetext_help']
msgctxt "$string['statuschangetext_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['submitandaddnew']
msgid "Save and add new"
msgstr ""

#: $string['subscribersto']
msgid "Teachers for  '{$a}"
msgstr ""

#: $string['subscribetocourse']
msgid "Enrol users in the course"
msgstr ""

#: $string['subscribeuser']
msgid "Do you really want to enrol the users in the following course"
msgstr ""

#: $string['sucesfulldeleted']
msgid "Category was sucesfully deleted!"
msgstr ""

#: $string['sucesfulldeletedinstitution']
msgid "Institution was sucesfully deleted"
msgstr ""

#: $string['tagsucesfullysaved']
msgid "Tag was sucesfully saved."
msgstr ""

#: $string['tagtag']
msgid "Tag"
msgstr ""

#: $string['tagtemplates']
msgid "Tag templates"
msgstr ""

#: $string['tagtext']
msgid "Text"
msgstr ""

#: $string['taken']
msgid "Taken"
msgstr ""

#: $string['teachers']
msgid "Teachers"
msgstr ""

#: $string['timecreated']
msgid "Time created"
msgstr ""

#: $string['timerestrict']
msgid "Restrict answering to this time period"
msgstr ""

#: $string['to']
msgid "to"
msgstr ""

#: $string['toomuchusersbooked']
msgid "The max number of users you can book is: {$a}"
msgstr ""

#: $string['unlimited']
msgid "Unlimited"
msgstr ""

#: $string['updatebooking']
msgid "Edit this booking option"
msgstr ""

#: $string['usedinbooking']
msgid "You can't delete this category, because you're using it in booking!"
msgstr ""

#: $string['userdownload']
msgid "Download users"
msgstr ""

#: $string['userleave']
msgid "User leave booking"
msgstr ""

#: $string['userleavebookedmessage']
msgid ""
"Hello {$a->participant},\n"
"\n"
"You have sucesfully unsubscribed from {$a->title}."
msgstr ""

#: $string['userleavebookedsubject']
msgid "You have sucesfully unsubscribed from {$a->title}"
msgstr ""

#: $string['userleave_help']
msgctxt "$string['userleave_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['usernameofbookingmanager']
msgid "Username of the booking manager"
msgstr ""

#: $string['usernameofbookingmanager_help']
msgid ""
"Username of the user who will be displayed in the \"From\" field of the "
"confirmation notifications.\n"
"  If the option \"Send confirmation email to booking manager\" is enabled, "
"this is the user who receives a copy of the confirmation notifications."
msgstr ""

#: $string['userrssucesfullenroled']
msgid "All users has been sucesfully enroled!"
msgstr ""

#: $string['users']
msgid "<< Manage responses"
msgstr ""

#: $string['usersOnList']
msgid "User on list"
msgstr ""

#: $string['userssucesfullybooked']
msgid "All users have been sucesfully booked to other booking."
msgstr ""

#: $string['viewallresponses']
msgid "Manage {$a} responses"
msgstr ""

#: $string['waitinglist']
msgid "On waiting list"
msgstr ""

#: $string['waitinglisttaken']
msgid "On the waiting list"
msgstr ""

#: $string['waitinglistusers']
msgid "Users on waiting list"
msgstr ""

#: $string['waitingplacesavailable']
msgid "Waiting list places available"
msgstr ""

#: $string['waitingtext']
msgid "Waiting list confirmation"
msgstr ""

#: $string['waitingtext_help']
msgctxt "$string['waitingtext_help']"
msgid ""
"Leave this blank to use the site default text. You can use any of the "
"following placeholders in the text:\n"
"<ul>\n"
"<li>{status}</li>\n"
"<li>{participant}</li>\n"
"<li>{title}</li>\n"
"<li>{duration}</li>\n"
"<li>{starttime}</li>\n"
"<li>{endtime}</li>\n"
"<li>{startdate}</li>\n"
"<li>{enddate}</li>\n"
"<li>{courselink}</li>\n"
"<li>{bookinglink}</li>\n"
"<li>{pollurl}</li>\n"
"<li>{pollurlteachers}</li>\n"
"<li>{location}</li>\n"
"<li>{institution}</li>\n"
"<li>{address}</li>\n"
"<li>{eventtype}</li>\n"
"<li>{teacher}</li>\n"
"<li>{teacherN} - N is number of teacher ex. {teacher1}</li>\n"
"<li>{pollstartdate}</li>\n"
"<li>{qr_id} - Insert QR code with user id</li>\n"
"<li>{qr_username} - Insert QR code with user username</li>\n"
"</ul>"
msgstr ""

#: $string['waitspaceavailable']
msgid "Places on waiting list available"
msgstr ""

#: $string['withselected']
msgid "With selected users:"
msgstr ""

#: $string['wrongdataallfields']
msgid "Please, fill all fields!"
msgstr ""

#: $string['wrongfile']
msgid "Wrong file!"
msgstr ""

#: $string['yes']
msgid "Yes"
msgstr ""

#: $string['yourselection']
msgid "Your selection"
msgstr ""
