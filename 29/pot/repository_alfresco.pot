#. extracted from en\repository_alfresco.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'repository_alfresco', language 'en', branch 'MOODLE_29_STABLE'
#.  *
#.  * @package   repository_alfresco
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['alfresco_url']
msgid "Alfresco URL"
msgstr ""

#: $string['alfrescourltext']
msgid ""
"Afresco API URL should be: http://yoursite.com/alfresco/api or "
"http://yoursite.com/alfresco/soapapi for Alfresco 4.2.d or greater."
msgstr ""

#: $string['alfresco:view']
msgid "View alfresco repository"
msgstr ""

#: $string['configplugin']
msgid "Alfresco configuration"
msgstr ""

#: $string['notitle']
msgid "notitle"
msgstr ""

#: $string['password']
msgid "Password"
msgstr ""

#: $string['pluginname']
msgid "Alfresco repository"
msgstr ""

#: $string['pluginname_help']
msgid "A plug-in for Alfresco CMS"
msgstr ""

#: $string['security_key_notice_message_content']
msgid ""
"A recent security issue was discovered when using external links to the "
"Alfresco Moodle repository. Users were able to gain access to the accounts "
"of other users on the Alfresco server through the use of information "
"contained in these links (tokens). This feature has now been disabled, but "
"it is possible that the tokens contained within these links still allow "
"access to another user's account. For your own protection, it is important "
"that you restart your Alfresco server in order to expire the tokens."
msgstr ""

#: $string['security_key_notice_message_small']
msgid ""
"Due to a recent security issue found in the Alfresco repository, we advice "
"you to restart your Alfresco server."
msgstr ""

#: $string['security_key_notice_message_subject']
msgid "Alfresco repository security notice"
msgstr ""

#: $string['soapmustbeenabled']
msgid "SOAP extension must be enabled for alfresco plugin"
msgstr ""

#: $string['space']
msgid "Space"
msgstr ""

#: $string['username']
msgid "User name"
msgstr ""
