#. extracted from en\repository_onenote.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'repository_onenote', language 'en', branch 'MOODLE_29_STABLE'
#.  *
#.  * @package   repository_onenote
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['cachedef_foldername']
msgid "Folder name cache"
msgstr ""

#: $string['clientid']
msgid "Client ID"
msgstr ""

#: $string['configplugin']
msgid "Configure Microsoft OneNote"
msgstr ""

#: $string['oauthinfo']
msgid ""
"<p>To use this plugin, you must register your site <a href=\""
"https://account.live.com/developers/applications\">with Microsoft</a>.<p>As "
"part of the registration process, you will need to enter the following URL "
"as 'Redirect domain':</p><p>{$a->callbackurl}</p>Once registered, you will "
"be provided with a client ID and secret which can be entered here.</p>"
msgstr ""

#: $string['onenote:view']
msgid "View OneNote Notebooks"
msgstr ""

#: $string['pluginname']
msgid "Microsoft OneNote"
msgstr ""
