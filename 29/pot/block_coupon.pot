#. extracted from en\block_coupon.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'block_coupon', language 'en', branch 'MOODLE_29_STABLE'
#.  *
#.  * @package   block_coupon
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['action:coupon:delete']
msgid "Delete coupon"
msgstr ""

#: $string['action:coupon:delete:confirm']
msgid "Are you sure you wish to delete this coupon? This cannot be undone!"
msgstr ""

#: $string['and']
msgid "and"
msgstr ""

#: $string['blockname']
msgctxt "$string['blockname']"
msgid "Coupon"
msgstr ""

#: $string['button:next']
msgid "Next"
msgstr ""

#: $string['button:save']
msgid "Generate Coupons"
msgstr ""

#: $string['button:submit_coupon_code']
msgid "Submit Coupon"
msgstr ""

#: $string['cohort']
msgid "cohort"
msgstr ""

#: $string['confirm_coupons_sent_body']
msgid ""
"Hello,<br /><br />\n"
"\n"
"We'd like to inform you that all the coupons created by you on "
"{$a->timecreated} have been sent.<br /><br />\n"
"\n"
"With kind regards,<br /><br />\n"
"\n"
"Moodle administrator"
msgstr ""

#: $string['confirm_coupons_sent_subject']
msgid "All Coupons have been sent"
msgstr ""

#: $string['coupon:addinstance']
msgid "Add a new Coupon block"
msgstr ""

#: $string['coupon:administration']
msgid "Administrate the Coupon block"
msgstr ""

#: $string['coupon:deleted']
msgid "Coupon has been deleted"
msgstr ""

#: $string['coupon:generatecoupons']
msgid "Generate a new coupon"
msgstr ""

#: $string['coupon:inputcoupons']
msgid "Use a coupon to subscribe"
msgstr ""

#: $string['coupon_mail_content']
msgid ""
"Dear {$a->to_name},<br /><br />\n"
"\n"
"You are receiving this message because there have been newly generated "
"coupons. The coupons have been added in the attachment to this message.<br "
"/><br />\n"
"\n"
"With kind regards,<br /><br />\n"
"\n"
"{$a->from_name}"
msgstr ""

#: $string['coupon_mail_csv_content']
msgid ""
"Dear ##to_gender## ##to_name##,<br /><br />\n"
"\n"
"You have recently been enrolled for our training ##course_fullnames##.\n"
"During the course you have access to our Online Learning Environment: "
"##site_name##.<br /><br />\n"
"\n"
"In this environment, apart from the course materials, you will have the "
"possibility to network with fellow students.\n"
"The course will start with a number of preparation assignments, we kindly "
"request to take a look at them\n"
"at the latest 3 (work)days before the course starts.\n"
"Both you and the teacher can then decently prepare for the course.<br /><br "
"/>\n"
"\n"
"All course materials will be accessible for you, at the very latest 4 days "
"before the course starts.\n"
"It can always happen that the teacher requests extra materials to be added "
"at a later time, for example\n"
"after a physical session. If this happens, you will be abe to see this in "
"the learning environment\n"
"During meetings you will not receive any printed lesson materials, we advise "
"you to bring a laptop and/or tablet.<br /><br />\n"
"\n"
"You'll find the coupon to enter the course attached. This coupon os personal "
"and unique, and gives access to the appropriate courses for your education.\n"
"Please read the instructions on the coupon carefully.<br /><br />\n"
"\n"
"If you have any questions regarding creating an account or find any other "
"problems, you can contact the helpdesk.\n"
"Information can be found on out Learning Environment.\n"
"When nobody is available to answer your question, please leave your name, e-"
"mailaddress and phonenumber behind and we will get back to you as\n"
"soon as possible.<br /><br />\n"
"\n"
"We wish you good luck on the course.<br /><br />\n"
"\n"
"With kind regards,<br /><br />\n"
"\n"
"##site_name##"
msgstr ""

#: $string['coupon_mail_csv_content_cohorts']
msgid ""
"Dear ##to_gender## ##to_name##,<br /><br />\n"
"\n"
"You have recently been enrolled for our training **PLEASE FILL IN MANUALLY**."
"\n"
"During the course you have access to our Online Learning Environment: "
"##site_name##.<br /><br />\n"
"\n"
"In this environment, apart from the course materials, you will have the "
"possibility to network with fellow students.\n"
"The course will start with a number of preparation assignments, we kindly "
"request to take a look at them\n"
"at the latest 3 (work)days before the course starts.\n"
"Both you and the teacher can then decently prepare for the course.<br /><br "
"/>\n"
"\n"
"All course materials will be accessible for you, at the very latest 4 days "
"before the course starts.\n"
"It can always happen that the teacher requests extra materials to be added "
"at a later time, for example\n"
"after a physical session. If this happens, you will be abe to see this in "
"the learning environment\n"
"During meetings you will not receive any printed lesson materials, we advise "
"you to bring a laptop and/or tablet.<br /><br />\n"
"\n"
"You'll find the coupon to enter the course attached. This coupon os personal "
"and unique, and gives access to the appropriate courses for your education.\n"
"Please read the instructions on the coupon carefully.<br /><br />\n"
"\n"
"If you have any questions regarding creating an account or find any other "
"problems, you can contact the helpdesk.\n"
"Information can be found on out Learning Environment.\n"
"When nobody is available to answer your question, please leave your name, e-"
"mailaddress and phonenumber behind and we will get back to you as\n"
"soon as possible.<br /><br />\n"
"\n"
"We wish you good luck on the course.<br /><br />\n"
"\n"
"With kind regards,<br /><br />\n"
"\n"
"##site_name##"
msgstr ""

#: $string['coupon_mail_subject']
msgid "Moodle Coupon generated"
msgstr ""

#: $string['coupon:myaddinstance']
msgid "Add a new Coupon block to the My Moodle page"
msgstr ""

#: $string['coupon_recipients_desc']
msgid ""
"The following columns are required to be present in the uploaded CSV, "
"regardless of order: E-mail, Gender, Name.<br/>\n"
"For every given person in the CSV, a coupon is generated and emailed to the "
"user.<br/>\n"
"Please take note these coupons will be created a-synchronous by a background "
"task; <i>not</i> instantly.\n"
"This is because the process of generating coupons may be quite lengthy, "
"especially for a large amount of users."
msgstr ""

#: $string['coupons_ready_to_send']
msgid ""
"Your coupon(s) has/have been generated and will be send at the entered date.<"
"br />\n"
"    You will receive a confirmation email message when all the coupons have "
"been sent."
msgstr ""

#: $string['coupons_sent']
msgid ""
"Your coupon(s) have been generated. Within several minutes you will receive "
"an email with the Coupons in the attachments."
msgstr ""

#: $string['coupon:viewallreports']
msgid "View Coupon reports (for all coupons)"
msgstr ""

#: $string['coupon:viewreports']
msgid "View Coupon reports (for my owned coupons)"
msgstr ""

#: $string['course']
msgid "course"
msgstr ""

#: $string['days_access']
msgid "{$a} days of"
msgstr ""

#: $string['default-coupon-page-template-botleft']
msgid ""
"<ol>\n"
"<li>Sign up at {site_url}</li>\n"
"<li>You will receive an email with the confirmation url. Click on the url to "
"activate your account.</li>\n"
"<li>Enter your coupon code in the Moodle Coupon block</li>\n"
"<li>Happy learning!</li>\n"
"</ol>"
msgstr ""

#: $string['default-coupon-page-template-botright']
msgid ""
"<ol>\n"
"<li>Log in at {site_url}</li>\n"
"<li>Enter your coupon code in the Moodle Coupon block</li>\n"
"<li>Happy learning!</li>\n"
"</ol>"
msgstr ""

#: $string['default-coupon-page-template-main']
msgid ""
"With this coupon you can activate access to the e-learning module. You have "
"{accesstime} access to this module.\n"
"\n"
"Please use the following coupon code to activate access.\n"
"\n"
"{coupon_code}"
msgstr ""

#: $string['download-sample-csv']
msgid "Download sample CSV file"
msgstr ""

#: $string['error:alternative_email_invalid']
msgid ""
"If you have checked 'use alternative email' this field should contain a "
"valid email address."
msgstr ""

#: $string['error:alternative_email_required']
msgid "If you have checked 'use alternative email' this field is required."
msgstr ""

#: $string['error:cohort_sync']
msgid ""
"An error occured while trying to synchronize the cohorts. Please contact "
"support."
msgstr ""

#: $string['error:coupon_already_used']
msgid "The coupon with this code has already been used."
msgstr ""

#: $string['error:coupon_amount-recipients-both-set']
msgid ""
"Please specify a number of coupons to generate OR a csv list of recipients."
msgstr ""

#: $string['error:coupon_amount-recipients-both-unset']
msgid "Either this field or the field Recipients must be set."
msgstr ""

#: $string['error:coupon_amount_too_high']
msgid "Please enter an amonut between {$a->min} and {$a->max}."
msgstr ""

#: $string['error:coupon_reserved']
msgid "The coupon with this code has been reserved for an other user."
msgstr ""

#: $string['error:course-coupons-not-copied']
msgid ""
"An error occured while trying to copy coupon-courses to the new "
"coupon_courses table. Please contact support."
msgstr ""

#: $string['error:course-not-found']
msgid "The course could not be found."
msgstr ""

#: $string['error:invalid_coupon_code']
msgid "You have entered an invalid coupon code."
msgstr ""

#: $string['error:invalid_email']
msgid "Please enter a valid email adress."
msgstr ""

#: $string['error:missing_cohort']
msgid ""
"The cohort(s) linked to this coupon does not exist anymore. Please contact "
"support."
msgstr ""

#: $string['error:missing_course']
msgid ""
"The course linked to this coupon does not exist anymore. Please contact "
"support."
msgstr ""

#: $string['error:missing_group']
msgid ""
"The group(s) linked to this coupon does not exist anymore. Please contact "
"support."
msgstr ""

#: $string['error:moodledata_not_writable']
msgid ""
"Your moodledata/coupon_logos folder is not writable. Please fix your "
"permissions."
msgstr ""

#: $string['error:no_coupons_submitted']
msgid "None of your coupons have been used yet."
msgstr ""

#: $string['error:nopermission']
msgid "You have no permission to do this"
msgstr ""

#: $string['error:numeric_only']
msgid "This field must be numeric."
msgstr ""

#: $string['error:plugin_disabled']
msgid "The cohort_sync plugin has been disabled. Please contact support."
msgstr ""

#: $string['error:recipients-columns-missing']
msgctxt "$string['error:recipients-columns-missing']"
msgid ""
"The file could not be validated. Are you sure you entered the right columns "
"and seperator?"
msgstr ""

#: $string['error:recipients-email-invalid']
msgid ""
"The email address {$a->email} is invalid. Please fix it in the csv file."
msgstr ""

#: $string['error:recipients-empty']
msgid "Please enter at least one user."
msgstr ""

#: $string['error:recipients-extension']
msgid "You can only upload .csv files."
msgstr ""

#: $string['error:recipients-invalid']
msgctxt "$string['error:recipients-invalid']"
msgid ""
"The file could not be validated. Are you sure you entered the right columns "
"and seperator?"
msgstr ""

#: $string['error:recipients-max-exceeded']
msgid ""
"Your csv file has exceeded the maximum of 10.000 coupon users. Please limit "
"it."
msgstr ""

#: $string['error:required']
msgid "This field is required."
msgstr ""

#: $string['error:sessions-expired']
msgid "Your session has been expired. Please try again."
msgstr ""

#: $string['error:unable_to_enrol']
msgid ""
"An error occured while trying to enrol you in the new course. Please contact "
"support."
msgstr ""

#: $string['error:wrong_code_length']
msgid "Please enter a number between 6 and 32."
msgstr ""

#: $string['error:wrong_doc_page']
msgid "You are trying to access a page that does not exist."
msgstr ""

#: $string['error:wrong_image_size']
msgid ""
"The uploaded background does not have the required size. Please upload an "
"image with a ratio of 210 mm by 297 mm."
msgstr ""

#: $string['heading:administration']
msgid "Manage"
msgstr ""

#: $string['heading:amountForm']
msgid "Amount settings"
msgstr ""

#: $string['heading:coupon_type']
msgid "Type of coupon"
msgstr ""

#: $string['heading:csvForm']
msgid "CSV settings"
msgstr ""

#: $string['heading:general_settings']
msgid "Last settings"
msgstr ""

#: $string['heading:generatecoupons']
msgctxt "$string['heading:generatecoupons']"
msgid "Generate coupons"
msgstr ""

#: $string['heading:imageupload']
msgid "Upload image"
msgstr ""

#: $string['heading:info']
msgid "Info"
msgstr ""

#: $string['heading:input_cohorts']
msgid "Select cohorts"
msgstr ""

#: $string['heading:input_coupon']
msgid "Input coupon"
msgstr ""

#: $string['heading:inputcoupons']
msgctxt "$string['heading:inputcoupons']"
msgid "Input Coupon"
msgstr ""

#: $string['heading:input_course']
msgid "Select course"
msgstr ""

#: $string['heading:input_groups']
msgid "Select groups"
msgstr ""

#: $string['heading:label_instructions']
msgid "Instructions"
msgstr ""

#: $string['heading:manualForm']
msgid "Manual settings"
msgstr ""

#: $string['label:alternative_email']
msgid "Alternative email"
msgstr ""

#: $string['label:alternative_email_help']
msgid "Send coupons by default to this email address."
msgstr ""

#: $string['label:api_enabled']
msgid "Enable API"
msgstr ""

#: $string['label:api_enabled_desc']
msgid ""
"The Coupon API grants the possibility to generate coupons from an external "
"system."
msgstr ""

#: $string['label:api_password']
msgid "API Password"
msgstr ""

#: $string['label:api_password_desc']
msgid "The password that can be used to generate a coupon using the API."
msgstr ""

#: $string['label:api_user']
msgid "API User"
msgstr ""

#: $string['label:api_user_desc']
msgid "The username that can be used to generate a coupon using the API."
msgstr ""

#: $string['label:cleanupage']
msgid "Maximum age?"
msgstr ""

#: $string['label:cleanupage_help']
msgid "Enter the maximum age of an unused coupon before it will be removed"
msgstr ""

#: $string['label:cohort']
msgctxt "$string['label:cohort']"
msgid "Cohort"
msgstr ""

#: $string['label:connected_courses']
msgid "Connected course(s)"
msgstr ""

#: $string['label:coupon_amount']
msgid "Amount of coupons"
msgstr ""

#: $string['label:coupon_amount_help']
msgid ""
"This is the the amount of coupons that will be generated. Please use this "
"field OR the field recipients, not both."
msgstr ""

#: $string['label:coupon_code']
msgid "Coupon Code"
msgstr ""

#: $string['label:coupon_code_help']
msgid ""
"The coupon code is the unique code which is linked to each individual "
"coupon. You can find this code on your coupon."
msgstr ""

#: $string['label:coupon_code_length']
msgid "Code length"
msgstr ""

#: $string['label:coupon_code_length_desc']
msgid "Amount of characters of the coupon code."
msgstr ""

#: $string['label:coupon_cohorts']
msgctxt "$string['label:coupon_cohorts']"
msgid "Cohort(s)"
msgstr ""

#: $string['label:coupon_cohorts_help']
msgid "Select the one or more cohorts your users will be enrolled in."
msgstr ""

#: $string['label:coupon_connect_course']
msgid "Add course(s)"
msgstr ""

#: $string['label:coupon_connect_course_help']
msgid ""
"Select all courses you wish to add to the cohort.\n"
"    <br /><b><i>Note: </i></b>All users who are already enrolled in this "
"cohort will also be enrolled in the selected courses!"
msgstr ""

#: $string['label:coupon_courses']
msgid "Course(s)"
msgstr ""

#: $string['label:coupon_courses_help']
msgid "Select the courses your users will be enrolled in."
msgstr ""

#: $string['label:coupon_email']
msgid "Email address"
msgstr ""

#: $string['label:coupon_email_help']
msgid "This is the email address the generated coupons will be send to."
msgstr ""

#: $string['label:coupon_groups']
msgid "Add group(s)"
msgstr ""

#: $string['label:coupon_groups_help']
msgid ""
"Select the groups you wish your users to be enrolled in upon enrolment in "
"the courses."
msgstr ""

#: $string['label:coupon_recipients']
msgctxt "$string['label:coupon_recipients']"
msgid "Recipients"
msgstr ""

#: $string['label:coupon_recipients_help']
msgid "With this field you can upload a csv file with users."
msgstr ""

#: $string['label:coupon_recipients_txt']
msgctxt "$string['label:coupon_recipients_txt']"
msgid "Recipients"
msgstr ""

#: $string['label:coupon_recipients_txt_help']
msgid "In this field you can make your final changes to the uploaded csv file."
msgstr ""

#: $string['label:coupon_type']
msgid "Generate based on"
msgstr ""

#: $string['label:coupon_type_help']
msgid ""
"The Coupons will be generated based on either the course or one or more "
"cohorts."
msgstr ""

#: $string['label:current_image']
msgid "Current Coupon background"
msgstr ""

#: $string['label:date_send_coupons']
msgctxt "$string['label:date_send_coupons']"
msgid "Send date"
msgstr ""

#: $string['label:date_send_coupons_help']
msgid "Date the coupons will be send to the recipient(s)."
msgstr ""

#: $string['label:email_body']
msgid "Email message"
msgstr ""

#: $string['label:email_body_help']
msgid "The email message that will be send to the recipients of the coupons."
msgstr ""

#: $string['label:enablecleanup']
msgid "Enable cleaning up unused coupons?"
msgstr ""

#: $string['label:enablecleanup_help']
msgid "Check this option to automatically clean (remove) unused coupons"
msgstr ""

#: $string['label:enrolment_period']
msgid "Enrolment period"
msgstr ""

#: $string['label:enrolment_period_help']
msgid ""
"Period (in days) the user will be enrolled in the courses. If set to 0 no "
"end will be issued."
msgstr ""

#: $string['label:enter_coupon_code']
msgid "Please enter your coupon code here"
msgstr ""

#: $string['label:generate_pdfs']
msgid "Generate seperate PDF's"
msgstr ""

#: $string['label:generate_pdfs_help']
msgid ""
"You can select here if you want to receive your coupons in either a single "
"file or each coupon in a separate PDF file."
msgstr ""

#: $string['label:image']
msgid "Coupon background"
msgstr ""

#: $string['label:image_desc']
msgid "Background to be placed in the generated coupons"
msgstr ""

#: $string['label:info_coupon_cohort_courses']
msgid "Information on page: Cohort courses"
msgstr ""

#: $string['label:info_coupon_cohorts']
msgid "Information on page: Select cohorts"
msgstr ""

#: $string['label:info_coupon_confirm']
msgid "Information on page: Confirm coupon"
msgstr ""

#: $string['label:info_coupon_course']
msgid "Information on page: Select course"
msgstr ""

#: $string['label:info_coupon_course_groups']
msgid "Information on page: Select course groups"
msgstr ""

#: $string['label:info_coupon_type']
msgid "Information on page: Select coupon type"
msgstr ""

#: $string['label:info_desc']
msgid "The information shown above the form."
msgstr ""

#: $string['label:info_imageupload']
msgid "Information on page: Upload image"
msgstr ""

#: $string['label:max_coupons']
msgid "Maximum coupons"
msgstr ""

#: $string['label:max_coupons_desc']
msgid "Amount of coupons that can be created in one time."
msgstr ""

#: $string['label:no_courses_connected']
msgid "There are no courses connected to this cohort."
msgstr ""

#: $string['label:no_groups_selected']
msgid "There are no groups connected to these courses yet."
msgstr ""

#: $string['label:redirect_url']
msgid "Redirect URL"
msgstr ""

#: $string['label:redirect_url_help']
msgid "The destination users will be send to after entering their coupon code."
msgstr ""

#: $string['label:selected_cohort']
msgid "Selected cohort(s)"
msgstr ""

#: $string['label:selected_courses']
msgid "Selected courses"
msgstr ""

#: $string['label:selected_groups']
msgid "Selected group(s)"
msgstr ""

#: $string['label:showform']
msgid "Generator options"
msgstr ""

#: $string['label:type_cohorts']
msgctxt "$string['label:type_cohorts']"
msgid "Cohort(s)"
msgstr ""

#: $string['label:type_course']
msgctxt "$string['label:type_course']"
msgid "Course"
msgstr ""

#: $string['label:use_alternative_email']
msgid "Use alternative email"
msgstr ""

#: $string['label:use_alternative_email_help']
msgid ""
"When checked it will by default use the email address provided in the "
"Alternative email field."
msgstr ""

#: $string['missing_config_info']
msgid ""
"Put your extra information here - to be set up in the global configuration "
"of the block."
msgstr ""

#: $string['page:generate_coupon.php:title']
msgctxt "$string['page:generate_coupon.php:title']"
msgid "Generate coupons"
msgstr ""

#: $string['page:generate_coupon_step_five.php:title']
msgctxt "$string['page:generate_coupon_step_five.php:title']"
msgid "Generate coupons"
msgstr ""

#: $string['page:generate_coupon_step_four.php:title']
msgctxt "$string['page:generate_coupon_step_four.php:title']"
msgid "Generate coupons"
msgstr ""

#: $string['page:generate_coupon_step_three.php:title']
msgctxt "$string['page:generate_coupon_step_three.php:title']"
msgid "Generate coupons"
msgstr ""

#: $string['page:generate_coupon_step_two.php:title']
msgctxt "$string['page:generate_coupon_step_two.php:title']"
msgid "Generate coupons"
msgstr ""

#: $string['page:unused_coupons.php:title']
msgctxt "$string['page:unused_coupons.php:title']"
msgid "Unused coupons"
msgstr ""

#: $string['pdf_generated']
msgid "The coupons have been attached to this email in PDF files.<br /><br />"
msgstr ""

#: $string['pdf-meta:keywords']
msgctxt "$string['pdf-meta:keywords']"
msgid "Moodle Coupon"
msgstr ""

#: $string['pdf-meta:subject']
msgctxt "$string['pdf-meta:subject']"
msgid "Moodle Coupon"
msgstr ""

#: $string['pdf-meta:title']
msgctxt "$string['pdf-meta:title']"
msgid "Moodle Coupon"
msgstr ""

#: $string['pdf:titlename']
msgctxt "$string['pdf:titlename']"
msgid "Moodle Coupon"
msgstr ""

#: $string['pluginname']
msgctxt "$string['pluginname']"
msgid "Coupon"
msgstr ""

#: $string['promo']
msgid "Coupon plugin for Moodle"
msgstr ""

#: $string['promodesc']
msgid ""
"This plugin is written by Sebsoft Managed Hosting & Software Development\n"
"(<a href='http://www.sebsoft.nl/' target='_new'>http://sebsoft.nl</a>).<br "
"/><br />\n"
"{$a}<br /><br />"
msgstr ""

#: $string['report:cohorts']
msgctxt "$string['report:cohorts']"
msgid "Cohort"
msgstr ""

#: $string['report:coupon_code']
msgctxt "$string['report:coupon_code']"
msgid "Subscription code"
msgstr ""

#: $string['report:dateformat']
msgid "%d-%m-%Y %H:%M:%S"
msgstr ""

#: $string['report:dateformatymd']
msgid "%d-%m-%Y"
msgstr ""

#: $string['report:download-excel']
msgid "Download unused coupons"
msgstr ""

#: $string['report:enrolperiod']
msgctxt "$string['report:enrolperiod']"
msgid "Owner"
msgstr ""

#: $string['report:for_user_email']
msgctxt "$string['report:for_user_email']"
msgid "Planned for"
msgstr ""

#: $string['report:heading:coursename']
msgid "Course name"
msgstr ""

#: $string['report:heading:coursetype']
msgid "Course type"
msgstr ""

#: $string['report:heading:datecomplete']
msgid "Date completed"
msgstr ""

#: $string['report:heading:datestart']
msgid "Startdate"
msgstr ""

#: $string['report:heading:grade']
msgid "Grade"
msgstr ""

#: $string['report:heading:status']
msgid "Status"
msgstr ""

#: $string['report:heading:user']
msgid "User"
msgstr ""

#: $string['report:immediately']
msgctxt "$string['report:immediately']"
msgid "Immediately"
msgstr ""

#: $string['report:issend']
msgid "Is send"
msgstr ""

#: $string['report:owner']
msgctxt "$string['report:owner']"
msgid "Owner"
msgstr ""

#: $string['report:senddate']
msgctxt "$string['report:senddate']"
msgid "Send date"
msgstr ""

#: $string['report:status_completed']
msgid "Course completed"
msgstr ""

#: $string['report:status_not_started']
msgid "Course not started yet"
msgstr ""

#: $string['report:status_started']
msgid "Course started"
msgstr ""

#: $string['showform-amount']
msgid "I want to create an arbitrary amount of coupons"
msgstr ""

#: $string['showform-csv']
msgid "I want to create coupons using a CSV with recipients"
msgstr ""

#: $string['showform-manual']
msgid "I want to manually configure the recipients"
msgstr ""

#: $string['str:mandatory']
msgid "Mandatory"
msgstr ""

#: $string['str:optional']
msgid "Optional"
msgstr ""

#: $string['success:coupon_used']
msgid "Coupon used - You can now access the course(s)"
msgstr ""

#: $string['success:uploadimage']
msgid "Your new coupon image has been uploaded."
msgstr ""

#: $string['tab:apidocs']
msgid "API Docs"
msgstr ""

#: $string['tab:report']
msgid "Progress report"
msgstr ""

#: $string['tab:unused']
msgctxt "$string['tab:unused']"
msgid "Unused coupons"
msgstr ""

#: $string['tab:used']
msgid "Used coupons"
msgstr ""

#: $string['tab:wzcouponimage']
msgid "Template image"
msgstr ""

#: $string['tab:wzcoupons']
msgid "Generate coupon(s)"
msgstr ""

#: $string['task:cleanup']
msgid "Cleaning up unused old coupons"
msgstr ""

#: $string['task:sendcoupons']
msgid "Send scheduled coupons"
msgstr ""

#: $string['tasksettings']
msgid "Task settings"
msgstr ""

#: $string['tasksettings_desc']
msgctxt "$string['tasksettings_desc']"
msgid ""
msgstr ""

#: $string['textsettings']
msgid "Text settings"
msgstr ""

#: $string['textsettings_desc']
msgid ""
"Here you can configure custom texts to be displayed by various wizard "
"screens for the coupon generator"
msgstr ""

#: $string['th:action']
msgid "Action(s)"
msgstr ""

#: $string['th:cohorts']
msgctxt "$string['th:cohorts']"
msgid "Cohort"
msgstr ""

#: $string['th:course']
msgctxt "$string['th:course']"
msgid "Course"
msgstr ""

#: $string['th:enrolperiod']
msgid "Enrolperiod"
msgstr ""

#: $string['th:for_user_email']
msgctxt "$string['th:for_user_email']"
msgid "Planned for"
msgstr ""

#: $string['th:groups']
msgid "Group(s)"
msgstr ""

#: $string['th:immediately']
msgctxt "$string['th:immediately']"
msgid "Immediately"
msgstr ""

#: $string['th:issend']
msgid "Sent?"
msgstr ""

#: $string['th:owner']
msgctxt "$string['th:owner']"
msgid "Owner"
msgstr ""

#: $string['th:senddate']
msgctxt "$string['th:senddate']"
msgid "Send date"
msgstr ""

#: $string['th:submission_code']
msgctxt "$string['th:submission_code']"
msgid "Subscription code"
msgstr ""

#: $string['unlimited_access']
msgid "unlimited"
msgstr ""

#: $string['url:api_docs']
msgid "API Documentation"
msgstr ""

#: $string['url:generate_coupons']
msgctxt "$string['url:generate_coupons']"
msgid "Generate Coupon"
msgstr ""

#: $string['url:input_coupon']
msgctxt "$string['url:input_coupon']"
msgid "Input Coupon"
msgstr ""

#: $string['url:uploadimage']
msgid "Change coupon image"
msgstr ""

#: $string['url:view_reports']
msgid "View reports"
msgstr ""

#: $string['url:view_unused_coupons']
msgid "View unused coupons"
msgstr ""

#: $string['view:api_docs:heading']
msgctxt "$string['view:api_docs:heading']"
msgid "Coupon API Documentation"
msgstr ""

#: $string['view:api_docs:title']
msgctxt "$string['view:api_docs:title']"
msgid "Coupon API Documentation"
msgstr ""

#: $string['view:api:heading']
msgctxt "$string['view:api:heading']"
msgid "Coupon API"
msgstr ""

#: $string['view:api:title']
msgctxt "$string['view:api:title']"
msgid "Coupon API"
msgstr ""

#: $string['view:generate_coupon:heading']
msgctxt "$string['view:generate_coupon:heading']"
msgid "Generate Coupon"
msgstr ""

#: $string['view:generate_coupon:title']
msgctxt "$string['view:generate_coupon:title']"
msgid "Generate Coupon"
msgstr ""

#: $string['view:input_coupon:heading']
msgctxt "$string['view:input_coupon:heading']"
msgid "Input Coupon"
msgstr ""

#: $string['view:input_coupon:title']
msgctxt "$string['view:input_coupon:title']"
msgid "Input Coupon"
msgstr ""

#: $string['view:reports:heading']
msgctxt "$string['view:reports:heading']"
msgid "Report - Coupon based progress"
msgstr ""

#: $string['view:reports:title']
msgctxt "$string['view:reports:title']"
msgid "Report - Coupon based progress"
msgstr ""

#: $string['view:reports-unused:heading']
msgctxt "$string['view:reports-unused:heading']"
msgid "Report - Unused Coupons"
msgstr ""

#: $string['view:reports-unused:title']
msgctxt "$string['view:reports-unused:title']"
msgid "Report - Unused Coupons"
msgstr ""

#: $string['view:reports-used:heading']
msgctxt "$string['view:reports-used:heading']"
msgid "Report - Used Coupons"
msgstr ""

#: $string['view:reports-used:title']
msgctxt "$string['view:reports-used:title']"
msgid "Report - Used Coupons"
msgstr ""

#: $string['view:uploadimage:heading']
msgid "Upload a new coupon background"
msgstr ""

#: $string['view:uploadimage:title']
msgid "Upload coupon background"
msgstr ""
