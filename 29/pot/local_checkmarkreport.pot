#. extracted from en\local_checkmarkreport.php
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-10-24 22:14+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Translate Toolkit 1.13.0\n"

#. // This file is part of Moodle - http://moodle.org/
#. //
#. // Moodle is free software: you can redistribute it and/or modify
#. // it under the terms of the GNU General Public License as published by
#. // the Free Software Foundation, either version 3 of the License, or
#. // (at your option) any later version.
#. //
#. // Moodle is distributed in the hope that it will be useful,
#. // but WITHOUT ANY WARRANTY; without even the implied warranty of
#. // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#. // GNU General Public License for more details.
#. //
#. // You should have received a copy of the GNU General Public License
#. // along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
#. /**
#.  * Strings for component 'local_checkmarkreport', language 'en', branch 'MOODLE_28_STABLE'
#.  *
#.  * @package   local_checkmarkreport
#.  * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
#.  * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
#.  */
#: $string['additional_columns']
msgid "Additional columns"
msgstr ""

#: $string['additional_information']
msgid "Additional information"
msgstr ""

#: $string['additional_settings']
msgid "Additional settings"
msgstr ""

#: $string['by']
msgid "by"
msgstr ""

#: $string['checkmarkreport:view']
msgid "View checkmark report"
msgstr ""

#: $string['checkmarkreport:view_courseoverview']
msgctxt "$string['checkmarkreport:view_courseoverview']"
msgid "View checkmark coursereport"
msgstr ""

#: $string['checkmarkreport:view_own_overview']
msgctxt "$string['checkmarkreport:view_own_overview']"
msgid "View my checkmark report"
msgstr ""

#: $string['checkmarkreport:view_students_overview']
msgctxt "$string['checkmarkreport:view_students_overview']"
msgid "View checkmark userreport"
msgstr ""

#: $string['error_retriefing_members']
msgid "Error getting groupmembers"
msgstr ""

#: $string['eventexported']
msgid "Checkmarkreport exported"
msgstr ""

#: $string['eventoverviewexported']
msgid "Checkmarkreport overview exported"
msgstr ""

#: $string['eventoverviewviewed']
msgid "Checkmarkreport overview viewed"
msgstr ""

#: $string['eventuseroverviewexported']
msgid "Checkmarkreport useroverview exported"
msgstr ""

#: $string['eventuseroverviewviewed']
msgid "Checkmarkreport useroverview viewed"
msgstr ""

#: $string['eventuserviewexported']
msgid "Checkmarkreport userview exported"
msgstr ""

#: $string['eventuserviewviewed']
msgid "Checkmarkreport userview viewed"
msgstr ""

#: $string['eventviewed']
msgid "Checkmarkreport viewed"
msgstr ""

#: $string['example']
msgid "Example"
msgstr ""

#: $string['examples']
msgid "Examples"
msgstr ""

#: $string['exportas']
msgid "Export as"
msgstr ""

#: $string['filter']
msgid "Filter"
msgstr ""

#: $string['grade']
msgid "Grade"
msgstr ""

#: $string['grade_help']
msgid ""
"Shows grade calculated by checked examples for the whole course as well as "
"for each checkmark."
msgstr ""

#: $string['groupings']
msgid "Groupings"
msgstr ""

#: $string['groups']
msgid "Groups"
msgstr ""

#: $string['groups_help']
msgid ""
"Use this to select the groups to be displayed. Empty groups are disabled and "
"only members of selected groups are displayed in the users selection (after "
"update)."
msgstr ""

#: $string['loading']
msgid "Loading..."
msgstr ""

#: $string['noaccess']
msgid ""
"You have no access to this module. You're missing the required capabilities "
"to view this content."
msgstr ""

#: $string['overview']
msgid "Overview"
msgstr ""

#: $string['overview_alt']
msgctxt "$string['overview_alt']"
msgid "View checkmark coursereport"
msgstr ""

#: $string['overwritten']
msgid "Overwritten"
msgstr ""

#: $string['pluginname']
msgid "Checkmark report"
msgstr ""

#: $string['pluginname_help']
msgid ""
"Checkmark reports extend the functionality of mod_checkmark by making "
"convenient course-overview reports for all checkmarks available."
msgstr ""

#: $string['pluginnameplural']
msgid "Checkmark reports"
msgstr ""

#: $string['showgrade']
msgid "Show grade"
msgstr ""

#: $string['showpoints']
msgid "Show points"
msgstr ""

#: $string['showpoints_help']
msgid ""
"Show coresponding points for each example instead of checked (☒) or "
"unchecked (☐) symbols."
msgstr ""

#: $string['status']
msgid "Status"
msgstr ""

#: $string['sumabs']
msgid "Show x/y examples"
msgstr ""

#: $string['sumabs_help']
msgid ""
"Show how many examples have been checked in the whole course and for each "
"checkmark."
msgstr ""

#: $string['sumrel']
msgid "Show % of examples/grades"
msgstr ""

#: $string['sumrel_help']
msgid ""
"Show relative values ( XX % ) of checked examples as well as grade "
"calculated by checked examples for course in total and each checkmark."
msgstr ""

#: $string['update']
msgid "Update"
msgstr ""

#: $string['useroverview']
msgid "Student Overview"
msgstr ""

#: $string['useroverview_alt']
msgctxt "$string['useroverview_alt']"
msgid "View checkmark userreport"
msgstr ""

#: $string['userview']
msgid "Userview"
msgstr ""

#: $string['userview_alt']
msgctxt "$string['userview_alt']"
msgid "View my checkmark report"
msgstr ""

#: $string['xlsover256']
msgid ""
"XLS file format only supports 256 columns at maximum. The file you're about "
"to generate exceeds this limitation. Please deselect some instances or avoid "
"using XLS."
msgstr ""
